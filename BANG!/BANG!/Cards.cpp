﻿#include "Cards.h"
#include <utility> 

Cards::Cards() :
	Cards("None", 0, "None")
{
	this->distance = 0;
}
void Cards::verifName(const std::string & name)
{
	if (name == "Bang!")
	{
		actions.push_back(action::BANG);
		actions.push_back(action::Oricare_jucator_la_o_distanta_disponibila);
	}
	if (name == "Ratat!")
	{
		actions.push_back(action::RATAT);
	}
	if (name == "Panica!")
	{
		actions.push_back(action::Trage_o_carte);
		actions.push_back(action::Oricare_jucator_la_o_distanta_maxima_afisata);
		this->distance = 1;
	}
	if (name == "Diligenta")
	{
		actions.push_back(action::Trage_o_carte);
		actions.push_back(action::Trage_o_carte);
	}
	if (name == "Wells Fargo")
	{
		actions.push_back(action::Trage_o_carte);
		actions.push_back(action::Trage_o_carte);
		actions.push_back(action::Trage_o_carte);
	}
	if (name == "Cat Balou")
	{
		actions.push_back(action::Obliga_sa_decarteze_o_carte);
		actions.push_back(action::Oricare_jucator);
	}
	if (name == "Salon")
	{
		actions.push_back(action::Primesti_1_punct_de_viata);
		actions.push_back(action::Fiecare_jucator);
	}
	if (name == "Luneta")
	{
		actions.push_back(action::Ii_vezi_pe_ceilalti_la_o_distanta_mai_mica_cu_1);
		this->type = true;
	}
	if (name == "Mustang")
	{
		actions.push_back(action::Ceilalti_te_vad_la_o_distanta_mai_mare_cu_1);
		this->type = true;
	}
	if (name == "Bere")
	{
		actions.push_back(action::Primesti_1_punct_de_viata);
	}
	if (name == "Butoi")
	{
		actions.push_back(action::Trage_o_carte);
		actions.push_back(action::Daca_e_inima);
		actions.push_back(action::RATAT);
		this->type = true;
	}
	if (name == "Gatling")
	{
		actions.push_back(action::BANG);
		actions.push_back(action::Fiecare_jucator);
	}
	if (name == "Volcanic")
	{
		actions.push_back(action::Poti_juca_oricate_carti_Bang);
		this->distance = 1;
		this->type = true;
	}
	if (name == "Remington")
	{
		this->distance = 3;
		this->type = true;
	}
	if (name == "Rev. Carabine")
	{
		this->distance = 4;
		this->type = true;
	}
	if (name == "Schofield")
	{
		this->distance = 2;
		this->type = true;
	}
	if (name == "Winchester")
	{
		this->distance = 5;
		this->type = true;
	}
	if (name == "Colt 45")
	{
		this->distance = 1;
		this->type = true;
	}
	
}

Cards::Cards(const std::string & name, const int &numb, const std::string &symbol)
{
	this->distance = 0;
	this->type = false;
	this->number.first = numb;
	if (symbol == "Hearts")
		this->number.second = Cards::symbol::Hearts;
	if (symbol == "Clovers")
		this->number.second = Cards::symbol::Clovers;
	if (symbol == "Pikes")
		this->number.second = Cards::symbol::Pikes;
	if (symbol == "Tiles")
		this->number.second = Cards::symbol::Tiles;
	if (symbol == "None")
		this->number.second = Cards::symbol::None;
	verifName(name);
	this->name = name;


}

Cards::Cards(const Cards& other)
{
	*this = other;
}

Cards & Cards::operator=(const Cards & other)
{
	this->name = other.name;
	this->actions = other.actions;
	this->number = other.number;
	this->distance = other.distance;
	this->type = other.type;
	return *(this);
}

std::string Cards::getName() const
{
	return this->name;
}

int Cards::getDistance() const
{
	return this->distance;
}

bool Cards::getType() const
{
	return this->type;
}

std::vector<Cards::action> Cards::getActions() const
{
	return this->actions;
}

std::pair<int, Cards::symbol> Cards::getNumber() const
{
	return this->number;
}

bool Cards::isGun() const
{
	if (name == "Volcanic")
	{
		return true;
	}
	if (name == "Remington")
	{
		return true;
	}
	if (name == "Rev. Carabine")
	{
		return true;
	}
	if (name == "Schofield")
	{
		return true;
	}
	if (name == "Winchester")
	{
		return true;
	}
	/*else 
		throw "This is not a gun.";*/
	return false;
}

std::ostream & operator<<(std::ostream & os, const Cards & card)
{
	os << "Nume: ";
	os << card.name << std::endl;
	os << "Tip: ";
	if (card.type == false)
		os << "Nu poate fi activata" << std::endl;
	else
		if (card.getName() != "Colt 45")
			os << "Poate fi activata" << std::endl;
	if (!card.actions.empty())
	{
		os << "Actiuni:\n";
		if (card.distance == 0 || card.distance == 1)
		{

			for (int index = 0; index < card.actions.size(); index++)
			{
				//os << static_cast<int>(card.actions[index]) << std::endl;
				switch (card.actions[index])
				{
				case Cards::action::BANG:os << "BANG!" << std::endl; break;
				case Cards::action::RATAT:os << "RATAT!" << std::endl; break;
				case Cards::action::Ceilalti_te_vad_la_o_distanta_mai_mare_cu_1:os << "Ceilalti te vad la o distanta mai mare cu 1" << std::endl; break;
				case Cards::action::Daca_e_inima:os << "Daca e inima" << std::endl; break;
				case Cards::action::Fiecare_jucator:os << "Fiecare jucator" << std::endl; break;
				case Cards::action::Ii_vezi_pe_ceilalti_la_o_distanta_mai_mica_cu_1:os << "Ii vezi pe ceilalti la o distanta mai mica cu 1" << std::endl; break;
				case Cards::action::Obliga_sa_decarteze_o_carte:os << "Obliga sa decarteze o carte" << std::endl; break;
				case Cards::action::Oricare_jucator:os << "Oricare jucator" << std::endl; break;
				case Cards::action::Oricare_jucator_la_o_distanta_disponibila:os << "Oricare jucator la o distanta disponibila" << std::endl; break;
				case Cards::action::Oricare_jucator_la_o_distanta_maxima_afisata:os << "Oricare jucator la o distanta maxima afisata" << std::endl; break;
				case Cards::action::Primesti_1_punct_de_viata:os << "Primesti 1 punct de viata" << std::endl; break;
				case Cards::action::Trage_o_carte:os << "Trage o carte" << std::endl; break;
				case Cards::action::Poti_juca_oricate_carti_Bang:os << "Poti juca oricate carti BANG! in tura ta, la o distanta de 1" << std::endl; break;
				}
			}
		}
	}
	if (card.distance > 0)
	{
		os << "Distanta de tragere: " << card.distance << std::endl;
	}

	switch (card.number.first)
	{
	case 11:os << "J"; break;
	case 12:os << "Q"; break;
	case 13:os << "K"; break;
	case 14:os << "A"; break;
	case 0:break;
	default:os << card.number.first; break;
	}
	switch (card.number.second)
	{
	case Cards::symbol::Hearts:os << " Hearts" << std::endl; break;
	case Cards::symbol::Tiles:os << " Tiles" << std::endl; break;
	case Cards::symbol::Clovers:os << " Clovers" << std::endl; break;
	case Cards::symbol::Pikes:os << " Pikes" << std::endl; break;

	}
	return	os;
}

std::istream & operator>>(std::istream & in, Cards & card)
{
	std::string symbol;
	//in>> card.name;
	std::getline(in, card.name);
	in >> card.number.first;

	std::getline(in, symbol);
	//in >> card.number.first;
	//in >>symbol;
	if (symbol == "Hearts")
		card.number.second = Cards::symbol::Hearts;
	if (symbol == "Clovers")
		card.number.second = Cards::symbol::Clovers;
	if (symbol == "Pikes")
		card.number.second = Cards::symbol::Pikes;
	if (symbol == "Tiles")
		card.number.second = Cards::symbol::Tiles;
	card.verifName(card.name);
	return in;
}

Cards::Cards(Cards&& other)
{
	*this = std::move(other);
}

Cards& Cards::operator= (Cards&& other)
{
	name = std::move(other.name);
	distance = std::move(other.distance);
	type = std::move(other.type);
	for (auto it : other.actions)
	{
		actions.push_back(std::move(it));
	}
	number = std::move(other.number);

	new(&other) Cards;

	return *this;
}
