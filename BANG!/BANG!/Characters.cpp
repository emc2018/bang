#include<iostream>
#include "Characters.h"
#include "../FunctionDLL/FunctionDLL/Functions.h"

Characters::Characters()
{
	for (auto index = 0; index <= 16; ++index)
		characters.push_back(index);
	Functions f;
	characters= f.shuffleVector(characters);

}

Characters::Characters(int nrCaracter)
{
	switch (nrCaracter)
	{
	case 1:
	{
		this->name = Name::BlackJack;
		this->lives = 4;
		break;
	}
	case 2:
	{
		this->name = Name::LuckyDuke;
		this->lives = 4;
		break;
	}
	case 3:
	{
		this->name = Name::JesseJones;
		this->lives = 4;
		break;
	}
	case 4:
	{
		this->name = Name::SuzyLafayette;
		this->lives = 4;
		break;
	}
	case 5:
	{
		this->name = Name::WillyTheKid;
		this->lives = 4;
		break;
	}
	case 6:
	{
		this->name = Name::SidKetchum;
		this->lives = 4;
		break;
	}
	case 7:
	{
		this->name = Name::PedroRamirez;
		this->lives = 4;
		break;
	}
	case 8:
	{
		this->name = Name::RoseDoolan;
		this->lives = 4;
		break;
	}
	case 9:
	{
		this->name = Name::ElGringo;
		this->lives = 3;
		break;
	}
	case 10:
	{
		this->name = Name::PaulRegret;
		this->lives = 3;
		break;
	}
	case 11:
	{
		this->name = Name::Jourdannais;
		this->lives = 4;
		break;
	}
	case 12:
	{
		this->name = Name::KitCarlson;
		this->lives = 4;
		break;
	}
	case 13:
	{
		this->name = Name::VultureSam;
		this->lives = 4;
		break;
	}
	case 14:
	{
		this->name = Name::CalamityJanet;
		this->lives = 4;
		break;
	}
	case 15:
	{
		this->name = Name::SlabTheKiller;
		this->lives = 4;
		break;
	}
	case 16:
	{
		this->name = Name::BartCassidy;
		this->lives = 4;
		break;
	}
	case 0: {
		this->name = Name::None;
		this->lives = 0;
		break;
	}
	/*default:
		throw "index out of bound."; break;*/
	}
}
std::ostream & operator << (std::ostream& os, const Characters& characters)
{
	switch (characters.name)
	{
	case  Characters::Name::BlackJack:
	{
		os << "Name: Black Jack" << std::endl << "Ability: In prima faza a turei arata a doua carte trasa,Daca e Cupa sau Caro, mai trage o carte."
			<< std::endl;
		break;

	}
	case Characters::Name::LuckyDuke:
	{
		os << "Name: Lucky Duke" << std::endl << "Ability: De fiecare data cand <<trage!>> intoarce primele doua carti si alege una."
			<< std::endl;
		break;

	}
	case Characters::Name::JesseJones:
	{
		os << "Name: Jasse Jones" << std::endl << "Ability:In prima faza a turei poate sa traga prima carte din mana altui jucator. "
			<< std::endl;
		break;

	}
	case Characters::Name::SuzyLafayette:
	{
		os << "Name: Suzi Lafayette" << std::endl << "Ability:Cand ramane fara carti trage o carte. "
			<< std::endl;
		break;

	}
	case  Characters::Name::WillyTheKid:
	{
		os << "Name: Willy The Kid" << std::endl << "Ability:Poate juca oricate carti BANG! In tura sa. "
			<< std::endl;
		break;
	}
	case Characters::Name::SidKetchum:
	{
		os << "Name: Sid Ketchum" << std::endl << "Ability:Oricand poate sa arunce doua carti si reprimeste o viata. "
			<< std::endl;
		break;
	}

	case Characters::Name::PedroRamirez:
	{
		os << "Name: Pedro Ramirez" << std::endl << "Ability:In prima faza a turei poate trage prima carte din teancul cartilor decarctate. "
			<< std::endl;
		break;
	}
	case Characters::Name::RoseDoolan:
	{
		os << "Name: Rose Doolan" << std::endl << "Ability:Ea ii vede pe ceilalti la o distanta mai mica de 1. "
			<< std::endl;
		break;
	}
	case Characters::Name::ElGringo:
	{
		os << "Name: El Gringo" << std::endl << "Ability:De fiecare data cand pierde o viata, trage o carte de la atacator. "
			<< std::endl;
		break;
	}
	case Characters::Name::PaulRegret:
	{
		os << "Name: Paul Regret" << std::endl << "Ability: Toti jucatorii il vad la o distanta mai mare cu 1. "
			<< std::endl;
		break;
	}
	case Characters::Name::Jourdannais:
	{
		os << "Name: Jourdannais" << std::endl << "Ability: Cand este atacat cu un BANG!, poate trage o carte: daca e cupa e Ratat! "
			<< std::endl;
		break;
	}
	case Characters::Name::KitCarlson:
	{
		os << "Name: Kit Carlson" << std::endl << "Ability:In prima faza a turei se uita la primele 3 carti si alege doua dintre ele."
			<< std::endl;
		break;
	}
	case Characters::Name::VultureSam:
	{
		os << "Name: Vultur Sam" << std::endl << "Ability:Cand un jucator este eliminat, primeste toate cartile acestuia."
			<< std::endl;
		break;
	}
	case Characters::Name::CalamityJanet:
	{
		os << "Name: Camanity Janet" << std::endl << "Ability:Poate folosi cartilr BANG! la fel ca Ratat! si invers."
			<< std::endl;
		break;
	}
	case Characters::Name::SlabTheKiller:
	{
		os << "Name: Slab The Killer" << std::endl << "Ability:Cand ataca cu un BANG!, cel atacat se poate apara doar cu 2 carti Ratat!"
			<< std::endl;
		break;
	}
	case Characters::Name::BartCassidy:
	{
		os << "Name: Bart Cassidy" << std::endl << "Ability:Cand pierde o viata trage o carte din teanc."
			<< std::endl;
		break;
	}
	default:
		break;
	}
	return os;
}
Characters::Name  Characters::getName()
{
	return this->name;
}
int Characters::getLives()
{
	return this->lives;
}
int Characters::pickCharacter(const int & nr, const int& nrPlayer)
{
	if (nr > 0 && nr <= 16 - nrPlayer + 1)
	{
		int number = std::move(characters.at(nr));
		characters.erase(characters.begin() + nr );
		
		return std::move(number);
	}
	else
		throw std::out_of_range("index out of bound.");
 }
Characters Characters::setLives(int nrLives)
{
	this->lives = nrLives;
	return *this;
}

Characters Characters::operator=(const Characters & other)
{
	this->characters = other.characters;
	this->lives = other.lives;
	this->name = other.name;
	return Characters();
}
