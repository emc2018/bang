#pragma once
#include<vector>
class Characters
{
public:
	enum class Name {
		None,
		BlackJack,
		LuckyDuke,
		JesseJones,
		SuzyLafayette,
		WillyTheKid,
		SidKetchum,
		PedroRamirez,
		RoseDoolan,
		ElGringo,
		PaulRegret,
		Jourdannais,
		KitCarlson,
		VultureSam,
		CalamityJanet,
		SlabTheKiller,
		BartCassidy
	};
public:
	Characters();
	Characters(int nrCaracter);

	Name getName();
	int getLives();
	Characters setLives(int nrLives);

	int pickCharacter(const int &nr, const int& nrPlayer);
	Characters operator=(const Characters& other);
	friend std::ostream & operator << (std::ostream& os, const Characters& characters);
	
private:
	//fiecare caracter are un nume si un numar de vieti si abilitatile acestor caractere se iau in fucntie de numele acestuia
	std::vector<int>characters;
	Name name;
	int lives;

};

