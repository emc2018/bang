#include<iostream>
#include "DiscardedCards.h"
#include "Cards.h"


DiscardedCards::DiscardedCards(const std::vector<Cards>& cards) :
	discardedCards(cards)
{
}

std::vector<Cards> DiscardedCards::getDiscardedCards()const
{
	return this->discardedCards;
}

void DiscardedCards::addCard(const Cards & card)
{
	this->discardedCards.push_back(card);
}

void DiscardedCards::showTheLastCard()
{
	if (!discardedCards.empty())
		std::cout << discardedCards.back();
}

Cards DiscardedCards::pickLastCard() 
{

	if (discardedCards.size() > 0)
	{
		auto card = std::move(discardedCards.back());
		discardedCards.pop_back();
		return std::move(card);
	}
	else
		throw std::exception("The package is empty");
}


