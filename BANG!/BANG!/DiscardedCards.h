#pragma once
#include<vector>
#include"Cards.h"
class DiscardedCards
{
public:
	DiscardedCards() = default;
	DiscardedCards(const std::vector<Cards>& cards);

	std::vector<Cards>getDiscardedCards()const;

	void addCard(const Cards& card);
	void showTheLastCard();
	Cards pickLastCard();

private:
	//tencul de carti decartate
	std::vector<Cards>discardedCards;
};

