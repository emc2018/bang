#include "Game.h"
#include "../FunctionDLL/FunctionDLL/Functions.h"
#include<fstream>
#include<iostream>
#include <chrono> 
#include <random>

std::ofstream of("syslog.log", std::ios::app);
Game::Game() :
	logger(of)
{
	logger.log("----------------------------------------------------------------------------------------", Logger::Level::None);
}

Game::~Game()
{
	of.close();
}
void Game::chooseRoles(std::array<int, 4> &roles)
{
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(roles.begin(), roles.end(), std::default_random_engine(seed));
}
void Game::VulturSem(Player & player1, Player & player2)
{
		std::string ply;
		player1.copiereCarti(player2);
		ply = "Player" + std::to_string(player1.getNrPlayer()) + "  took all the cards of the Player" + std::to_string(player2.getNrPlayer());
		logger.log(ply, Logger::Level::Info);
		ply.clear();
}
void Game::ifPaulRegret(Player& player1, Player& player2, Player& player3, Player& player4)
{
	if (player1.getCharacterName() == Characters::Name::PaulRegret)
	{
		player2.increaseDistance(player1.getNrPlayer() - 1);
		player3.increaseDistance(player1.getNrPlayer() - 1);
		player4.increaseDistance(player1.getNrPlayer() - 1);
	}
	else if (player2.getCharacterName() == Characters::Name::PaulRegret)
	{
		player1.increaseDistance(player2.getNrPlayer() - 1);
		player3.increaseDistance(player2.getNrPlayer() - 1);
		player4.increaseDistance(player2.getNrPlayer() - 1);
	}
	else if (player3.getCharacterName() == Characters::Name::PaulRegret)
	{
		player1.increaseDistance(player3.getNrPlayer() - 1);
		player2.increaseDistance(player3.getNrPlayer() - 1);
		player4.increaseDistance(player3.getNrPlayer() - 1);
	}
	else if (player4.getCharacterName() == Characters::Name::PaulRegret)
	{
		player1.increaseDistance(player4.getNrPlayer() - 1);
		player2.increaseDistance(player4.getNrPlayer() - 1);
		player3.increaseDistance(player4.getNrPlayer() - 1);
	}
}
void Game::ifRooseDoolan(Player& player1, Player& player2, Player& player3, Player& player4)
{

	if (player1.getCharacterName() == Characters::Name::RoseDoolan)
	{
		for (int index = 0; index < nrPlayers; index++)
			if (index != player1.getNrPlayer() - 1)
				player1.decreaseDistance(index);
	}
	else if (player2.getCharacterName() == Characters::Name::RoseDoolan)
	{
		for (int index = 0; index < nrPlayers; index++)
			if (index != player2.getNrPlayer() - 1)
				player2.decreaseDistance(index);
	}
	else if (player3.getCharacterName() == Characters::Name::RoseDoolan)
	{
		for (int index = 0; index < nrPlayers; index++)
			if (index != player3.getNrPlayer() - 1)
				player3.decreaseDistance(index);
	}
	else if (player4.getCharacterName() == Characters::Name::RoseDoolan)
	{
		for (int index = 0; index < nrPlayers; index++)
			if (index != player3.getNrPlayer() - 1)
				player3.decreaseDistance(index);
	}
}

void Game::verificare(Player& player1, Player& player2, Player& player3, Player& player4)
{
	ifPaulRegret(player1, player2, player3, player4);
	ifRooseDoolan(player1, player2, player3, player4);
}

void Game::swapPlayer(Player& player1, Player& player2)
{
	std::swap(player1, player2);
	int aux = player1.getNrPlayer();
	player1.setNrPlayer(player2.getNrPlayer());
	player2.setNrPlayer(aux);
	player1.distanceCalculation(nrPlayers);
	player2.distanceCalculation(nrPlayers);
}

void Game::whoIsSherif(Player & player1, Player & player2, Player & player3, Player & player4)
{
	if (player1.getRole() != Roles::Rol::Sheriff)
	{
		if (player2.getRole() == Roles::Rol::Sheriff)
		{
			swapPlayer(player1, player2);
		}
		else if (player3.getRole() == Roles::Rol::Sheriff)
		{
			swapPlayer(player1, player3);
		}
		else if (player4.getRole() == Roles::Rol::Sheriff)
		{
			swapPlayer(player1, player4);
		}
	}
}

void Game::distanceByMustang(Player & player1, Player & player2, Player & player3, const int & nrPlayer, const int& sem)
{
	if (sem == 1)
	{
		player2.increaseDistance(nrPlayer - 1);
		player3.increaseDistance(nrPlayer - 1);
		player1.increaseDistance(nrPlayer - 1);
	}
	else
		if (sem == 0)
		{
			player2.decreaseDistance(nrPlayer - 1);
			player3.decreaseDistance(nrPlayer - 1);
			player1.decreaseDistance(nrPlayer - 1);
		}
}

void Game::distanceByLuneta(Player & player1, const int &sem)
{
	if (sem == 1)
	{
		for (int index = 0; index < nrPlayers; index++)
			if (index != player1.getNrPlayer() - 1)
				player1.decreaseDistance(index);
	}
	else
		if (sem == 0)
			for (int index = 0; index < nrPlayers; index++)
				if (index != player1.getNrPlayer() - 1)
					player1.increaseDistance(index);
}

void Game::distanceAfterDead(Player & player1, Player & player2, Player & player3)
{
	if (player1.isAlive())
	{
		if (player2.isAlive())
			player1.distanceCalculationAfterOnePlayerDead(player2.getNrPlayer(), nrPlayers);
		if (player3.isAlive())
			player1.distanceCalculationAfterOnePlayerDead(player3.getNrPlayer(), nrPlayers);
	}
	if (player2.isAlive())
	{
		if (player1.isAlive())
			player2.distanceCalculationAfterOnePlayerDead(player1.getNrPlayer(), nrPlayers);
		if (player3.isAlive())
			player2.distanceCalculationAfterOnePlayerDead(player3.getNrPlayer(), nrPlayers);
	}
	if (player3.isAlive())
	{
		if (player2.isAlive())
			player3.distanceCalculationAfterOnePlayerDead(player2.getNrPlayer(), nrPlayers);
		if (player1.isAlive())
			player3.distanceCalculationAfterOnePlayerDead(player1.getNrPlayer(), nrPlayers);
	}
	nrPlayers--;

}

bool Game::isSherifAlive(const Player & player2, const Player & player3, const Player & player4) const
{
	if (player2.isAlive() != true && player2.getRole() == Roles::Rol::Sheriff)
		return false;
	if (player3.isAlive() != true && player3.getRole() == Roles::Rol::Sheriff)
		return false;
	if (player4.isAlive() != true && player4.getRole() == Roles::Rol::Sheriff)
		return false;
	return true;
}

bool Game::OnlySherifAlive(const Player & player2, const Player & player3, const Player & player4) const
{
	if (player2.isAlive() != true && player2.getRole() != Roles::Rol::Sheriff && player3.isAlive() != true && player3.getRole() != Roles::Rol::Sheriff &&player4.isAlive() != true && player4.getRole() != Roles::Rol::Sheriff)
		return true;

	return false;
}

bool Game::isOutlaw(const Player& player2, const Player& player3, const Player& player4) const
{
	if (player2.isAlive() == true && player2.getRole() == Roles::Rol::Outlaw)
		return true;
	if (player3.isAlive() == true && player3.getRole() == Roles::Rol::Outlaw)
		return true;
	if (player4.isAlive() == true && player4.getRole() == Roles::Rol::Outlaw)
		return true;
	return false;
}

void Game::takeACard(Player & player)
{
	std::string ply;
	if (unusedCards.sizePackage() == 0)
	{
		unusedCards = std::move(discardedCards);
		unusedCards.shuffleVector();
	}
	player.addCard(unusedCards.pickCards());
	ply = "Player" + std::to_string(player.getNrPlayer()) + " take a card";
	logger.log(ply, Logger::Level::Info);
	ply.clear();
}

void Game::butoiAction(Player& player1, Player& player2, Player& player3, Player& player4,Cards& butoi)
{
	Functions wait;
	std::string ply;
	discardedCards.addCard(std::move(butoi));
	Cards card = unusedCards.pickCards();
	if (card.getNumber().second == Cards::symbol::Hearts)
	{
		std::cout << "Ratat!:p\n";
		ply = "Player" + std::to_string(player2.getNrPlayer()) + " used a barrel and escaped the bang";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
		discardedCards.addCard(std::move(card));
		Functions wait; wait.wait(5);
	}
	else
	{
		discardedCards.addCard(std::move(card));
		ply = "Player" + std::to_string(player2.getNrPlayer()) + " used a barrel but did not escape the bang";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
		Cards ratat = player2.isRatat();
		if (ratat.getName() == "Ratat!")
		{
			ratatAction(player2, ratat);
			ply = "Player" + std::to_string(player2.getNrPlayer()) + " used Ratat! and escape the bang";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
		}
		else
			if (player2.getCharacterName() == Characters::Name::CalamityJanet)
			{
				Cards bang = player2.isBang();
				if (bang.getName() == "Bang!")
				{
					ratatAction(player2, bang);
					ply = "Player" + std::to_string(player2.getNrPlayer()) + " used Bang! and escape the bang";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
			}
			else
			{
				player2.updateLives(0);
				std::cout << "BANG!" << std::endl;
				wait.wait(5);
				if (player2.getCharacterName() == Characters::Name::BartCassidy && player2.isAlive() == true)
				{
					takeACard(player2);
				}
				if (player2.getCharacterName() == Characters::Name::ElGringo && player2.isAlive() == true)
				{
					isElGringo(player1, player2);
				}
				if (player2.isAlive() != true)
				{
					Cards beer = player2.isBeer();
					if (beer.getName() == "Bere")
					{
						discardedCards.addCard(beer);
						if (player2.getCharacterName() == Characters::Name::SuzyLafayette && player2.getNumberOfCards() == 0)
						{
							takeACard(player2);
						}
						player2.updateLives(1);
						ply = "Player" + std::to_string(player2.getNrPlayer()) + " used a beer";
						logger.log(ply, Logger::Level::Info);
						ply.clear();
					}
					else
						if (player2.getCharacterName() == Characters::Name::SidKetchum && player2.getNumberOfCards() >= 2)
						{
							isSidKetchum(player2);
						}
						else
						{
							if (player1.getCharacterName() == Characters::Name::VultureSam)
							{
								VulturSem(player1, player2);
								distanceAfterDead(player1, player3, player4);
							}
							else if (player3.getCharacterName() == Characters::Name::VultureSam && player3.isAlive()==true)
							{
								VulturSem(player3, player2);
								distanceAfterDead(player1, player3, player4);
							}
							else if (player4.getCharacterName() == Characters::Name::VultureSam &&player3.isAlive()==true)
							{
								VulturSem(player4, player2);
								distanceAfterDead(player1, player3, player4);
							}
							else {
								distanceAfterDead(player1, player3, player4);
								player2.discardALLCards(discardedCards);
							}
						}
				}
			}
	}

}
void Game::ratatAction(Player& player, const Cards& ratat)
{
	std::string ply;
	discardedCards.addCard(std::move(ratat));
	if (player.getCharacterName() == Characters::Name::SuzyLafayette && player.getNumberOfCards() == 0)
	{
		takeACard(player);
	}
	std::cout << "Ratat!:p\n";
	ply = "Player" + std::to_string(player.getNrPlayer()) + " escape from a Bang";
	logger.log(ply, Logger::Level::Info);
	ply.clear();
	Functions wait; wait.wait(5);
}
void Game::isElGringo(Player& player1, Player& player2)
{
	std::string ply;
	if (player1.getNumberOfCards() != 0)
	{
		system("cls");
		std::cout << "Player" << player2.getNrPlayer() << " a primit un BANG! de la Player" << player1.getNrPlayer() << "\nPlayer" << player2.getNrPlayer() << " alege o cartea de la Player" << player1.getNrPlayer();
		std::cout << "\nAlege un umar intre 1 si " << player1.getNumberOfCards() << std::endl;
		int cart;
		while (true)
		{
			std::cin >> cart;
			if ((cart<1 || cart>player1.getNumberOfCards()))
			{
				std::cout << "Nu ati ales o caret existanta." << std::endl;
			}
			else break;
		}
		player2.addCard(player1.discardCard(cart - 1));
		ply = "Player" + std::to_string(player2.getNrPlayer()) + "  takes a card from Player" + std::to_string(player1.getNrPlayer());
		logger.log(ply, Logger::Level::Info);
		ply.clear();
	}
}
void Game::isSidKetchum(Player& player)
{
	std::string ply;
	std::cout << "-------------Esti player" << player.getNrPlayer() << "\n\n";
	player.showCards(1);
	std::cout << "\n\nAlege doua carti sa decartezi pentru a primi o viata\n";
	int c1, c2;
	while (true)
	{
		std::cout << "Cartea 1: "; std::cin >> c1;
		std::cout << "Cartea 2: "; std::cin >> c2;
		if ((c1<1 || c1>player.getNumberOfCards()) && (c2 < 1 || c2 < player.getNumberOfCards()))
		{
			std::cout << "Nu ati ales o caret existanta." << std::endl;
		}
		else break;
	}
	if (c1 < c2)
		std::swap(c1, c2);
	discardedCards.addCard(player.discardCard(c1 - 1));
	discardedCards.addCard(player.discardCard(c2 - 1));
	player.updateLives(1);
	ply = "Player" + std::to_string(player.getNrPlayer()) + "  discarded card number " + std::to_string(c1) + " and " + std::to_string(c2) + " to take a life back";
	logger.log(ply, Logger::Level::Info);
	ply.clear();
}
void Game::bangAction(Player & player1, Player & player2, Player & player3, Player & player4, const int& nrCarte, int &tragere)
{
	Functions wait;
	std::string ply;
	if (player1.getCharacterName() != Characters::Name::SlabTheKiller)
	{
		if (player2.isAlive() == true)
		{
			tragere++;
			discardedCards.addCard(player1.discardCard(nrCarte - 1));
			if (player2.getCharacterName() == Characters::Name::Jourdannais)
			{
				Cards butoi = unusedCards.pickCards();
				if (butoi.getNumber().second == Cards::symbol::Hearts)
				{
					ratatAction(player2, butoi);
					std::cout << "Player is Jourdannais and escaped the bang with a Heart" << std::endl;
					wait.wait(5);
					ply = "Player" + std::to_string(player2.getNrPlayer()) + " is Jourdannais and escaped the bang with a Heart";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				else
				{
					discardedCards.addCard(std::move(butoi));
					std::cout << "BANG!" << std::endl;
					ply = "Player" + std::to_string(player2.getNrPlayer()) + " is Jourdannais but did not escape the bang";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					Cards butoi = player2.isButoi();
					if (butoi.getName() == "Butoi")
					{
						butoiAction(player1, player2, player3, player4,butoi);
					}
				}
			}
			else
			{
				Cards butoi = player2.isButoi();
				if (butoi.getName() == "Butoi")
				{
					butoiAction(player1, player2, player3, player4, butoi);
				}
				else
				{
					Cards ratat = player2.isRatat();
					if (ratat.getName() == "Ratat!")
					{
						ratatAction(player2, ratat);
					}
					else
						if (player2.getCharacterName() == Characters::Name::CalamityJanet)
						{
							Cards bang = player2.isBang();
							if (bang.getName() == "Bang!")
							{
								ratatAction(player2, bang);
							}
						}
						else
						{
							player2.updateLives(0);
							std::cout << "BANG!" << std::endl;
							wait.wait(5);
							if (player2.getCharacterName() == Characters::Name::BartCassidy && player2.isAlive() == true)
							{
								takeACard(player2);
							}
							if (player2.getCharacterName() == Characters::Name::ElGringo && player2.isAlive() == true)
							{
								isElGringo(player1, player2);
							}
							if (player2.isAlive() != true)
							{
								Cards beer = player2.isBeer();
								if (beer.getName() == "Bere")
								{
									discardedCards.addCard(beer);
									if (player2.getCharacterName() == Characters::Name::SuzyLafayette && player2.getNumberOfCards() == 0)
									{
										takeACard(player2);
									}
									player2.updateLives(1);
									ply = "Player" + std::to_string(player2.getNrPlayer()) + " used a Beer";
									logger.log(ply, Logger::Level::Info);
									ply.clear();
								}
								else
									if (player2.getCharacterName() == Characters::Name::SidKetchum && player2.getNumberOfCards() >= 2)
									{
										isSidKetchum(player2);
									}
									else
									{
										if (player1.getCharacterName() == Characters::Name::VultureSam)
										{
											VulturSem(player1, player2);
											distanceAfterDead(player1, player3, player4);
										}
										else if (player3.getCharacterName() == Characters::Name::VultureSam && player3.isAlive() == true)
										{
											VulturSem(player3, player2);
											distanceAfterDead(player1, player3, player4);
										}
										else if (player4.getCharacterName() == Characters::Name::VultureSam &&player3.isAlive() == true)
										{
											VulturSem(player4, player2);
											distanceAfterDead(player1, player3, player4);
										}
										else {
											distanceAfterDead(player1, player3, player4);
											player2.discardALLCards(discardedCards);
										}
									}
							}
						}
					if (player1.getCharacterName() == Characters::Name::SuzyLafayette && player1.getNumberOfCards() == 0 && player1.isAlive()==true)
					{
						takeACard(player1);
					}
				}
			}
		}
		else
		{
			std::cout << "Playerul nu este in viata";
			logger.log("Player is not alive", Logger::Level::Warning);
			 wait.wait(5);
		}

	}
	else
	{
		if (player2.isAlive() == true)
		{
			tragere++;
			discardedCards.addCard(player1.discardCard(nrCarte - 1));
			if (player2.getCharacterName() != Characters::Name::CalamityJanet)
			{
				Cards c1 = player2.isRatat(), c2 = player2.isRatat();
				if (c1.getName() == "None")
				{
					player2.updateLives(0);
					std::cout << "BANG!" << std::endl;
					wait.wait(5);
					ply = "Player" + std::to_string(player2.getNrPlayer()) + " didn't have 2 Ratat! and he was shot";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				if (c1.getName() == "Ratat!" && c2.getName() == "None")
				{
					player2.addCard(std::move(c1));
					player2.updateLives(0);
					std::cout << "BANG!" << std::endl;
					wait.wait(5);
					ply = "Player" + std::to_string(player2.getNrPlayer()) + " didn't have 2 Ratat! and he was shot";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				if (c1.getName() == "Ratat!" && c2.getName() == "Ratat!")
				{
					discardedCards.addCard(std::move(c1));
					discardedCards.addCard(std::move(c2));
					std::cout << "Ratat!:p\n";
					ply = "Player" + std::to_string(player2.getNrPlayer()) + "  had 2 Ratat! and escape";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					wait.wait(5);
				}
				if (player2.isAlive() != true)
				{
					Cards beer = player2.isBeer();
					if (beer.getName() == "Bere")
					{
						discardedCards.addCard(beer);
						player2.updateLives(1);
						ply = "Player" + std::to_string(player2.getNrPlayer()) + " used a Beer";
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						if (player2.getCharacterName() == Characters::Name::SuzyLafayette && player2.getNumberOfCards() == 0 && player2.isAlive()==true)
						{
							takeACard(player2);
						}
					}
					else
						if (player2.getCharacterName() == Characters::Name::SidKetchum && player2.getNumberOfCards() >= 2 )
						{
							isSidKetchum(player2);
						}
						else if (player3.getCharacterName() == Characters::Name::VultureSam && player3.isAlive() == true)
						{
							VulturSem(player3, player2);
							distanceAfterDead(player1, player3, player4);
						}
						else if (player4.getCharacterName() == Characters::Name::VultureSam &&player3.isAlive() == true)
						{
							VulturSem(player4, player2);
							distanceAfterDead(player1, player3, player4);
						}
						else {
							distanceAfterDead(player1, player3, player4);
							player2.discardALLCards(discardedCards);
						}
				}
			}
			else
			{
				Cards c1 = player2.isRatat(), c2 = player2.isRatat(), c3 = player2.isBang(), c4 = player2.isBang();
				if (c1.getName() == "None" && c3.getName() == "None")
				{
					player2.updateLives(0);
					std::cout << "BANG!" << std::endl;
					wait.wait(5);
					ply = "Player" + std::to_string(player2.getNrPlayer()) + " didn't have 2 Ratat! and he was shot";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				if (c1.getName() == "Ratat!" && c2.getName() == "None" && c3.getName() == "None" && c4.getName() == "None")
				{
					player2.addCard(std::move(c1));
					player2.updateLives(0);
					std::cout << "BANG!" << std::endl;
					wait.wait(5);
					ply = "Player" + std::to_string(player2.getNrPlayer()) + " didn't have 2 Ratat! and he was shot";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				if (c3.getName() == "Bang!" && c4.getName() == "None" && c1.getName() == "None" && c2.getName() == "None")
				{
					player2.addCard(std::move(c3));
					player2.updateLives(0);
					std::cout << "BANG!" << std::endl;
					wait.wait(5);
					ply = "Player" + std::to_string(player2.getNrPlayer()) + " didn't have 2 Ratat! and he was shot";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				if (c1.getName() == "Ratat!" && c2.getName() == "Ratat!")
				{
					discardedCards.addCard(std::move(c1));
					discardedCards.addCard(std::move(c2));
					if (c3.getName() == "Bang!")
						player2.addCard(std::move(c3));
					if (c4.getName() == "Bang!")
						player2.addCard(std::move(c4));
					std::cout << "Ratat!:p\n";
					ply = "Player" + std::to_string(player2.getNrPlayer()) + "  had 2 Ratat! and escape";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					wait.wait(5);
				}
				if (c3.getName() == "Bang!" && c4.getName() == "Bang!")
				{
					discardedCards.addCard(std::move(c3));
					discardedCards.addCard(std::move(c4));
					if (c1.getName() == "Ratat!")
						player2.addCard(std::move(c1));
					if (c2.getName() == "Ratat!")
						player2.addCard(std::move(c2));
					std::cout << "Ratat!:p\n";
					ply = "Player" + std::to_string(player2.getNrPlayer()) + "  had 2 Ratat! and escape";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					 wait.wait(5);
				}
				if (c1.getName() == "Ratat!" && c2.getName() == "None" && c3.getName() == "Bang!")
				{
					discardedCards.addCard(std::move(c1));
					discardedCards.addCard(std::move(c3));
					if (c4.getName() == "Bang!")
						player2.addCard(std::move(c4));
					std::cout << "Ratat!:p\n";
					ply = "Player" + std::to_string(player2.getNrPlayer()) + "  had 2 Ratat! and escape";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					wait.wait(5);
				}
				if (player2.isAlive() != true)
				{
					Cards beer = player2.isBeer();
					if (beer.getName() == "Bere")
					{
						discardedCards.addCard(beer);
						player2.updateLives(1);
						ply = "Player" + std::to_string(player2.getNrPlayer()) + " used a Beer";
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						if (player2.getCharacterName() == Characters::Name::SuzyLafayette && player2.getNumberOfCards() == 0 && player2.isAlive() == true)
						{
							takeACard(player2);
						}
					}
					else
						if (player2.getCharacterName() == Characters::Name::SidKetchum && player2.getNumberOfCards() >= 2)
						{
							isSidKetchum(player2);
						}
						else if (player3.getCharacterName() == Characters::Name::VultureSam && player3.isAlive() == true)
						{
							VulturSem(player3, player2);
							distanceAfterDead(player1, player3, player4);
						}
						else if (player4.getCharacterName() == Characters::Name::VultureSam &&player3.isAlive() == true)
						{
							VulturSem(player4, player2);
							distanceAfterDead(player1, player3, player4);
						}
						else {
							distanceAfterDead(player1, player3, player4);
							player2.discardALLCards(discardedCards);
						}
				}
			}
		}
		else
		{
			std::cout << "Playerul nu este in viata";
			logger.log("Player is not alive", Logger::Level::Warning);
			Functions wait; wait.wait(5);
		}
	}
}

void Game::panicaAction(Player& player1, Player& player2, Player& player3, Player& player4, const int & nrCarte)
{
	std::string ply;
	system("cls");
	player2.showCards(0);
	int pachet;
	std::cout << "1: Carti in mana\n";
	std::cout << "2: Carti activate\n";
	while (true)
	{
		std::cin >> pachet;
		if (pachet!=1&&pachet!=2)
		{
			std::cout << "Nu ati ales o optiune valida" << std::endl;
		}
		else if (pachet == 2 && player2.getNrActiveCards() == 0)
		{
			std::cout << "Jucatorul nu are carti activate, alegeti prima optiune" << std::endl;
		}
		else break;
	}
	switch (pachet)
	{
	case 1: {
		std::cout << "Alege Cartea\n";
		std::cout << "Alege un umar intre 1 si " << player2.getNumberOfCards() << std::endl;
		int cart;
		while (true)
		{
			std::cin >> cart;
			if (cart<1&&cart>player2.getNumberOfCards())
			{
				std::cout << "Nu ati ales o carte existanta." << std::endl;
			}
			else break;
		}
		ply = "Player" + std::to_string(player2.getNrPlayer()) + " chose to take a card from Player" + std::to_string(player2.getNrPlayer()) + " hands";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
		player1.addCard(player2.discardCard(cart - 1));
	}break;
	case 2: {
		Functions wait;
		std::string cart;
		while (true)
		{
			while (true)
			{
				std::cout << "Alege NUMELE cartii\n";
				std::cin >> cart;
				if (cart != "Mustang"&&cart != "Luneta"&&cart != "Butoi")
				{
					std::cout << "Nu ati ales cartea potrivita" << std::endl;
				}
				else break;
			}
			Cards card = player2.discardActiveCard(cart);
			if (card.getName() != "None")
			{
				if (card.getName() == "Mustang")
					distanceByMustang(player1, player3, player4, player2.getNrPlayer(), 0);
				if (card.getName() == "Luneta")
					distanceByLuneta(player2, 0);
				player1.addCard(std::move(card));
				ply = "Player" + std::to_string(player2.getNrPlayer()) + " chose to take " + cart + " from Player" + std::to_string(player2.getNrPlayer());
				logger.log(ply, Logger::Level::Info);
				ply.clear();
				break;
			}
			else {
				std::cout << "Aceasta carte nu este activata" << std::endl;
				wait.wait(5);
			}
		}
	}break;
	}
}

void Game::catBalouAction(Player & player1, Player & player2, Player& player3, Player& player4)
{
	std::string ply;
	system("cls");
	std::cout << "Esti player"+player2.getNrPlayer()<<" si trebuie sa decartezi o carte\n";
	player2.showCards(2);
	int pachet = 0;
	std::cout << "1: Carti in mana\n";
	std::cout << "2: Carti activate\n";
	while (true)
	{
		std::cin >> pachet;
		if (pachet!=1&&pachet!=2)
		{
			std::cout << "Nu ati ales o optiune valida" << std::endl;
		}
		else if (pachet == 2 && player2.getNrActiveCards() == 0)
		{
			std::cout << "Nu aveti carti activate, alegeti optiunea 1" << std::endl;
		}
		else break;
	}
	switch (pachet)
	{
	case 1: {
		std::cout << "Alege Cartea\n";
		int cart;
		
		while (true)
		{
			std::cin >> cart;
			if (cart<1||cart>player2.getNumberOfCards())
			{
				std::cout << "Nu ati ales o caret existanta." << std::endl;
			}
			else break;
		}
		ply = "Player" + std::to_string(player2.getNrPlayer()) + " chose to discard card number " + std::to_string(cart) + " from his hands";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
		discardedCards.addCard(player2.discardCard(cart - 1));
	}break;
	case 2: {
		std::cout << "Alege NUMELE cartii\n";
		std::string cart;
		Functions wait;
		while (true)
		{
			while (true)
			{
				std::cin >> cart;
				if (cart != "Mustang"&&cart != "Luneta"&&cart != "Butoi")
				{
					std::cout << "Nu ati ales o carte care poate fi activata." << std::endl;
					wait.wait(5);
				}
				else break;
			}
			Cards card = player2.discardActiveCard(cart);
			if (card.getName() != "None")
			{
				if (card.getName() == "Mustang")
					distanceByMustang(player1, player3, player4, player2.getNrPlayer(), 0);
				if (card.getName() == "Luneta")
					distanceByLuneta(player2, 0);
				discardedCards.addCard(std::move(card));
				ply = "Player" + std::to_string(player2.getNrPlayer()) + " chose to discard " + cart;
				logger.log(ply, Logger::Level::Info);
				ply.clear();
				break;
			}
			else {
				std::cout << "Nu ai aceasta carte activata" << std::endl;
				wait.wait(5);
			}
		}
	}break;
	}
}

void Game::gatlingActionForOnPlayer(Player& player1, Player& player2, Player& player3, Player& player4)
{
	std::string ply;
	player2.updateLives(0);
	if (player2.getCharacterName() == Characters::Name::BartCassidy && player2.isAlive() == true)
	{
		takeACard(player2);
		ply = "Player" + std::to_string(player2.getNrPlayer()) + "  lost a life";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
	}
	if (player2.getCharacterName() == Characters::Name::ElGringo && player2.isAlive() == true)
	{
		isElGringo(player1, player2);
	}
	if (player2.getNumberOfLives() == 0)
	{
		Cards beer = player2.isBeer();
		if (beer.getName() == "Bere")
		{
			discardedCards.addCard(std::move(beer));
			player2.updateLives(1);
			ply = "Player" + std::to_string(player2.getNrPlayer()) + "  used a beer";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
		}
		else
			if (player2.getCharacterName() == Characters::Name::SidKetchum && player2.getNumberOfCards() >= 2)
			{
				isSidKetchum(player2);
			}
			else
				if (player1.getCharacterName() == Characters::Name::VultureSam)
				{
					VulturSem(player1, player2);
					distanceAfterDead(player1, player3, player4);
				}
				else if (player3.getCharacterName() == Characters::Name::VultureSam && player3.isAlive() == true)
				{
					VulturSem(player3, player2);
					distanceAfterDead(player1, player3, player4);
				}
				else if (player4.getCharacterName() == Characters::Name::VultureSam &&player3.isAlive() == true)
				{
					VulturSem(player4, player2);
					distanceAfterDead(player1, player3, player4);
				}
				else
				{
					player2.discardALLCards(discardedCards);
					distanceAfterDead(player1, player3, player4);
				}
	}
}
void Game::gatlingAction(Player & player1, Player & player2, Player & player3, Player & player4)
{
	gatlingActionForOnPlayer(player1, player2, player3, player4);
	gatlingActionForOnPlayer(player1, player3, player2, player4);
	gatlingActionForOnPlayer(player1, player4, player3, player2);
}

void Game::salonAction(Player & player1, Player & player2, Player & player3, Player & player4)
{
	std::string ply;
	if (player1.getNumberOfLives() < 5)
	{
		player1.updateLives(1);
		ply = "Player" + std::to_string(player1.getNrPlayer()) + " took life from Salon ";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
	}
	if (player2.getNumberOfLives() < 5 && player2.isAlive()==true)
	{
		player2.updateLives(1);
		ply = "Player" + std::to_string(player2.getNrPlayer()) + " took life from Salon ";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
	}
	if (player3.getNumberOfLives() < 5 && player3.isAlive()==true)
	{
		player3.updateLives(1);
		ply = "Player" + std::to_string(player3.getNrPlayer()) + " took life from Salon ";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
	}
	if (player4.getNumberOfLives() < 5 && player4.isAlive()==true)
	{
		player4.updateLives(1);
		ply = "Player" + std::to_string(player4.getNrPlayer()) + " took life from Salon ";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
	}
}

void Game::startTurnPlayer(Player & player1, Player & player2, Player & player3, Player & player4)
{
	std::string ply;
	const int nrP2 = player2.getNrPlayer();
	const int nrP3 = player3.getNrPlayer(), nrP4 = player4.getNrPlayer();
	if (player1.getCharacterName() == Characters::Name::JesseJones)
	{
		int op;
		while (true)
		{
			system("cls");
			std::cout << "Vrei sa tragi o carte de la un jucator\n";
			std::cout << "1:Da\n";
			std::cout << "2:Nu\n";
			std::cin >> op;
			if (op != 1 &&op != 2)
			{
				std::cout << "Nu ai ales o optiune valida" << std::endl;
				Functions f; f.wait(5);
			}
			else break;
		}
		switch (op)
		{
		case 1:
		{
			std::cout << "Alege jucatorul\n";
			int jucator;
			std::cin >> jucator;
			ply = "Player" + std::to_string(player1.getNrPlayer()) + "  chose to take a card from Player" + std::to_string(jucator);
			logger.log(ply, Logger::Level::Info);
			ply.clear();
			if (jucator == nrP2)
			{
				system("cls");
				std::cout << "Alege Cartea\n";
				std::cout << "Alege un umar intre 1 si " << player2.getNumberOfCards() << std::endl;
				int cart;
				while (true)
				{
					std::cin >> cart;
					if (cart<1 || cart>player2.getNumberOfCards())
					{
						std::cout << "Nu ati ales o caret existanta." << std::endl;
					}
					else break;
				}
				player1.addCard(player2.discardCard(cart - 1));
			}
			if (jucator == nrP3)
			{
				system("cls");
				std::cout << "Alege Cartea\n";
				std::cout << "Alege un umar intre 1 si " << player3.getNumberOfCards() << std::endl;
				int cart;
				while (true)
				{
					std::cin >> cart;
					if (cart<1||cart>player3.getNumberOfCards())
					{
						std::cout << "Nu ati ales o caret existanta." << std::endl;
					}
					else break;
				}
				player1.addCard(player3.discardCard(cart - 1));
			}
			if (jucator == nrP4)
			{
				system("cls");
				std::cout << "Alege Cartea\n";
				std::cout << "Alege un umar intre 1 si " << player4.getNumberOfCards() << std::endl;
				int cart;
				
				while (true)
				{
					std::cin >> cart;
					if (cart<1||cart>player4.getNumberOfCards())
					{
						std::cout << "Nu ati ales o carte existanta." << std::endl;
					}
					else break;
				}
				player1.addCard(player4.discardCard(cart - 1));
			}
			player1.addCard(unusedCards.pickCards());
		}break;
		case 2: {
			player1.addCard(unusedCards.pickCards());
			player1.addCard(unusedCards.pickCards());
			ply = "Player" + std::to_string(player1.getNrPlayer()) + " took 2 cards";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
		}break;
		}
	}
	if (player1.getCharacterName() != Characters::Name::LuckyDuke && player1.getCharacterName() != Characters::Name::KitCarlson && player1.getCharacterName() != Characters::Name::JesseJones &&player1.getCharacterName() != Characters::Name::PedroRamirez)
	{
		player1.addCard(unusedCards.pickCards());
		player1.addCard(unusedCards.pickCards());
		ply = "Player" + std::to_string(player1.getNrPlayer()) + " took 2 cards";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
		if (player1.getCharacterName() == Characters::Name::BlackJack)
		{
			if (player1.getCard(player1.getNumberOfCards() - 1).getNumber().second == Cards::symbol::Hearts ||
				player1.getCard(player1.getNumberOfCards() - 1).getNumber().second == Cards::symbol::Tiles)
			{
				system("cls");
				std::cout << "A doua carte pe care ai tras-o era Caro sau Cupa si ai luat o carte in plus\n";
				player1.addCard(unusedCards.pickCards());
				ply = "Player" + std::to_string(player1.getNrPlayer()) + " took an extra card";
				logger.log(ply, Logger::Level::Info);
				ply.clear();
				Functions wait; wait.wait(5);
			}
		}
	}
	else
		if (player1.getCharacterName() == Characters::Name::LuckyDuke)
		{
			int ct = 0;
			while (ct < 2)
			{
				int op;
				Cards c1, c2;
				while (true)
				{
					system("cls");
					std::cout << "---------------Alege una dintre carti---------------\n";
					c1 = unusedCards.pickCards(), c2 = unusedCards.pickCards();
					std::cout << "\n----------1-----------\n";
					std::cout << c1;
					std::cout << "\n----------2-----------\n";
					std::cout << c2;
					std::cin >> op;
					if (op != 1 && op != 2)
					{
						std::cout << "Nu ai ales o optiune valida" << std::endl;
						Functions f; f.wait(5);
					}
					else break;
				}
				ply = "Player" + std::to_string(player1.getNrPlayer()) + " chose card number " + std::to_string(op);
				logger.log(ply, Logger::Level::Info);
				ply.clear();
				switch (op)
				{
				case 1:player1.addCard(std::move(c1)); discardedCards.addCard(std::move(c2)); break;
				case 2:player1.addCard(std::move(c2)); discardedCards.addCard(std::move(c1)); break;
				}
				ct++;
			}
		}
		else
			if (player1.getCharacterName() == Characters::Name::KitCarlson)
			{
				int op;
				Cards c1, c2, c3;
				while (true)
				{
					std::cout << "---------------Alege carte pe care nu vrei sa o pastrezi---------------\n";
					c1 = unusedCards.pickCards(), c2 = unusedCards.pickCards(), c3 = unusedCards.pickCards();
					std::cout << "\n----------1-----------\n";
					std::cout << c1;
					std::cout << "\n----------2-----------\n";
					std::cout << c2;
					std::cout << "\n----------3-----------\n";
					std::cout << c3;				
					std::cin >> op;
					if (op != 1 && op != 2&&op!=3)
					{
						std::cout << "Nu ai ales o optiune valida" << std::endl;
						Functions f; f.wait(5);
					}
					else break;
				}
				ply = "Player" + std::to_string(player1.getNrPlayer()) + " chose not to keep card number " + std::to_string(op);
				logger.log(ply, Logger::Level::Info);
				ply.clear();
				switch (op)
				{
				case 1:player1.addCard(std::move(c2)); player1.addCard(std::move(c3)); discardedCards.addCard(std::move(c1)); break;
				case 2:player1.addCard(std::move(c1)); player1.addCard(std::move(c3)); discardedCards.addCard(std::move(c2)); break;
				case 3:player1.addCard(std::move(c2)); player1.addCard(std::move(c1)); discardedCards.addCard(std::move(c3)); break;
				}
			}
	if (player1.getCharacterName() == Characters::Name::PedroRamirez && discardedCards.getDiscardedCards().size() != 0)
	{
		discardedCards.showTheLastCard();
		int op;
		while (true)
		{
			system("cls");
			std::cout << "Vrei sa tragi ultima carte din teancul de decartate?\n";
			std::cout << "1:Da\n";
			std::cout << "2:Nu\n";
			std::cin >> op;
			if (op != 1 && op != 2)
			{
				std::cout << "Nu ai ales o optiune valida" << std::endl;
				Functions f; f.wait(5);
			}
			else break;
		}
		switch (op)
		{
		case 1: {
			ply = "Player" + std::to_string(player1.getNrPlayer()) + " chose take last card from discarted cards ";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
			player1.addCard(discardedCards.pickLastCard()); player1.addCard(unusedCards.pickCards()); 
		}break;
		case 2: {
			ply = "Player" + std::to_string(player1.getNrPlayer()) + " chose not to take last card from discarted cards ";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
			player1.addCard(unusedCards.pickCards()); player1.addCard(unusedCards.pickCards()); 
		}break;
		}
	}
}
void Game::run()
{
	//-----------Initializari--------------------------
	logger.log("Game Start", Logger::Level::Info);
	unusedCards.citire();
	logger.log("UnusedCards was read", Logger::Level::Info);
	std::cout << "\n\n\n\n\n\n\n\n\n\n\n\n";
	std::cout << "                                                 START GAME                               ";
	Functions wait; wait.wait(4500);
	system("cls");
	Characters characters;
	//Numbers for rols
	std::array <int, 4>roles = { 1,2,2,3 };
	chooseRoles(roles);

	Player player1, player2, player3, player4;
	player1 = constructPlayer(roles[0], characters, 1);
	player2 = constructPlayer(roles[1], characters, 2);
	player3 = constructPlayer(roles[2], characters, 3);
	player4 = constructPlayer(roles[3], characters, 4);
	whoIsSherif(player1, player2, player3, player4);
	verificare(player1, player2, player3, player4);
	logger.log("Players were initialaize", Logger::Level::Info);

	while (true)
	{
		system("cls");
		std::cout << "Player1 turn\n";
		logger.log("Player1 turn", Logger::Level::Info);
		if (player1.isAlive() == true)
			if (tunPlayer(player1, player2, player3, player4) == 2)
			{
				system("cls");
				std::cout << "\n\n\n\n\n------------------Serif winnn------------------------- \n\n\n\n\n";
				logger.log("Serif winnn", Logger::Level::Info);
				break;
			}
		system("cls");
		if (player2.isAlive())
		{
			std::cout << "Player2 turn\n";
			logger.log("Player2 turn", Logger::Level::Info);
			if (tunPlayer(player2, player1, player3, player4) == 1)
			{
				if (isOutlaw(player2, player3, player4) == true)
				{
					system("cls");
					std::cout << "\n\n\n\n\n------------------Outlaw winnn------------------------- \n\n\n\n\n";
					logger.log("Outlaw winnn", Logger::Level::Info);
				}
				else
				{
					system("cls");
					std::cout << "\n\n\n\n\n------------------Renegat winnn------------------------- \n";
					logger.log("Renegat winnn", Logger::Level::Info);
				}
				break;
			}
		}
		system("cls");
		if (player3.isAlive())
		{
			std::cout << "Player3 turn\n";
			logger.log("Player3 turn", Logger::Level::Info);
			if (tunPlayer(player3, player1, player2, player4) == 1)
			{
				if (isOutlaw(player2, player3, player4) == true)
				{
					system("cls");
					std::cout << "\n\n\n\n\n------------------Outlaw winnn------------------------- \n\n\n\n\n";
					logger.log("Outlaw winnn", Logger::Level::Info);
				}
				else
				{
					system("cls");
					std::cout << "\n\n\n\n\n------------------Renegat winnn------------------------- \n";
					logger.log("Renegat winnn", Logger::Level::Info);
				}
				break;
			}
		}
		system("cls");
		if (player4.isAlive())
		{
			std::cout << "Player4 turn\n";
			logger.log("Player4 turn", Logger::Level::Info);
				if (tunPlayer(player4, player1, player3, player2) == 1)
				{
					if (isOutlaw(player2, player3, player4) == true)
					{
						system("cls");
						std::cout << "\n\n\n\n\n------------------Outlaw winnn------------------------- \n\n\n\n\n";
						logger.log("Outlaw winnn", Logger::Level::Info);
					}
					else
					{
						system("cls");
						std::cout << "\n\n\n\n\n------------------Renegat winnn------------------------- \n";
						logger.log("Renegat winnn", Logger::Level::Info);
					}
					break;
				}
		}
	}
}

int Game::tunPlayer(Player & player1, Player & player2, Player & player3, Player & player4)
{
	Functions wait;
	int tragere = 0;
	const int nrP2 = player2.getNrPlayer();
	const int nrP3 = player3.getNrPlayer(), nrP4 = player4.getNrPlayer();
	if (unusedCards.sizePackage() <= 2)
	{
		unusedCards = std::move(discardedCards);
		unusedCards.shuffleVector();
	}
	startTurnPlayer(player1, player2, player3, player4);
	int ok = 1;
	while (ok != 0)
	{
		system("cls");
		std::cout << player1;
		std::cout << "\n--------------------------------------\n";
		std::cout << "Player" << player2.getNrPlayer() << " lives:: " << player2.getNumberOfLives() << std::endl;
		std::cout << "Player" << player3.getNrPlayer() << " lives:: " << player3.getNumberOfLives() << std::endl;
		std::cout << "Player" << player4.getNrPlayer() << " lives:: " << player4.getNumberOfLives() << std::endl;

		std::cout << "\n---------Discard Card-------------\n";
		discardedCards.showTheLastCard();
		std::cout << "\n--------------------------------------\n";
		std::cout << "1:Activeaza o carte(Arma,Butoi,Mustang,Luneta)\n";
		std::cout << "2:Joaca o carte\n";
		std::cout << "3:Decarteaza o carte\n";
		std::cout << "4:Incheie tura\n";
		if (player1.getCharacterName() == Characters::Name::SidKetchum)
		{
			std::cout << "***********************************************\n";
			std::cout << "* 5:Decarteaza 2 carti pentru a primi o viata *\n";
			std::cout << "***********************************************\n";
		}
		int op;
		while (true)
		{
			std::cin >> op;
			if (player1.getCharacterName() == Characters::Name::SidKetchum)
			{
				if (op < 1 || op>5)
				{
					std::cout << "Nu ai ales o optiune valida" << std::endl;
					Functions f; f.wait(5);
				}
				else break;
			}
			else if (op<1||op>4)
			{
				std::cout << "Nu ai ales o optiune valida" << std::endl;
				Functions f; f.wait(5);
			}
			else break;
		}
		std::string ply = "Player";
		std::string nrPly = std::to_string(player1.getNrPlayer());
		ply += nrPly + " chose option " + std::to_string(op);
		logger.log(ply, Logger::Level::Info);
		ply.clear();
		switch (op)
		{
		case 1: {
			std::cout << "Alegeti carte pe care doriti sa o activati\n";
			int nrCarte;
			while (true)
			{
				std::cin >> nrCarte;
				if (nrCarte<1 || nrCarte>player1.getNumberOfCards())
				{
					std::cout << "Nu ai ales o carte existanta" << std::endl;
				}
				else break;
			}
			if (player1.getCard(nrCarte - 1).isGun() == true)
			{
				Cards gun = player1.discardGun();
				if (gun.getName() != "None")
				{
					discardedCards.addCard(gun);
					ply = "Player" + nrPly + " discarded a weapon";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				player1.setGun(player1.discardCard(nrCarte - 1));
				ply = "Player" + nrPly + " activated a weapon";
				logger.log(ply, Logger::Level::Info);
				ply.clear();
			}
			else
				if (player1.getCard(nrCarte - 1).getName() == "Butoi" && player1.isButoi().getName() == "None")
				{
					player1.activeCard(nrCarte - 1);
					ply = "Player" + nrPly + " activated a Barrel";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				else
					if (player1.getCard(nrCarte - 1).getName() == "Butoi" && player1.isButoi().getName() != "None")
					{
						std::cout << "Nu poti activa decat o carte Butoi";
						logger.log("A barrel is already activated", Logger::Level::Warning);
						Functions wait; wait.wait(5);
						break;
					}
					else
						if (player1.getCard(nrCarte - 1).getName() == "Mustang" && player1.isMustang().getName() == "None")
						{
							player1.activeCard(nrCarte - 1);
							distanceByMustang(player2, player3, player4, player1.getNrPlayer(), 1);
							ply = "Player" + nrPly + " activated a Mustang";
							logger.log(ply, Logger::Level::Info);
							ply.clear();
						}
						else
							if (player1.getCard(nrCarte - 1).getName() == "Mustang" && player1.isMustang().getName() != "None")
							{
								std::cout << "Nu poti activa decat o carte Mustang";
								logger.log("A Mustang is already activated", Logger::Level::Warning);
								Functions wait; wait.wait(5);
								break;
							}
							else
								if (player1.getCard(nrCarte - 1).getName() == "Luneta"  && player1.isLuneta().getName() == "None")
								{
									player1.activeCard(nrCarte - 1);
									distanceByLuneta(player1, 1);
									ply = "Player" + nrPly + " activated a Luneta";
									logger.log(ply, Logger::Level::Info);
									ply.clear();
								}
								else
									if (player1.getCard(nrCarte - 1).getName() == "Luneta"  && player1.isLuneta().getName() != "None")
									{
										std::cout << "Nu poti activa decat o carte Luneta";
										logger.log("Luneta is already activated", Logger::Level::Warning);
										wait.wait(5);
										break;
									}
									else
									{
										std::cout << "Nu se poate activa\n";
										logger.log("This card can't be activated", Logger::Level::Warning);
										wait.wait(5);
										break;
									}
			if (player1.getCharacterName() == Characters::Name::SuzyLafayette && player1.getNumberOfCards() == 0)
			{
				takeACard(player1);

			}
		} break;
		case 2: {
			std::cout << "Alegeti carte pe care doriti sa o jucati\n";
			int nrCarte;
			while (true)
			{
				std::cin >> nrCarte;
				if (nrCarte<1 || nrCarte>player1.getNumberOfCards())
				{
					std::cout << "Nu ai ales o carte existanta" << std::endl;
					wait.wait(5);
				}
				else break;
			}
			ply = "Player" + nrPly + " chose card number " + std::to_string(nrCarte);
			logger.log(ply, Logger::Level::Info);
			ply.clear();
			if (player1.getCard(nrCarte - 1).getName() == "Bere")
			{
				if (player1.getNumberOfLives() < 5)
				{
					player1.updateLives(1);
					discardedCards.addCard(player1.discardCard(nrCarte - 1));
					ply = "Player" + nrPly + " used a beer to take a life back";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				else
				{
					std::cout << "Nu poti activa aceasta carte, ai deja numarul maxim de vieti\n";
					wait.wait(5);
					logger.log("This card can not be used because you have max numbers of lifes", Logger::Level::Warning);
				}
			}
			else
				if (player1.getCard(nrCarte - 1).getName() == "Salon")
				{
					discardedCards.addCard(player1.discardCard(nrCarte - 1));
					salonAction(player1, player2, player3, player4);
					ply = "Player" + nrPly + " used Salon ";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				else
					if (player1.getCard(nrCarte - 1).getName() == "Gatling")
					{
						discardedCards.addCard(player1.discardCard(nrCarte - 1));
						gatlingAction(player1, player2, player3, player4);
						ply = "Player" + nrPly + " used Gatling";
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						if (isSherifAlive(player2, player3, player4) == false)
						{
							ok = 0;
							return 1;
						}
						if (OnlySherifAlive(player2, player3, player4) == true)
						{
							ok = 0;
							return 2;
						}
					}
					else
						if (player1.getCard(nrCarte - 1).getName() == "Diligenta")
						{
							player1.addCard(unusedCards.pickCards());
							player1.addCard(unusedCards.pickCards());
							discardedCards.addCard(player1.discardCard(nrCarte - 1));
							ply = "Player" + nrPly + " used Diligenta";
							logger.log(ply, Logger::Level::Info);
							ply.clear();
						}
						else
							if (player1.getCard(nrCarte - 1).getName() == "Wells Fargo")
							{
								player1.addCard(unusedCards.pickCards());
								player1.addCard(unusedCards.pickCards());
								player1.addCard(unusedCards.pickCards());
								discardedCards.addCard(player1.discardCard(nrCarte - 1));
								ply = "Player" + nrPly + " used Wells Fargo";
								logger.log(ply, Logger::Level::Info);
								ply.clear();
							}
							else
								if (player1.getCard(nrCarte - 1).getName() == "Cat Balou")
								{
									std::cout << "Alege jucatorul\n";
									int jucator;
									while (true)
									{
										std::cin >> jucator;
										if (jucator<1 || jucator>nrPlayers)
										{
											std::cout << "Nu ai ales un jucator disponibil" << std::endl;
											wait.wait(5);
										}else
										if (jucator == player1.getNrPlayer()) {
											std::cout << "Nu poti folosi aceasta carte pe tine" << std::endl;
											wait.wait(5);
										}
										else break;
									}
									if (jucator == nrP2 && player2.getNumberOfLives() != 0)
									{
										discardedCards.addCard(player1.discardCard(nrCarte - 1));
										catBalouAction(player1, player2, player3, player4);
										ply = "Player" + nrPly + " used Cat Balou on player" + std::to_string(jucator);
										logger.log(ply, Logger::Level::Info);
										ply.clear();
									}
									else
										if (jucator == nrP3 && player3.getNumberOfLives() != 0)
										{
											discardedCards.addCard(player1.discardCard(nrCarte - 1));
											catBalouAction(player1, player3, player2, player4);
											ply = "Player" + nrPly + " used Cat Balou on player" + std::to_string(jucator);
											logger.log(ply, Logger::Level::Info);
											ply.clear();
										}
										else
											if (jucator == nrP4 && player4.getNumberOfLives() != 0)
											{
												discardedCards.addCard(player1.discardCard(nrCarte - 1));
												catBalouAction(player1, player4, player2, player3);
												ply = "Player" + nrPly + " used Cat Balou on player" + std::to_string(jucator);
												logger.log(ply, Logger::Level::Info);
												ply.clear();
											}
											else
											{
												std::cout << "Playerul nu este in viata";
												logger.log("Player is not alive", Logger::Level::Warning);
												wait.wait(5);
											}
								}
								else
									if (player1.getCard(nrCarte - 1).getName() == "Panica!")
									{
										while (true)
										{
											std::cout << "Alege jucatorul\n";
											int jucator;
											while (true)
											{
												std::cin >> jucator;
												if (jucator<1 || jucator>nrPlayers)
												{
													std::cout << "Nu ati ales un jucator valid." << std::endl;
													wait.wait(5);
												}else
												if (jucator == player1.getNrPlayer()) {
													std::cout << "Nu poti folosi aceasta carte pe tine" << std::endl;
													wait.wait(5);
												}
												else break;
											}
											if (jucator == player1.getNrPlayer())
											{
												std::cout << "Nu poti folosi aceasta carte pe tine\n";
												logger.log("You can't use Panica! on you", Logger::Level::Warning);
												Functions wait; wait.wait(5);
											}
											else
											{
												if (player1.getDistance(jucator - 1) <= 1)
												{
													if (jucator == nrP2 && player2.getNumberOfLives() != 0)
													{
														panicaAction(player1, player2, player3, player4, nrCarte);
														discardedCards.addCard(player1.discardCard(nrCarte - 1));
														ply = "Player" + nrPly + " used Panica! on player" + std::to_string(jucator);
														logger.log(ply, Logger::Level::Info);
														ply.clear();
													}
													if (jucator == nrP3 && player3.getNumberOfLives() != 0)
													{
														panicaAction(player1, player3, player2, player4, nrCarte);
														discardedCards.addCard(player1.discardCard(nrCarte - 1));
														ply = "Player" + nrPly + " used Panica! on player" + std::to_string(jucator);
														logger.log(ply, Logger::Level::Info);
														ply.clear();
													}
													if (jucator == nrP4 && player4.getNumberOfLives() != 0)
													{
														panicaAction(player1, player4, player2, player3, nrCarte);
														discardedCards.addCard(player1.discardCard(nrCarte - 1));
														ply = "Player" + nrPly + " used Panica! on player" + std::to_string(jucator);
														logger.log(ply, Logger::Level::Info);
														ply.clear();
													}
													break;
												}
												else
												{
													std::cout << "Nu poti folosi aceasta carte pe jucatorul selectat\n";
													logger.log("You can't use Panica! on this player,is too far", Logger::Level::Warning);
													wait.wait(5);
												}
											}
										}
									}
									else
										if ((player1.getCard(nrCarte - 1).getName() == "Bang!" || (player1.getCard(nrCarte - 1).getName() == "Ratat!") && player1.getCharacterName() == Characters::Name::CalamityJanet))
										{
											if (tragere >= player1.getNrTrageri())
											{
												std::cout << "Ai folosit numarul maxim de carti bang pe tura\n";
												logger.log("You used the maxim numbers of bang", Logger::Level::Warning);
												Functions wait; wait.wait(5);
											}
											else
											{
												std::cout << "Alege jucatorul\n";
												int jucator;
												while (true)
												{
													std::cin >> jucator;
													if (jucator<1 || jucator>nrPlayers)
													{
														std::cout << "Nu ati ales un jucator valid." << std::endl;
														wait.wait(5);
													}
													else break;
												}
												if (jucator == player1.getNrPlayer())
												{
													std::cout << "Nu te poti impusca singur\n";
													logger.log("You can't use Bang! on you", Logger::Level::Warning);
													Functions wait; wait.wait(5);
												}
												else
												{
													if (player1.getDistance(jucator - 1) <= player1.getGun().getDistance())
													{
														if (jucator == nrP2)
														{
															ply = "Player" + nrPly + " used Bang! on player" + std::to_string(jucator);
															logger.log(ply, Logger::Level::Info);
															ply.clear();
															bangAction(player1, player2, player3, player4, nrCarte, tragere);
														}
														else
															if (jucator == nrP3)
															{
																ply = "Player" + nrPly + " used Bang! on player" + std::to_string(jucator);
																logger.log(ply, Logger::Level::Info);
																ply.clear();
																bangAction(player1, player3, player2, player4, nrCarte, tragere);
															}
															else
																if (jucator == nrP4)
																{
																	ply = "Player" + nrPly + " used Bang! on player" + std::to_string(jucator);
																	logger.log(ply, Logger::Level::Info);
																	ply.clear();
																	bangAction(player1, player4, player2, player3, nrCarte, tragere);
																}
														if (isSherifAlive(player2, player3, player4) == false)
														{
															ok = 0;
															return 1;
														}
														if (OnlySherifAlive(player2, player3, player4) == true)
														{
															ok = 0;
															return 2;
														}
													}
													else
													{
														std::cout << "Nu poti folosi aceasta carte pe jucatorul selectat\n";
														logger.log("You can't use Bang! on selected player because is too far", Logger::Level::Warning);
														wait.wait(5);
													}

												}
											}
										}
										else
										{
											std::cout << "Nu poti folosi aceasta carte ";
											wait.wait(5);
										}
			if (player1.getCharacterName() == Characters::Name::SuzyLafayette && player1.getNumberOfCards() == 0)
			{
				takeACard(player1);
			}
		}break;
		case 3: {
			std::cout << "Alege cartea pe care vrei sa o decartezi\n";
			int nrCart;
			while (true)
			{
				std::cin >> nrCart;
				if (nrCart<1 || nrCart>player1.getNumberOfCards())
				{
					std::cout << "Nu ati ales o carte existanta." << std::endl;
				}
				else break;
			}
			discardedCards.addCard(player1.discardCard(nrCart - 1));
			ply = "Player" + nrPly + " discarted a card";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
			if (player1.getCharacterName() == Characters::Name::SuzyLafayette && player1.getNumberOfCards() == 0)
			{
				takeACard(player1);
			}
		}break;
		case 4: {
			if (player1.getNumberOfLives() >= player1.getNumberOfCards())
			{
				ok = 0;
				break;
			}
			else
			{
				int nrCartiDecartat = player1.getNumberOfCards() - player1.getNumberOfLives();
				std::cout << "Trebuie sa decartezi " << nrCartiDecartat << " carti\n";
				ply = "Player" + nrPly + " has to discard " + std::to_string(nrCartiDecartat) + " cards";
				logger.log(ply, Logger::Level::Warning);
				ply.clear();
				for (auto it = 0; it < nrCartiDecartat; ++it)
				{
					std::cout << "Alege cartea\n ";
					int nrCart;
					while (true)
					{
						std::cin >> nrCart;
						if (nrCart<1 || nrCart>player1.getNumberOfCards())
						{
							std::cout << "Nu ati ales o carte existanta." << std::endl;
						}
						else break;
					}
					discardedCards.addCard(player1.discardCard(nrCart - 1));
					ply = "Player" + nrPly + "  discarded card number " + std::to_string(nrCart);
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				ok = 0;
				break;
			}
		}break;
		case 5: {
			if (player1.getCharacterName() != Characters::Name::SidKetchum)
				break;
			else
			{
				if (player1.getRole() == Roles::Rol::Sheriff && player1.getNumberOfLives() < 5 && player1.getNumberOfCards() >= 2)
				{
					std::cout << "Alege doua carti pe care vrei sa le decartezi\n";
					int c1, c2;
					while (true)
					{
						std::cout << "Cartea 1: "; std::cin >> c1;
						std::cout << "Cartea 2: "; std::cin >> c2;
						if ((c1<1 || c1>player1.getNumberOfCards()) && (c2 < 1 || c2 < player1.getNumberOfCards()))
						{
							std::cout << "Nu ati ales o caret existanta." << std::endl;
						}
						else break;
					}
					if (c1 < c2)
						std::swap(c1, c2);
					discardedCards.addCard(player1.discardCard(c1 - 1));
					discardedCards.addCard(player1.discardCard(c2 - 1));
					player1.updateLives(1);
					ply = "Player" + nrPly + "  discarded card number " + std::to_string(c1) + " and " + std::to_string(c2) + " to take a life back";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				else
					if (player1.getRole() != Roles::Rol::Sheriff && player1.getNumberOfLives() < 4 && player1.getNumberOfCards() >= 2)
					{
						std::cout << "Alege doua carti pe care vrei sa le decartezi\n";
						int c1, c2;
						while (true)
						{
							std::cout << "Cartea 1: "; std::cin >> c1;
							std::cout << "Cartea 2: "; std::cin >> c2;
							if ((c1<1 || c1>player1.getNumberOfCards()) && (c2 < 1 || c2 < player1.getNumberOfCards()))
							{
								std::cout << "Nu ati ales o caret existanta." << std::endl;
							}
							else break;
						}
						if (c1 < c2)
							std::swap(c1, c2);
						discardedCards.addCard(player1.discardCard(c1 - 1));
						discardedCards.addCard(player1.discardCard(c2 - 1));
						player1.updateLives(1);
						ply = "Player" + nrPly + "  discarded card number " + std::to_string(c1) + " and " + std::to_string(c2) + " to take a life back";
						logger.log(ply, Logger::Level::Info);
						ply.clear();
					}
					else {
						std::cout << "Nu poti folosi aceasta optiune, numarul maxim de vieti a fost atins\n";
						wait.wait(5);
					}
			}
		}break;
		}
	}
	return 0;
}

Player Game::constructPlayer(const int & rolNumber, Characters& caracter, const int &nrPlayer)
{
	int nrCarti = 0;
	int numarCaracter;
	int nrCaracter;
	Characters carac;
	while (true)
	{
		std::cout << "Alege un numar intre 1 si " << 16 - nrPlayer + 1 << std::endl;
		std::cin >> numarCaracter;
		try {
			nrCaracter = caracter.pickCharacter(numarCaracter,nrPlayer);
			Characters c(nrCaracter);
			carac = c;
			break;
		}
		catch (const std::out_of_range& exception) {
			std::cout << exception.what() << std::endl;
		}
	}
	nrCarti = carac.getLives();
	if (rolNumber == 1)
		nrCarti++;
	std::vector<Cards>carti;
	for (auto index = 0; index < nrCarti; ++index)
		carti.push_back(unusedCards.pickCards());
	Player player(rolNumber, nrCaracter, carti, nrPlayer, nrPlayers);
	return std::move(player);
}
