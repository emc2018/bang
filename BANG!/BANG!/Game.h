#pragma once
#include<array>
#include"Player.h"
#include"UnusedCards.h"
#include"DiscardedCards.h"
#include"../Logging/Logging/Logging.h"
class Game
{
public:
	//numar de jucatori, acesta nu este o constanta deoarece ce se va modfica in momentul cand un jucator moarte, ne vom folosi de acesta varibiala atunci cand calculam distante
	int nrPlayers = 4;
public:
	Game();
	void run();

	Player constructPlayer(const int &rolNumber, Characters& caracter, const int &nrPlayer);
	void chooseRoles(std::array<int,4> &roles);
	void verificare(Player& player1, Player& player2, Player& player3, Player& player4);
	void distanceAfterDead(Player& player1, Player& player2, Player& player3);

	void whoIsSherif(Player& player1, Player& player2, Player& player3, Player& player4);
	//functiile pentru a determina cine a castigat
	bool isSherifAlive(const Player& player2, const Player& player3, const Player& player4)const;
	bool OnlySherifAlive(const Player& player2, const Player& player3, const Player& player4)const;
	bool isOutlaw(const Player& player2, const Player& player3, const Player& player4)const;

	//fiecare functie descrie actiunile cartilor
	void catBalouAction(Player &player1, Player &player2, Player& player3, Player& player4);
	void bangAction(Player &player1, Player &player2, Player & player3, Player & player4, const int& nrCarte, int &tragere);
	void panicaAction(Player& player1, Player& player2, Player& player3, Player& player4, const int& nrCarte);
	void salonAction(Player &player1, Player &player2, Player& player3, Player& player4);
	void gatlingAction(Player &player1, Player &player2, Player& player3, Player& player4);
	void butoiAction(Player& player1, Player& player2, Player& player3, Player& player4,Cards& butoi);
	void ratatAction(Player& player, const Cards& ratat);
	void distanceByMustang(Player& player1, Player& player2, Player& player3, const int &nrPlayer, const int& sem);
	void distanceByLuneta(Player& player1, const int &sem);
	void gatlingActionForOnPlayer(Player& player1, Player& player2, Player& player3, Player& player4);

	int tunPlayer(Player& player1, Player& player2, Player& player3, Player& player4);
	void swapPlayer(Player& player1, Player& player2);
	void takeACard(Player &player);
	void startTurnPlayer(Player &player1, Player &player2, Player& player3, Player& player4);

	//aceste functii sunt specifice abilitatilor anumitor caractere
	void VulturSem(Player& player1, Player& player2);
	void ifPaulRegret(Player& player1, Player& player2, Player& player3, Player& player4);
	void ifRooseDoolan(Player& player1, Player& player2, Player& player3, Player& player4);
	void isElGringo(Player& player1, Player&player2);
	void isSidKetchum(Player& player);
	

public:
	
	~Game();
private:
	Logger logger;
	UnusedCards unusedCards;
	DiscardedCards discardedCards;
};

