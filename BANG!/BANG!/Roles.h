#pragma once
#include <iostream>

class Roles
{
public:
	enum class Rol {
		None,
		Sheriff,
		Outlaw,
		Renegate,
		DeputySheriff
	};

public:
	Roles(int nrRol);
	Rol getName() const;
	friend std::ostream & operator <<(std::ostream& os, const Roles& rol);

private:
	Rol name;
};

