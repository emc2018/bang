#include "UnusedCards.h"
#include <algorithm>
#include<fstream>
#include <chrono> 
#include <random> 


UnusedCards::UnusedCards(std::vector<Cards> cards) :
	unusedCards(cards)
{

}

void UnusedCards::citire()
{
	std::ifstream in("Cards.in");
	for (int index = 0; index < 69; index++)
	{
		Cards c;
		in >> c;
		unusedCards.push_back(std::move(c));
	}
	shuffleVector();
}


void UnusedCards::shuffleVector()
{

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(unusedCards.begin(), unusedCards.end(), std::default_random_engine(seed));
	//std::random_shuffle(unusedCards.begin(), unusedCards.end());
}

UnusedCards & UnusedCards::operator=(const DiscardedCards & other)
{
	this->unusedCards = other.getDiscardedCards();
	return (*this);
}

UnusedCards & UnusedCards::operator=(DiscardedCards && other)
{
	Cards card = other.getDiscardedCards().back();
	std::vector<Cards>cards;
	for (auto it = 0; it < other.getDiscardedCards().size() - 1; ++it)
		cards.push_back(std::move(other.getDiscardedCards().at(it)));
	this->unusedCards = std::move(cards);
	new(&other)DiscardedCards;
	other.addCard(std::move(card));
	return (*this);
}

int UnusedCards::sizePackage()const
{
	return unusedCards.size();
}

Cards UnusedCards::pickCards()
{

	if (unusedCards.size() > 0)
	{
		auto card = std::move(unusedCards.back());
		unusedCards.pop_back();
		return std::move(card);
	}
	else
		throw std::exception("The package is empty");
}