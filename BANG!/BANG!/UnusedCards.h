#pragma once
#include<vector>
#include"DiscardedCards.h"
#include "Cards.h"
class UnusedCards
{
public:
	UnusedCards() = default;
	UnusedCards(std::vector<Cards> cards);

	void citire();
	void shuffleVector();

	UnusedCards& operator= (const DiscardedCards & other);
	UnusedCards& operator= (DiscardedCards && other);
	int sizePackage()const;
	Cards pickCards();

private:
	std::vector<Cards> unusedCards;
};

