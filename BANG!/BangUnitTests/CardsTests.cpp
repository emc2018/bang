#include "stdafx.h"
#include "CppUnitTest.h"
#include "Cards.h"
#include "DiscardedCards.h"
#include "UnusedCards.h"
#include "Player.h"
#include "Characters.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BangUnitTests
{
	TEST_CLASS(CardsTests)
	{
	public:

		TEST_METHOD(ConstructorWithNOParam)
		{
			Cards card;
			if(card.getName() != "None" && card.getNumber().first != 0  && card.getNumber().second != Cards::symbol::None)
			{
				Assert::Fail;
			}
		}

		TEST_METHOD(ConstructorWithParam)
		{
			Cards card("Bang!", 1, "Hearts");
			if (card.getName() == "Bang!" && card.getNumber().first == 1 && card.getNumber().second == Cards::symbol::Hearts)
			{

			}
			else
				Assert::Fail;
		}

		TEST_METHOD(OperatorEgal)
		{
			Cards card("Ratat!", 3, "Clovers");
			Cards anotherCard;
			anotherCard = card;

			if (anotherCard.getName() != "Ratat!" && anotherCard.getNumber().first != 3 && anotherCard.getNumber().second != Cards::symbol::Clovers)
			{
				Assert::Fail;
			}
		}

		TEST_METHOD(CopyConstructor)
		{
			Cards card("Bang", 7, "Tiles");
			Cards anotherCard(card);

			if (anotherCard.getName() != card.getName() && anotherCard.getNumber().first != 7 && anotherCard.getNumber().second != Cards::symbol::Tiles)
				Assert::Fail;
		}

		TEST_METHOD(PickCardfromUnusedCards)
		{
			UnusedCards unusedCards;
			unusedCards.citire();
			std::vector<Cards>carti;
			for (auto index = 0; index < Characters(9).getLives(); ++index)
				carti.push_back(unusedCards.pickCards());

			Player player(3, 9, carti, 1, 4);
			unusedCards.pickCards();

			if (carti.size() == Characters(9).getLives() + 1)
				Assert::IsTrue;
		}

	};
}