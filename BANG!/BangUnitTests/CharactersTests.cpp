#include "stdafx.h"
#include "CppUnitTest.h"
#include "Characters.h"
#include "../FunctionDLL/FunctionDLL/Functions.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BangUnitTests
{		
	TEST_CLASS(CharactersTests)
	{
	public:
		
		TEST_METHOD(ConstructorWithParam)
		{
			Characters character(3);
			Assert::IsTrue(character.getName() == Characters::Name::JesseJones &&
						   character.getLives() == 4);
		}

		TEST_METHOD(ConstructorWithNoParam)
		{
			Characters character;
			if (character.getName() == Characters::Name::None && character.getLives() == 0)
			{
				Assert::Fail;
			}
			
		}

		TEST_METHOD(OperatorEgal)
		{
			Characters first(7);
			Characters second;
			second = first;

			if (second.getName() == first.getName() && second.getLives() == first.getLives())
				Assert::IsTrue;
		}

	};
}