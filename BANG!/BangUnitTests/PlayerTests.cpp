#include "stdafx.h"
#include "CppUnitTest.h"
#include "Player.h"
#include "Roles.h"
#include "Characters.h"
#include "Cards.h"
#include "UnusedCards.h"
#include "DiscardedCards.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BangUnitTests
{
	TEST_CLASS(PlayerTests)
	{
	public:

		TEST_METHOD(ConstructorWithNoParam)
		{
			Player player;
			Cards gun("Colt 45", 0, "None");
			
			if (player.getGun().getName() != gun.getName() && player.getGun().getNumber().first != 0
				&& player.getGun().getNumber().second != Cards::symbol::None 
				&& player.getRole() != Roles::Rol::None && player.getCharacterName() != Characters::Name::None
				&& player.getNumberOfLives() != 0)
			{
				Assert::Fail;
			}

		}

		TEST_METHOD(ConstructorWithParam)
		{
			Cards gun("Colt 45", 0, "None");
			UnusedCards unusedCards;
			std::vector<Cards>carti;
			/*for (auto index = 0; index < Characters(9).getLives(); ++index)
				carti.push_back(unusedCards.pickCards());*/
			Player player(3, 9, carti, 1, 4);

			if (player.getGun().getName() != gun.getName() && player.getGun().getNumber().first != 0
				&& player.getGun().getNumber().second != Cards::symbol::None
				&& player.getRole() != Roles::Rol::Renegate && player.getCharacterName() != Characters::Name::ElGringo
				&& player.getNumberOfLives() != Characters(9).getLives())
			{
				Assert::Fail;
			}
			
		}

		TEST_METHOD(DiscardGun)
		{
			std::vector<Cards>carti;
			Player player(3, 9, carti, 1, 4);
			Cards gun("Remington", 13, "Clovers");
			player.setGun(gun);

			player.discardGun();
			if (player.getGun().getName() == gun.getName())
				Assert::Fail;
		}

		TEST_METHOD(AddCard)
		{
			UnusedCards unusedCards;
			unusedCards.citire();
			std::vector<Cards>carti;
			for (auto index = 0; index < Characters(9).getLives(); ++index)
				carti.push_back(unusedCards.pickCards());
			
			Cards card("Salon", 5, "Hearts");
			Player player(3, 9, carti, 1, 4);
			player.addCard(card);

			for (auto a : carti)
				if (a.getName() == card.getName())
					Assert::IsTrue;
		}

		TEST_METHOD(ActiveCard)
		{
			UnusedCards unusedCards;
			unusedCards.citire();
			std::vector<Cards>carti;
			for (auto index = 0; index < Characters(9).getLives(); ++index)
				carti.push_back(unusedCards.pickCards());

			Player player(3, 9, carti, 1, 4);
			Cards gun("Remington", 13, "Clovers");
			player.addCard(gun);

			for (auto index = 0; index < carti.size(); ++index)
			{
				if (carti[index].getName() == gun.getName())
				{
					player.activeCard(index);
				}
			}

			for (auto a : carti)
				if (a.getName() == gun.getName())
					Assert::Fail;
		}

		TEST_METHOD(DiscardAllCards)
		{
			UnusedCards unusedCards;
			DiscardedCards discardCards;
			unusedCards.citire();
			std::vector<Cards>carti;
			for (auto index = 0; index < Characters(9).getLives(); ++index)
				carti.push_back(unusedCards.pickCards());

			Player player(3, 9, carti, 1, 4);
			player.discardALLCards(discardCards);

			if (carti.size() != 0)
				Assert::Fail;
		}

		TEST_METHOD(DiscardCard)
		{
			UnusedCards unusedCards;
			DiscardedCards discardCards;
			unusedCards.citire();
			std::vector<Cards>carti;
			for (auto index = 0; index < Characters(9).getLives(); ++index)
				carti.push_back(unusedCards.pickCards());

			Player player(3, 9, carti, 1, 4);
			player.discardCard(1);

			if (carti.size() == Characters(9).getLives() - 2)
				Assert::IsTrue;

		}

		TEST_METHOD(IsBang)
		{
			std::vector<Cards>carti;
			Cards card("Bang!", 12, "Hearts");
			Cards anotherCard("Remington", 13, "Clovers");
			carti.push_back(card);
			carti.push_back(anotherCard);

			Player player(3, 9, carti, 1, 4);
			player.isBang();
			
			for (auto index : carti)
				if (index.getName() == card.getName())
					Assert::Fail;
		}

		TEST_METHOD(IsRatat)
		{
			std::vector<Cards>carti;
			Cards card("Bang!", 12, "Hearts");
			Cards anotherCard("Ratat", 10, "Clovers");
			carti.push_back(card);
			carti.push_back(anotherCard);

			Player player(3, 9, carti, 1, 4);
			player.isRatat();

			for (auto index : carti)
				if (index.getName() == anotherCard.getName())
					Assert::Fail;
		}

	};
}