﻿#pragma once
#include<string>
#include<vector>
class Cards
{
public:
	enum class symbol { None, Hearts, Tiles, Clovers, Pikes };
	enum class action {
		None, BANG, RATAT, Primesti_1_punct_de_viata, Obliga_sa_decarteze_o_carte, Trage_o_carte,
		Oricare_jucator, Fiecare_jucator, Oricare_jucator_la_o_distanta_disponibila, Oricare_jucator_la_o_distanta_maxima_afisata,
		Ii_vezi_pe_ceilalti_la_o_distanta_mai_mica_cu_1, Ceilalti_te_vad_la_o_distanta_mai_mare_cu_1, Daca_e_inima, Poti_juca_oricate_carti_Bang
	};

public:
	Cards();
	Cards(const std::string &name, const int &number, const std::string &symbol);
	Cards(const Cards& other);
	Cards(Cards&& other);


	void verifName(const std::string &name);
	friend std::ostream& operator << (std::ostream& os, const Cards& card);
	friend std::istream& operator >>(std::istream& in, Cards& card);

	Cards & operator = (const Cards & other);
	Cards & operator= (Cards&& other);

	std::string getName()const;
	int getDistance()const;
	int getPriority()const;
	void setPriority(const int& priority );
	bool getType()const;
	std::vector<action> getActions()const;
	std::pair<int, symbol> getNumber()const;
	bool isGun()const;

private:
	//fiecare carte are un nume, o distanta(in caz ca e arma), tipul cartii respective, actiunile ce se pot realiza cu acesta,
	// in plus fiecare carte are simibolurile speficice jocului de carti normal, ceea ce infuenteaza jocul in cazul numitor actiuni
	std::string name;
	int distance;
	bool type;
	std::vector<action>actions;
	std::pair<int, symbol>number;
	int priority;
};

