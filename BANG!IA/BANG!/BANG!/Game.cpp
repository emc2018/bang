#include "Game.h"
#include "../FunctionDLL/FunctionDLL/Functions.h"
#include<fstream>
#include<iostream>
#include <chrono> 
#include <random>

std::ofstream of("syslog.log", std::ios::app);
Game::Game() :
	logger(of)
{
	logger.log("----------------------------------------------------------------------------------------", Logger::Level::None);
}

Game::~Game()
{
	of.close();
}
void Game::chooseRoles(std::array<int, 4> &roles)
{
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(roles.begin(), roles.end(), std::default_random_engine(seed));
}
void Game::VulturSem(Player & player1, Player & player2)
{
		std::string ply;
		player1.copiereCarti(player2);
		ply = "Player" + std::to_string(player1.getNrPlayer()) + "  took all the cards of the Player" + std::to_string(player2.getNrPlayer());
		logger.log(ply, Logger::Level::Info);
		ply.clear();
}
void Game::ifPaulRegret(Player& player1, Player& player2, Player& player3, Player& player4)
{
	if (player1.getCharacterName() == Characters::Name::PaulRegret)
	{
		player2.increaseDistance(player1.getNrPlayer() - 1);
		player3.increaseDistance(player1.getNrPlayer() - 1);
		player4.increaseDistance(player1.getNrPlayer() - 1);
	}
	else if (player2.getCharacterName() == Characters::Name::PaulRegret)
	{
		player1.increaseDistance(player2.getNrPlayer() - 1);
		player3.increaseDistance(player2.getNrPlayer() - 1);
		player4.increaseDistance(player2.getNrPlayer() - 1);
	}
	else if (player3.getCharacterName() == Characters::Name::PaulRegret)
	{
		player1.increaseDistance(player3.getNrPlayer() - 1);
		player2.increaseDistance(player3.getNrPlayer() - 1);
		player4.increaseDistance(player3.getNrPlayer() - 1);
	}
	else if (player4.getCharacterName() == Characters::Name::PaulRegret)
	{
		player1.increaseDistance(player4.getNrPlayer() - 1);
		player2.increaseDistance(player4.getNrPlayer() - 1);
		player3.increaseDistance(player4.getNrPlayer() - 1);
	}
}
void Game::ifRooseDoolan(Player& player1, Player& player2, Player& player3, Player& player4)
{

	if (player1.getCharacterName() == Characters::Name::RoseDoolan)
	{
		for (int index = 0; index < nrPlayers; index++)
			if (index != player1.getNrPlayer() - 1)
				player1.decreaseDistance(index);
	}
	else if (player2.getCharacterName() == Characters::Name::RoseDoolan)
	{
		for (int index = 0; index < nrPlayers; index++)
			if (index != player2.getNrPlayer() - 1)
				player2.decreaseDistance(index);
	}
	else if (player3.getCharacterName() == Characters::Name::RoseDoolan)
	{
		for (int index = 0; index < nrPlayers; index++)
			if (index != player3.getNrPlayer() - 1)
				player3.decreaseDistance(index);
	}
	else if (player4.getCharacterName() == Characters::Name::RoseDoolan)
	{
		for (int index = 0; index < nrPlayers; index++)
			if (index != player3.getNrPlayer() - 1)
				player3.decreaseDistance(index);
	}
}

void Game::verificare(Player& player1, Player& player2, Player& player3, Player& player4)
{
	ifPaulRegret(player1, player2, player3, player4);
	ifRooseDoolan(player1, player2, player3, player4);
}

void Game::swapPlayer(Player& player1, Player& player2)
{
	std::swap(player1, player2);
	/*int aux = player1.getNrPlayer();
	player1.setNrPlayer(player2.getNrPlayer());
	player2.setNrPlayer(aux);
	player1.distanceCalculation(nrPlayers);
	player2.distanceCalculation(nrPlayers);*/
}

std::pair<int, int> maxPrio(const int& nr1, const int& nr2, const int& nr3)
{
	std::pair<int, int> prio(0, 0);
	if (nr1 == nr2 && nr2 == nr3)
	{
		prio.first = 1;
		prio.second = 2;
		return prio;
	}
		
	if (nr1 <nr2)
	{
		if (nr2 < nr3)
		{
			prio.first = 1;
			prio.second = 2;
		}
		else if (nr3 > nr1)
		{
			prio.first = 1;
			prio.second = 3;
		}
		else
		{
			prio.first = 3;
			prio.second = 1;
		}
	}
	else
	{
		if (nr1 < nr3)
		{
			prio.first = 2;
			prio.second = 1;
		}
		else if (nr2 > nr3)
		{
			prio.first = 2;
			prio.second = 3;
		}
		else
		{
			prio.first = 3;
			prio.second = 2;
		}
	}
	return prio;
}

void Game::whoIsSherif(Player & player1, Player & player2, Player & player3, Player & player4)
{
	if (player1.getRole() != Roles::Rol::Sheriff)
	{
		if (player2.getRole() == Roles::Rol::Sheriff)
		{
			swapPlayer(player1, player2);
		}
		else if (player3.getRole() == Roles::Rol::Sheriff)
		{
			swapPlayer(player1, player3);
		}
		else if (player4.getRole() == Roles::Rol::Sheriff)
		{
			swapPlayer(player1, player4);
		}
	}
}

void Game::distanceByMustang(Player & player1, Player & player2, Player & player3, const int & nrPlayer, const int& sem)
{
	if (sem == 1)
	{
		player2.increaseDistance(nrPlayer - 1);
		player3.increaseDistance(nrPlayer - 1);
		player1.increaseDistance(nrPlayer - 1);
	}
	else
		if (sem == 0)
		{
			player2.decreaseDistance(nrPlayer - 1);
			player3.decreaseDistance(nrPlayer - 1);
			player1.decreaseDistance(nrPlayer - 1);
		}
}

void Game::distanceByLuneta(Player & player1, const int &sem)
{
	if (sem == 1)
	{
		for (int index = 0; index < nrPlayers; index++)
			if (index != player1.getNrPlayer() - 1)
				player1.decreaseDistance(index);
	}
	else
		if (sem == 0)
			for (int index = 0; index < nrPlayers; index++)
				if (index != player1.getNrPlayer() - 1)
					player1.increaseDistance(index);
}

void Game::distanceAfterDead(Player & player1, Player & player2, Player & player3)
{
	if (player1.isAlive())
	{
		if (player2.isAlive())
			player1.distanceCalculationAfterOnePlayerDead(player2.getNrPlayer(), nrPlayers);
		if (player3.isAlive())
			player1.distanceCalculationAfterOnePlayerDead(player3.getNrPlayer(), nrPlayers);
	}
	if (player2.isAlive())
	{
		if (player1.isAlive())
			player2.distanceCalculationAfterOnePlayerDead(player1.getNrPlayer(), nrPlayers);
		if (player3.isAlive())
			player2.distanceCalculationAfterOnePlayerDead(player3.getNrPlayer(), nrPlayers);
	}
	if (player3.isAlive())
	{
		if (player2.isAlive())
			player3.distanceCalculationAfterOnePlayerDead(player2.getNrPlayer(), nrPlayers);
		if (player1.isAlive())
			player3.distanceCalculationAfterOnePlayerDead(player1.getNrPlayer(), nrPlayers);
	}
	nrPlayers--;

}

bool Game::isSherifAlive(const Player & player2, const Player & player3, const Player & player4) const
{
	if (player2.isAlive() != true && player2.getRole() == Roles::Rol::Sheriff)
		return false;
	if (player3.isAlive() != true && player3.getRole() == Roles::Rol::Sheriff)
		return false;
	if (player4.isAlive() != true && player4.getRole() == Roles::Rol::Sheriff)
		return false;
	return true;
}

bool Game::OnlySherifAlive(const Player & player2, const Player & player3, const Player & player4) const
{
	if (player2.isAlive() != true && player2.getRole() != Roles::Rol::Sheriff && player3.isAlive() != true && player3.getRole() != Roles::Rol::Sheriff &&player4.isAlive() != true && player4.getRole() != Roles::Rol::Sheriff)
		return true;

	return false;
}

bool Game::isOutlaw(const Player& player2, const Player& player3, const Player& player4) const
{
	if (player2.isAlive() == true && player2.getRole() == Roles::Rol::Outlaw)
		return true;
	if (player3.isAlive() == true && player3.getRole() == Roles::Rol::Outlaw)
		return true;
	if (player4.isAlive() == true && player4.getRole() == Roles::Rol::Outlaw)
		return true;
	return false;
}

void Game::takeACard(Player & player)
{
	std::string ply;
	if (unusedCards.sizePackage() == 0)
	{
		unusedCards = std::move(discardedCards);
		unusedCards.shuffleVector();
	}
	player.addCard(unusedCards.pickCards());
	ply = "Player" + std::to_string(player.getNrPlayer()) + " take a card";
	logger.log(ply, Logger::Level::Info);
	ply.clear();
}

void Game::butoiAction(Player& player1, Player& player2, Player& player3, Player& player4,Cards& butoi)
{
	Functions wait;
	std::string ply;
	discardedCards.addCard(std::move(butoi));
	Cards card = unusedCards.pickCards();
	if (card.getNumber().second == Cards::symbol::Hearts)
	{
		
		std::cout << "Ratat!\n";
		ply = "Player" + std::to_string(player2.getNrPlayer()) + " used "+ card.getName() +" and escaped the bang";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
		discardedCards.addCard(std::move(card));
		Functions wait; wait.wait(5);
	}
	else
	{
		discardedCards.addCard(std::move(card));
		ply = "Player" + std::to_string(player2.getNrPlayer()) + " used a barrel but did not escape the bang";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
		Cards ratat = player2.isRatat();
		if (ratat.getName() == "Ratat!")
		{
			ratatAction(player2, ratat);
			ply = "Player" + std::to_string(player2.getNrPlayer()) + " used " + ratat.getName() + " and escape the bang";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
		}
		else
			if (player2.getCharacterName() == Characters::Name::CalamityJanet)
			{
				Cards bang = player2.isBang();
				if (bang.getName() == "Bang!")
				{
					ratatAction(player2, bang);
					ply = "Player" + std::to_string(player2.getNrPlayer()) + " used "+ bang.getName() +" and escape the bang";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
			}
			else
			{
				player2.updateLives(0);
				std::cout << "BANG!" << std::endl;
				wait.wait(5);
				if (player2.getCharacterName() == Characters::Name::BartCassidy && player2.isAlive() == true)
				{
					takeACard(player2);
				}
				if (player2.getCharacterName() == Characters::Name::ElGringo && player2.isAlive() == true)
				{
					if(player2.getNrPlayer()==1)
						isElGringo(player1, player2);
					else
						isElGringoAI(player1, player2);
				}
				if (player2.isAlive() != true)
				{
					Cards beer = player2.isBeer();
					if (beer.getName() == "Bere")
					{
						discardedCards.addCard(beer);
						if (player2.getCharacterName() == Characters::Name::SuzyLafayette && player2.getNumberOfCards() == 0)
						{
							takeACard(player2);
						}
						player2.updateLives(1);
						ply = "Player" + std::to_string(player2.getNrPlayer()) + " used " + beer.getName();
						logger.log(ply, Logger::Level::Info);
						ply.clear();
					}
					else
						if (player2.getCharacterName() == Characters::Name::SidKetchum && player2.getNumberOfCards() >= 2)
						{
							if(player2.getNrPlayer()==1)
								isSidKetchum(player2);
							else
								isSidKetchumAI(player2);
						}
						else
						{
							if (player1.getCharacterName() == Characters::Name::VultureSam)
							{
								VulturSem(player1, player2);
								distanceAfterDead(player1, player3, player4);
							}
							else if (player3.getCharacterName() == Characters::Name::VultureSam && player3.isAlive()==true)
							{
								VulturSem(player3, player2);
								distanceAfterDead(player1, player3, player4);
							}
							else if (player4.getCharacterName() == Characters::Name::VultureSam &&player3.isAlive()==true)
							{
								VulturSem(player4, player2);
								distanceAfterDead(player1, player3, player4);
							}
							else {
								distanceAfterDead(player1, player3, player4);
								player2.discardALLCards(discardedCards);
							}
						}
				}
			}
	}

}
void Game::ratatAction(Player& player, const Cards& ratat)
{
	std::string ply;
	discardedCards.addCard(std::move(ratat));
	if (player.getCharacterName() == Characters::Name::SuzyLafayette && player.getNumberOfCards() == 0)
	{
		takeACard(player);
	}
	std::cout << "Ratat!:p\n";
	ply = "Player" + std::to_string(player.getNrPlayer()) + " escape from a Bang";
	logger.log(ply, Logger::Level::Info);
	ply.clear();
	Functions wait; wait.wait(5);
}
void Game::isElGringo(Player& player1, Player& player2)
{
	std::string ply;
	if (player1.getNumberOfCards() != 0)
	{
		system("cls");
		std::cout << "Player" << player2.getNrPlayer() << " a primit un Bang de la Player" << player1.getNrPlayer() << "\nPlayer" << player2.getNrPlayer() << " alege o cartea de la Player" << player1.getNrPlayer();
		std::cout << "\nAlege un umar intre 1 si " << player1.getNumberOfCards() << std::endl;
		int cart;
		while (true)
		{
			std::cin >> cart;
			if ((cart<1 || cart>player1.getNumberOfCards()))
			{
				std::cout << "Nu ati ales o caret existanta." << std::endl;
			}
			else break;
		}
		player2.addCard(player1.discardCard(cart - 1));
		ply = "Player" + std::to_string(player2.getNrPlayer()) + "  takes a card from Player" + std::to_string(player1.getNrPlayer());
		logger.log(ply, Logger::Level::Info);
		ply.clear();
	}
}
void Game::isSidKetchum(Player& player)
{
	std::string ply;
	std::cout << "-------------Esti player" << player.getNrPlayer() << "\n\n";
	player.showCards(1);
	std::cout << "\n\nAlege doua carti sa decartezi pentru a primi o viata\n";
	int c1, c2;
	while (true)
	{
		std::cout << "Cartea 1: "; std::cin >> c1;
		std::cout << "Cartea 2: "; std::cin >> c2;
		if ((c1<1 || c1>player.getNumberOfCards()) && (c2 < 1 || c2 < player.getNumberOfCards()))
		{
			std::cout << "Nu ati ales o caret existanta." << std::endl;
		}
		else break;
	}
	if (c1 < c2)
		std::swap(c1, c2);
	discardedCards.addCard(player.discardCard(c1 - 1));
	discardedCards.addCard(player.discardCard(c2 - 1));
	player.updateLives(1);
	ply = "Player" + std::to_string(player.getNrPlayer()) + "  discarded card number " + std::to_string(c1) + " and " + std::to_string(c2) + " to take a life back";
	logger.log(ply, Logger::Level::Info);
	ply.clear();
}
void Game::isElGringoAI(Player & player1, Player & player2)
{
	std::string ply;
	if (player1.getNumberOfCards() != 0)
	{
		unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
		int cart = rand() % player1.getNumberOfCards() + 1;
		player2.addCard(player1.discardCard(cart - 1));
		ply = "Player" + std::to_string(player2.getNrPlayer()) + "  takes a card from Player" + std::to_string(player1.getNrPlayer());
		logger.log(ply, Logger::Level::Info);
		ply.clear();
	}
}
void Game::isSidKetchumAI(Player & player)
{
	std::string ply;
	int c1, c2;
	c1 = player.cardWithLessPriority();
	discardedCards.addCard(player.discardCard(c1));
	c2 = player.cardWithLessPriority();
	discardedCards.addCard(player.discardCard(c2));
	player.updateLives(1);
	ply = "Player" + std::to_string(player.getNrPlayer()) + " discard to cards and take on life back";
	logger.log(ply, Logger::Level::Info);
	ply.clear();
}
bool myfunction(std::pair<std::string, int> i, std::pair<std::string, int> j) { return (i.second > j.second); }
std::vector < std::pair<std::string, int>> Game::createPriority()
{
	std::ifstream in("syslog.log");
	std::string cuv;
	std::unordered_map<std::string, int>priority = { 
		{"Bang!",0},{"Luneta",0}, {"Mustang",0} ,{"Butoi",0},{"Volcanic",0},{"Schofield",0},{"Remington",0}, {"Salon",0},
		{"Rev. Carabine",0},{"Winchester",0}, {"Cat Balou",0} ,{"Diligenta",0},{"Wells Fargo",0},{"Panica!",0},{"Bere",0},{"Gatling",0},{"Ratat!",0}
	};
	while (!in.eof())
	{
		in >> cuv;
		if (cuv == "Wells")
			priority.at("Wells Fargo")++;
		if (cuv == "Cat")
			priority.at("Cat Balou")++;
		if (cuv == "Rev.")
			priority.at("Rev. Carabine")++;
		if (cuv == "Bang!")
			priority.at(cuv)++;
		if (cuv == "Luneta")
			priority.at(cuv)++;
		if (cuv == "Mustang")
			priority.at(cuv)++;
		if (cuv == "Butoi")
			priority.at(cuv)++;
		if (cuv == "Volcanic")
			priority.at(cuv)++;
		if (cuv == "Schofield")
			priority.at(cuv)++;
		if (cuv == "Remington")
			priority.at(cuv)++;
		if (cuv == "Salon")
			priority.at(cuv)++;
		if (cuv == "Winchester")
			priority.at(cuv)++;
		if (cuv == "Diligenta")
			priority.at(cuv)++;
		if (cuv == "Panica!")
			priority.at(cuv)++;
		if (cuv == "Bere")
			priority.at(cuv)++;
		if (cuv == "Gatling")
			priority.at(cuv)++;
		if (cuv == "Ratat!")
			priority.at(cuv)++;
	}
	std::vector < std::pair<std::string, int>>prioriti;
	for (auto x : priority)
	{
		std::pair<std::string, int> pair;
		pair.first = x.first;
		pair.second = x.second;
		prioriti.push_back(pair);
	}
	std::sort(prioriti.begin() , prioriti.end(), myfunction);
	std::vector < std::pair<std::string, int>>prioritii;
	for (auto x : prioriti)
	{
		prioritii.push_back(x);
	}
	for (int i = 0; i < prioritii.size(); ++i)
	{
		prioritii.at(i).second = 0;
	}
	prioritii.at(0).first = prioriti.at(0).first;
	prioritii.at(0).second = 1;
	for (int i = 1; i < prioriti.size(); ++i)
	{
		if (prioriti.at(i).second == prioriti.at(i - 1).second)
		{
			prioritii.at(i).first = prioriti.at(i).first;
			prioritii.at(i).second = prioritii.at(i-1).second;
		}
		else {
			prioritii.at(i).first = prioriti.at(i).first;
			prioritii.at(i).second = prioritii.at(i - 1).second+1;
		}
	}
	return prioritii;

}
void Game::bangAction(Player & player1, Player & player2, Player & player3, Player & player4, const int& nrCarte, int &tragere)
{
	Functions wait;
	std::string ply;
	if (player1.getCharacterName() != Characters::Name::SlabTheKiller)
	{
		if (player2.isAlive() == true)
		{
			tragere++;
			discardedCards.addCard(player1.discardCard(nrCarte - 1));
			if (player2.getCharacterName() == Characters::Name::Jourdannais)
			{
				Cards butoi = unusedCards.pickCards();
				if (butoi.getNumber().second == Cards::symbol::Hearts)
				{
					ratatAction(player2, butoi);
					ply = "Player" + std::to_string(player2.getNrPlayer()) + " is Jourdannais and escaped the bang with a Heart";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				else
				{
					discardedCards.addCard(std::move(butoi));
					std::cout << "BANG!" << std::endl;
					ply = "Player" + std::to_string(player2.getNrPlayer()) + " is Jourdannais but did not escape the bang";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					Cards butoi = player2.isButoi();
					if (butoi.getName() == "Butoi")
					{
						butoiAction(player1, player2, player3, player4,butoi);
					}
				}
			}
			else
			{
				Cards butoi = player2.isButoi();
				if (butoi.getName() == "Butoi")
				{
					butoiAction(player1, player2, player3, player4, butoi);
				}
				else
				{
					Cards ratat = player2.isRatat();
					if (ratat.getName() == "Ratat!")
					{
						ratatAction(player2, ratat);
					}
					else
						if (player2.getCharacterName() == Characters::Name::CalamityJanet)
						{
							Cards bang = player2.isBang();
							if (bang.getName() == "Bang!")
							{
								ratatAction(player2, bang);
							}
						}
						else
						{
							player2.updateLives(0);
							std::cout << "BANG!" << std::endl;
							wait.wait(5);
							if (player2.getCharacterName() == Characters::Name::BartCassidy && player2.isAlive() == true)
							{
								takeACard(player2);
							}
							if (player2.getCharacterName() == Characters::Name::ElGringo && player2.isAlive() == true )
							{
								if (player2.getNrPlayer() == 1)
									isElGringo(player1, player2);
								else
									isElGringoAI(player1, player2);
							}
							if (player2.isAlive() != true)
							{
								Cards beer = player2.isBeer();
								if (beer.getName() == "Bere")
								{
									discardedCards.addCard(beer);
									if (player2.getCharacterName() == Characters::Name::SuzyLafayette && player2.getNumberOfCards() == 0)
									{
										takeACard(player2);
									}
									player2.updateLives(1);
									ply = "Player" + std::to_string(player2.getNrPlayer()) + " used "+ beer.getName();
									logger.log(ply, Logger::Level::Info);
									ply.clear();
								}
								else
									if (player2.getCharacterName() == Characters::Name::SidKetchum && player2.getNumberOfCards() >= 2)
									{
										if (player2.getNrPlayer() == 1)
											isSidKetchum(player2);
										else
											isSidKetchumAI(player2);
									}
									else
									{
										if (player1.getCharacterName() == Characters::Name::VultureSam)
										{
											VulturSem(player1, player2);
											distanceAfterDead(player1, player3, player4);
										}
										else if (player3.getCharacterName() == Characters::Name::VultureSam && player3.isAlive() == true)
										{
											VulturSem(player3, player2);
											distanceAfterDead(player1, player3, player4);
										}
										else if (player4.getCharacterName() == Characters::Name::VultureSam &&player3.isAlive() == true)
										{
											VulturSem(player4, player2);
											distanceAfterDead(player1, player3, player4);
										}
										else {
											distanceAfterDead(player1, player3, player4);
											player2.discardALLCards(discardedCards);
										}
									}
							}
						}
					if (player1.getCharacterName() == Characters::Name::SuzyLafayette && player1.getNumberOfCards() == 0 && player1.isAlive()==true)
					{
						takeACard(player1);
					}
				}
			}
		}
		else
		{
			std::cout << "Playerul nu este in viata";
			logger.log("Player is not alive", Logger::Level::Warning);
			 wait.wait(5);
		}

	}
	else
	{
		if (player2.isAlive() == true)
		{
			tragere++;
			discardedCards.addCard(player1.discardCard(nrCarte - 1));
			if (player2.getCharacterName() != Characters::Name::CalamityJanet)
			{
				Cards c1 = player2.isRatat(), c2 = player2.isRatat();
				if (c1.getName() == "None")
				{
					player2.updateLives(0);
					std::cout << "BANG!" << std::endl;
					wait.wait(5);
					ply = "Player" + std::to_string(player2.getNrPlayer()) + " didn't have 2 ratat and he was shot";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				if (c1.getName() == "Ratat!" && c2.getName() == "None")
				{
					player2.addCard(std::move(c1));
					player2.updateLives(0);
					std::cout << "BANG!" << std::endl;
					wait.wait(5);
					ply = "Player" + std::to_string(player2.getNrPlayer()) + " didn't have 2 ratat and he was shot";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				if (c1.getName() == "Ratat!" && c2.getName() == "Ratat!")
				{
					discardedCards.addCard(std::move(c1));
					discardedCards.addCard(std::move(c2));
					std::cout << "Ratat!:p\n";
					ply = "Player" + std::to_string(player2.getNrPlayer()) + "  had 2 Ratat! and escape";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					wait.wait(5);
				}
				if (player2.isAlive() != true)
				{
					Cards beer = player2.isBeer();
					if (beer.getName() == "Bere")
					{
						discardedCards.addCard(beer);
						player2.updateLives(1);
						ply = "Player" + std::to_string(player2.getNrPlayer()) + " used "+ beer.getName();
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						if (player2.getCharacterName() == Characters::Name::SuzyLafayette && player2.getNumberOfCards() == 0 && player2.isAlive()==true)
						{
							takeACard(player2);
						}
					}
					else
						if (player2.getCharacterName() == Characters::Name::SidKetchum && player2.getNumberOfCards() >= 2 )
						{
							isSidKetchum(player2);
						}
						else if (player3.getCharacterName() == Characters::Name::VultureSam && player3.isAlive() == true)
						{
							VulturSem(player3, player2);
							distanceAfterDead(player1, player3, player4);
						}
						else if (player4.getCharacterName() == Characters::Name::VultureSam &&player3.isAlive() == true)
						{
							VulturSem(player4, player2);
							distanceAfterDead(player1, player3, player4);
						}
						else {
							distanceAfterDead(player1, player3, player4);
							player2.discardALLCards(discardedCards);
						}
				}
			}
			else
			{
				Cards c1 = player2.isRatat(), c2 = player2.isRatat(), c3 = player2.isBang(), c4 = player2.isBang();
				if (c1.getName() == "None" && c3.getName() == "None")
				{
					player2.updateLives(0);
					std::cout << "BANG!" << std::endl;
					wait.wait(5);
					ply = "Player" + std::to_string(player2.getNrPlayer()) + " didn't have 2 ratat and he was shot";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				if (c1.getName() == "Ratat!" && c2.getName() == "None" && c3.getName() == "None" && c4.getName() == "None")
				{
					player2.addCard(std::move(c1));
					player2.updateLives(0);
					std::cout << "BANG!" << std::endl;
					wait.wait(5);
					ply = "Player" + std::to_string(player2.getNrPlayer()) + " didn't have 2 ratat and he was shot";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				if (c3.getName() == "Bang!" && c4.getName() == "None" && c1.getName() == "None" && c2.getName() == "None")
				{
					player2.addCard(std::move(c3));
					player2.updateLives(0);
					std::cout << "BANG!" << std::endl;
					wait.wait(5);
					ply = "Player" + std::to_string(player2.getNrPlayer()) + " didn't have 2 ratat and he was shot";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				if (c1.getName() == "Ratat!" && c2.getName() == "Ratat!")
				{
					discardedCards.addCard(std::move(c1));
					discardedCards.addCard(std::move(c2));
					if (c3.getName() == "Bang!")
						player2.addCard(std::move(c3));
					if (c4.getName() == "Bang!")
						player2.addCard(std::move(c4));
					std::cout << "Ratat!:p\n";
					ply = "Player" + std::to_string(player2.getNrPlayer()) + "  had 2 Ratat! and escape";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					wait.wait(5);
				}
				if (c3.getName() == "Bang!" && c4.getName() == "Bang!")
				{
					discardedCards.addCard(std::move(c3));
					discardedCards.addCard(std::move(c4));
					if (c1.getName() == "Ratat!")
						player2.addCard(std::move(c1));
					if (c2.getName() == "Ratat!")
						player2.addCard(std::move(c2));
					std::cout << "Ratat!:p\n";
					ply = "Player" + std::to_string(player2.getNrPlayer()) + "  had 2 Ratat! and escape";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					 wait.wait(5);
				}
				if (c1.getName() == "Ratat!" && c2.getName() == "None" && c3.getName() == "Bang!")
				{
					discardedCards.addCard(std::move(c1));
					discardedCards.addCard(std::move(c3));
					if (c4.getName() == "Bang!")
						player2.addCard(std::move(c4));
					std::cout << "Ratat!:p\n";
					ply = "Player" + std::to_string(player2.getNrPlayer()) + "  had 2 Ratat! and escape";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					wait.wait(5);
				}
				if (player2.isAlive() != true)
				{
					Cards beer = player2.isBeer();
					if (beer.getName() == "Bere")
					{
						discardedCards.addCard(beer);
						player2.updateLives(1);
						ply = "Player" + std::to_string(player2.getNrPlayer()) + " used "+ beer.getName();
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						if (player2.getCharacterName() == Characters::Name::SuzyLafayette && player2.getNumberOfCards() == 0 && player2.isAlive() == true)
						{
							takeACard(player2);
						}
					}
					else
						if (player2.getCharacterName() == Characters::Name::SidKetchum && player2.getNumberOfCards() >= 2)
						{
							if(player2.getNrPlayer()==1)
								isSidKetchum(player2);
							else
								isSidKetchumAI(player2);
						}
						else if (player3.getCharacterName() == Characters::Name::VultureSam && player3.isAlive() == true)
						{
							VulturSem(player3, player2);
							distanceAfterDead(player1, player3, player4);
						}
						else if (player4.getCharacterName() == Characters::Name::VultureSam &&player3.isAlive() == true)
						{
							VulturSem(player4, player2);
							distanceAfterDead(player1, player3, player4);
						}
						else {
							distanceAfterDead(player1, player3, player4);
							player2.discardALLCards(discardedCards);
						}
				}
			}
		}
		else
		{
			std::cout << "Playerul nu este in viata";
			logger.log("Player is not alive", Logger::Level::Warning);
			Functions wait; wait.wait(5);
		}
	}
}

void Game::panicaAction(Player& player1, Player& player2, Player& player3, Player& player4, const int & nrCarte)
{
	std::string ply;
	system("cls");
	player2.showCards(0);
	int pachet;
	std::cout << "1: Carti in mana\n";
	std::cout << "2: Carti activate\n";
	while (true)
	{
		std::cin >> pachet;
		if (pachet!=1&&pachet!=2)
		{
			std::cout << "Nu ati ales o optiune valida" << std::endl;
		}
		else if (pachet == 2 && player2.getNrActiveCards() == 0)
		{
			std::cout << "Jucatorul nu are carti activate, alegeti prima optiune" << std::endl;
		}
		else break;
	}
	switch (pachet)
	{
	case 1: {
		std::cout << "Alege Cartea\n";
		std::cout << "Alege un umar intre 1 si " << player2.getNumberOfCards() << std::endl;
		int cart;
		while (true)
		{
			std::cin >> cart;
			if (cart<1&&cart>player2.getNumberOfCards())
			{
				std::cout << "Nu ati ales o carte existanta." << std::endl;
			}
			else break;
		}
		ply = "Player" + std::to_string(player2.getNrPlayer()) + " chose to take a card from Player" + std::to_string(player2.getNrPlayer()) + " hands";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
		player1.addCard(player2.discardCard(cart - 1));
	}break;
	case 2: {
		Functions wait;
		std::string cart;
		while (true)
		{
			while (true)
			{
				std::cout << "Alege NUMELE cartii\n";
				std::cin >> cart;
				if (cart != "Mustang"&&cart != "Luneta"&&cart != "Butoi")
				{
					std::cout << "Nu ati ales cartea potrivita" << std::endl;
				}
				else break;
			}
			Cards card = player2.discardActiveCard(cart);
			if (card.getName() != "None")
			{
				if (card.getName() == "Mustang")
					distanceByMustang(player1, player3, player4, player2.getNrPlayer(), 0);
				if (card.getName() == "Luneta")
					distanceByLuneta(player2, 0);
				player1.addCard(std::move(card));
				ply = "Player" + std::to_string(player2.getNrPlayer()) + " chose to take " + cart + " from Player" + std::to_string(player2.getNrPlayer());
				logger.log(ply, Logger::Level::Info);
				ply.clear();
				break;
			}
			else {
				std::cout << "Aceasta carte nu este activata" << std::endl;
				wait.wait(5);
			}
		}
	}break;
	}
}

void Game::panicaActionAI(Player & player1, Player & player2, Player & player3, Player & player4, const int & nrCarte)
{

	
	if (player2.getGun().getDistance() > player1.getGun().getDistance())
	{
		player1.addCard(player2.discardGun());
	}
	else
		if (player2.getNrActiveCards() > 0)
		{
			Cards card = player2.discardActiveCard("Butoi");
			if (card.getName() != "None")
			{
				if (card.getName() == "Butoi")
				{
					player1.addCard(std::move(card));
				}

			}
			else
			{
				card = player2.discardActiveCard("Mustang");
				if (card.getName() != "None")
				{
					if (card.getName() == "Mustang")
					{
						distanceByMustang(player1, player3, player4, player2.getNrPlayer(), 0);
						player1.addCard(std::move(card));
					}
				}
				else
				{
					card = player2.discardActiveCard("Luneta");
					if (card.getName() != "None")
					{
						if (card.getName() == "Luneta")
						{
							distanceByLuneta(player2, 0);
							player1.addCard(std::move(card));
						}
					}
				}
			}

		}
		else
		{
			unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
			int carte = rand() % player2.getNumberOfCards() + 1;
			player1.addCard(player2.discardCard(carte - 1));
		}
}

void Game::catBalouAction(Player & player1, Player & player2, Player& player3, Player& player4)
{
	std::string ply;
	system("cls");
	std::cout << "Esti player"+std::to_string(player2.getNrPlayer())<<" si trebuie sa decartezi o carte\n";
	player2.showCards(2);
	int pachet = 0;
	std::cout << "1: Carti in mana\n";
	std::cout << "2: Carti activate\n";
	while (true)
	{
		std::cin >> pachet;
		if (pachet!=1&&pachet!=2)
		{
			std::cout << "Nu ati ales o optiune valida" << std::endl;
		}
		else if (pachet == 2 && player2.getNrActiveCards() == 0)
		{
			std::cout << "Nu aveti carti activate, alegeti optiunea 1" << std::endl;
		}
		else break;
	}
	switch (pachet)
	{
	case 1: {
		std::cout << "Alege Cartea\n";
		int cart;
		
		while (true)
		{
			std::cin >> cart;
			if (cart<1||cart>player2.getNumberOfCards())
			{
				std::cout << "Nu ati ales o caret existanta." << std::endl;
			}
			else break;
		}
		ply = "Player" + std::to_string(player2.getNrPlayer()) + " chose to discard card number " + std::to_string(cart) + " from his hands";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
		discardedCards.addCard(player2.discardCard(cart - 1));
	}break;
	case 2: {
		std::cout << "Alege NUMELE cartii\n";
		std::string cart;
		Functions wait;
		while (true)
		{
			while (true)
			{
				std::cin >> cart;
				if (cart != "Mustang"&&cart != "Luneta"&&cart != "Butoi")
				{
					std::cout << "Nu ati ales o carte care poate fi activata." << std::endl;
					wait.wait(5);
				}
				else break;
			}
			Cards card = player2.discardActiveCard(cart);
			if (card.getName() != "None")
			{
				if (card.getName() == "Mustang")
					distanceByMustang(player1, player3, player4, player2.getNrPlayer(), 0);
				if (card.getName() == "Luneta")
					distanceByLuneta(player2, 0);
				discardedCards.addCard(std::move(card));
				ply = "Player" + std::to_string(player2.getNrPlayer()) + " chose to discard " + card.getName();
				logger.log(ply, Logger::Level::Info);
				ply.clear();
				break;
			}
			else {
				std::cout << "Nu ai aceasta carte activata" << std::endl;
				wait.wait(5);
			}
		}
	}break;
	}
}

void Game::catBalouActionAI(Player & player1, Player & player2, Player & player3, Player & player4)
{
	if (player2.getNumberOfCards() >= 1)
		discardedCards.addCard(player2.discardCard(player2.cardWithLessPriority()));
	else
	{
		if (player2.getNrActiveCards() > 0)
		{
			Cards card = player2.discardActiveCard("Butoi");
			if (card.getName() != "None")
			{
				if (card.getName() == "Butoi")
				{
					discardedCards.addCard(std::move(card));
				}

			}
			else
			{
				card = player2.discardActiveCard("Mustang");
				if (card.getName() != "None")
				{
					if (card.getName() == "Mustang")
					{
						distanceByMustang(player1, player3, player4, player2.getNrPlayer(), 0);
						discardedCards.addCard(std::move(card));
					}
				}
				else
				{
					card = player2.discardActiveCard("Luneta");
					if (card.getName() != "None")
					{
						if (card.getName() == "Luneta")
						{
							distanceByLuneta(player2, 0);
							discardedCards.addCard(std::move(card));
						}
					}
				}
			}

		}
		else
			if (player2.getGunName() != "Colt 45")
				discardedCards.addCard(player2.discardGun());
	}

	
}

void Game::gatlingActionForOnPlayer(Player& player1, Player& player2, Player& player3, Player& player4)
{
	std::string ply;
	player2.updateLives(0);
	if (player2.getCharacterName() == Characters::Name::BartCassidy && player2.isAlive() == true)
	{
		takeACard(player2);
		ply = "Player" + std::to_string(player2.getNrPlayer()) + "  lost a life";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
	}
	if (player2.getCharacterName() == Characters::Name::ElGringo && player2.isAlive() == true)
	{
		if(player2.getNrPlayer()==1)
			isElGringo(player1, player2);
		else
			isElGringoAI(player1, player2);
	}
	if (player2.getNumberOfLives() == 0)
	{
		Cards beer = player2.isBeer();
		if (beer.getName() == "Bere")
		{
			discardedCards.addCard(std::move(beer));
			player2.updateLives(1);
			ply = "Player" + std::to_string(player2.getNrPlayer()) + "  used "+ beer.getName();
			logger.log(ply, Logger::Level::Info);
			ply.clear();
		}
		else
			if (player2.getCharacterName() == Characters::Name::SidKetchum && player2.getNumberOfCards() >= 2)
			{
				if(player2.getNrPlayer()==1)
					isSidKetchum(player2);
				else
					isSidKetchumAI(player2);
			}
			else
				if (player1.getCharacterName() == Characters::Name::VultureSam)
				{
					VulturSem(player1, player2);
					distanceAfterDead(player1, player3, player4);
				}
				else if (player3.getCharacterName() == Characters::Name::VultureSam && player3.isAlive() == true)
				{
					VulturSem(player3, player2);
					distanceAfterDead(player1, player3, player4);
				}
				else if (player4.getCharacterName() == Characters::Name::VultureSam &&player3.isAlive() == true)
				{
					VulturSem(player4, player2);
					distanceAfterDead(player1, player3, player4);
				}
				else
				{
					player2.discardALLCards(discardedCards);
					distanceAfterDead(player1, player3, player4);
				}
	}
}
void Game::gatlingAction(Player & player1, Player & player2, Player & player3, Player & player4)
{
	gatlingActionForOnPlayer(player1, player2, player3, player4);
	gatlingActionForOnPlayer(player1, player3, player2, player4);
	gatlingActionForOnPlayer(player1, player4, player3, player2);
}

void Game::salonAction(Player & player1, Player & player2, Player & player3, Player & player4)
{
	std::string ply;
	if (player1.getNumberOfLives() < 5)
	{
		player1.updateLives(1);
		ply = "Player" + std::to_string(player1.getNrPlayer()) + " took life from salon ";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
	}
	if (player2.getNumberOfLives() < 5 && player2.isAlive()==true)
	{
		player2.updateLives(1);
		ply = "Player" + std::to_string(player2.getNrPlayer()) + " took life from salon ";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
	}
	if (player3.getNumberOfLives() < 5 && player3.isAlive()==true)
	{
		player3.updateLives(1);
		ply = "Player" + std::to_string(player3.getNrPlayer()) + " took life from salon ";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
	}
	if (player4.getNumberOfLives() < 5 && player4.isAlive()==true)
	{
		player4.updateLives(1);
		ply = "Player" + std::to_string(player4.getNrPlayer()) + " took life from c ";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
	}
}

void Game::startTurnPlayer(Player & player1, Player & player2, Player & player3, Player & player4)
{
	std::string ply;
	const int nrP2 = player2.getNrPlayer();
	const int nrP3 = player3.getNrPlayer(), nrP4 = player4.getNrPlayer();
	if (player1.getCharacterName() == Characters::Name::JesseJones)
	{
		int op;
		while (true)
		{
			system("cls");
			std::cout << "Vrei sa tragi o carte de la un jucator\n";
			std::cout << "1:Da\n";
			std::cout << "2:Nu\n";
			if (player1.getRole() == Roles::Rol::Outlaw)
			{
				std::cout << "Hint: Poti sa alegi sa iei o carte de la serif (player1), pentru a-i micsora sansele de a se apara." << std::endl;
			}
			std::cin >> op;
			if (op != 1 &&op != 2)
			{
				std::cout << "Nu ai ales o optiune valida" << std::endl;
				Functions f; f.wait(5);
			}
			else break;
		}
		switch (op)
		{
		case 1:
		{
			std::cout << "Alege jucatorul\n";
			int jucator;
			std::cin >> jucator;
			ply = "Player" + std::to_string(player1.getNrPlayer()) + "  chose to take a card from Player" + std::to_string(jucator);
			logger.log(ply, Logger::Level::Info);
			ply.clear();
			if (jucator == nrP2)
			{
				system("cls");
				std::cout << "Alege Cartea\n";
				std::cout << "Alege un umar intre 1 si " << player2.getNumberOfCards() << std::endl;
				int cart;
				while (true)
				{
					std::cin >> cart;
					if (cart<1 || cart>player2.getNumberOfCards())
					{
						std::cout << "Nu ati ales o caret existanta." << std::endl;
					}
					else break;
				}
				player1.addCard(player2.discardCard(cart - 1));
			}
			if (jucator == nrP3)
			{
				system("cls");
				std::cout << "Alege Cartea\n";
				std::cout << "Alege un umar intre 1 si " << player3.getNumberOfCards() << std::endl;
				int cart;
				while (true)
				{
					std::cin >> cart;
					if (cart<1||cart>player3.getNumberOfCards())
					{
						std::cout << "Nu ati ales o caret existanta." << std::endl;
					}
					else break;
				}
				player1.addCard(player3.discardCard(cart - 1));
			}
			if (jucator == nrP4)
			{
				system("cls");
				std::cout << "Alege Cartea\n";
				std::cout << "Alege un umar intre 1 si " << player4.getNumberOfCards() << std::endl;
				int cart;
				
				while (true)
				{
					std::cin >> cart;
					if (cart<1||cart>player4.getNumberOfCards())
					{
						std::cout << "Nu ati ales o carte existanta." << std::endl;
					}
					else break;
				}
				player1.addCard(player4.discardCard(cart - 1));
			}
			player1.addCard(unusedCards.pickCards());
		}break;
		case 2: {
			player1.addCard(unusedCards.pickCards());
			player1.addCard(unusedCards.pickCards());
			ply = "Player" + std::to_string(player1.getNrPlayer()) + " took 2 cards";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
		}break;
		}
	}
	if (player1.getCharacterName() != Characters::Name::LuckyDuke && player1.getCharacterName() != Characters::Name::KitCarlson && player1.getCharacterName() != Characters::Name::JesseJones &&player1.getCharacterName() != Characters::Name::PedroRamirez)
	{
		player1.addCard(unusedCards.pickCards());
		player1.addCard(unusedCards.pickCards());
		ply = "Player" + std::to_string(player1.getNrPlayer()) + " took 2 cards";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
		if (player1.getCharacterName() == Characters::Name::BlackJack)
		{
			if (player1.getCard(player1.getNumberOfCards() - 1).getNumber().second == Cards::symbol::Hearts ||
				player1.getCard(player1.getNumberOfCards() - 1).getNumber().second == Cards::symbol::Tiles)
			{
				system("cls");
				std::cout << "A doua carte pe care ai tras-o era Caro sau Cupa si ai luat o carte in plus\n";
				player1.addCard(unusedCards.pickCards());
				ply = "Player" + std::to_string(player1.getNrPlayer()) + " took an extra card";
				logger.log(ply, Logger::Level::Info);
				ply.clear();
				Functions wait; wait.wait(5);
			}
		}
	}
	else
		if (player1.getCharacterName() == Characters::Name::LuckyDuke)
		{
			int ct = 0;
			while (ct < 2)
			{
				int op;
				Cards c1, c2;
				while (true)
				{
					system("cls");
					std::cout << "---------------Alege una dintre carti---------------\n";
					c1 = unusedCards.pickCards(), c2 = unusedCards.pickCards();
					std::cout << "\n----------1-----------\n";
					std::cout << c1;
					std::cout << "\n----------2-----------\n";
					std::cout << c2;
					if (c1.getPriority() > c2.getPriority())
						std::cout << "Hint: Prima carte este considerata a fi mai folositoare, o poti alege pe aceea." << std::endl;
					else if (c1.getPriority() < c2.getPriority())
						std::cout << "Hint: A doua carte este considerata a fi mai folositoare, o poti alege pe aceea." << std::endl;
					std::cin >> op;
					if (op != 1 && op != 2)
					{
						std::cout << "Nu ai ales o optiune valida" << std::endl;
						Functions f; f.wait(5);
					}
					else break;
				}
				ply = "Player" + std::to_string(player1.getNrPlayer()) + " chose card number " + std::to_string(op);
				logger.log(ply, Logger::Level::Info);
				ply.clear();
				switch (op)
				{
				case 1:player1.addCard(std::move(c1)); discardedCards.addCard(std::move(c2)); break;
				case 2:player1.addCard(std::move(c2)); discardedCards.addCard(std::move(c1)); break;
				}
				ct++;
			}
		}
		else
			if (player1.getCharacterName() == Characters::Name::KitCarlson)
			{
				int op;
				Cards c1, c2, c3;
				while (true)
				{
					std::pair<int, int> prio;
					std::cout << "---------------Alege carte pe care nu vrei sa o pastrezi---------------\n";
					c1 = unusedCards.pickCards(), c2 = unusedCards.pickCards(), c3 = unusedCards.pickCards();
					std::cout << "\n----------1-----------\n";
					std::cout << c1;
					std::cout << "\n----------2-----------\n";
					std::cout << c2;
					std::cout << "\n----------3-----------\n";
					std::cout << c3;	
					if (prio.first != 0)
						std::cout << "Hint:Ai putea sa alegi cartile " << prio.first << " si " << prio.second << std::endl;
					std::cin >> op;
					if (op != 1 && op != 2&&op!=3)
					{
						std::cout << "Nu ai ales o optiune valida" << std::endl;
						Functions f; f.wait(5);
					}
					else break;
				}
				ply = "Player" + std::to_string(player1.getNrPlayer()) + " chose not to keep card number " + std::to_string(op);
				logger.log(ply, Logger::Level::Info);
				ply.clear();
				switch (op)
				{
				case 1:player1.addCard(std::move(c2)); player1.addCard(std::move(c3)); discardedCards.addCard(std::move(c1)); break;
				case 2:player1.addCard(std::move(c1)); player1.addCard(std::move(c3)); discardedCards.addCard(std::move(c2)); break;
				case 3:player1.addCard(std::move(c2)); player1.addCard(std::move(c1)); discardedCards.addCard(std::move(c3)); break;
				}
			}
	if (player1.getCharacterName() == Characters::Name::PedroRamirez && discardedCards.getDiscardedCards().size() != 0)
	{
		discardedCards.showTheLastCard();
		int op;
		while (true)
		{
			system("cls");
			std::cout << "Vrei sa tragi ultima carte din teancul de decartate?\n";
			std::cout << "1:Da\n";
			std::cout << "2:Nu\n";
			if (discardedCards.getPriority() <= 3)
				std::cout << "Hint: Ai putea alege sa tragi ultima carte din teancul de decartate, deoarece este o carte cu prioritate ridicata.\n";
			else std::cout << "Hint: Ai putea  sa  nu tragi ultima carte din teancul de decartate, deoarece este o carte cu prioritate redusa.\n";

			std::cin >> op;
			if (op != 1 && op != 2)
			{
				std::cout << "Nu ai ales o optiune valida" << std::endl;
				Functions f; f.wait(5);
			}
			else break;
		}
		switch (op)
		{
		case 1: {
			ply = "Player" + std::to_string(player1.getNrPlayer()) + " chose take last card from discarted cards ";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
			player1.addCard(discardedCards.pickLastCard()); player1.addCard(unusedCards.pickCards()); 
		}break;
		case 2: {
			ply = "Player" + std::to_string(player1.getNrPlayer()) + " chose not to take last card from discarted cards ";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
			player1.addCard(unusedCards.pickCards()); player1.addCard(unusedCards.pickCards()); 
		}break;
		}
	}
}
void Game::startTurnPlayerAI(Player & player1, Player & player2, Player & player3, Player & player4)
{
	std::string ply;
	const int nrP2 = player2.getNrPlayer();
	const int nrP3 = player3.getNrPlayer(), nrP4 = player4.getNrPlayer();
	if (player1.getCharacterName() == Characters::Name::JesseJones)
	{
		if (player1.getRole() == Roles::Rol::Outlaw)
		{
			if (player2.getRole() == Roles::Rol::Sheriff)
			{
				unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
				int carte = rand() % player2.getNumberOfCards() + 1;
				player1.addCard(player2.discardCard(carte - 1));
				ply = "Player" + std::to_string(player1.getNrPlayer()) + "  chose to take a card from Player" + std::to_string(player2.getNrPlayer());
				logger.log(ply, Logger::Level::Info);
				ply.clear();
			}
			if (player3.getRole() == Roles::Rol::Sheriff)
			{
				unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
				int carte = rand() % player3.getNumberOfCards() + 1;
				player1.addCard(player3.discardCard(carte - 1));
				ply = "Player" + std::to_string(player1.getNrPlayer()) + "  chose to take a card from Player" + std::to_string(player3.getNrPlayer());
				logger.log(ply, Logger::Level::Info);
				ply.clear();
			}
			if (player4.getRole() == Roles::Rol::Sheriff)
			{
				unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
				int carte = rand() % player4.getNumberOfCards() + 1;
				player1.addCard(player4.discardCard(carte - 1));
				ply = "Player" + std::to_string(player1.getNrPlayer()) + "  chose to take a card from Player" + std::to_string(player4.getNrPlayer());
				logger.log(ply, Logger::Level::Info);
				ply.clear();
			}
		}
		else
		{

			unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
			int op = rand() % 2 + 1;
			if (op == 1)
			{
				int jucator = rand() % 4 + 1;
				while (jucator == player1.getNrPlayer())
				{
					jucator = rand() % 4 + 1;
				}
				if (player2.getNrPlayer() == jucator)
				{
					unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
					int carte = rand() % player2.getNumberOfCards() + 1;
					player1.addCard(player2.discardCard(carte - 1));
					ply = "Player" + std::to_string(player1.getNrPlayer()) + "  chose to take a card from Player" + std::to_string(jucator);
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				if (player3.getNrPlayer() == jucator)
				{
					unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
					int carte = rand() % player3.getNumberOfCards() + 1;
					player1.addCard(player3.discardCard(carte - 1));
					ply = "Player" + std::to_string(player1.getNrPlayer()) + "  chose to take a card from Player" + std::to_string(jucator);
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				if (player4.getNrPlayer() == jucator)
				{
					unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
					int carte = rand() % player4.getNumberOfCards() + 1;
					player1.addCard(player4.discardCard(carte - 1));
					ply = "Player" + std::to_string(player1.getNrPlayer()) + "  chose to take a card from Player" + std::to_string(jucator);
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
			}
			else
			{
				player1.addCard(unusedCards.pickCards());
				player1.addCard(unusedCards.pickCards());
				ply = "Player" + std::to_string(player1.getNrPlayer()) + " took 2 cards";
				logger.log(ply, Logger::Level::Info);
				ply.clear();
			}
		}
	}
	if (player1.getCharacterName() != Characters::Name::LuckyDuke && player1.getCharacterName() != Characters::Name::KitCarlson && player1.getCharacterName() != Characters::Name::JesseJones &&player1.getCharacterName() != Characters::Name::PedroRamirez)
	{
		player1.addCard(unusedCards.pickCards());
		player1.addCard(unusedCards.pickCards());
		ply = "Player" + std::to_string(player1.getNrPlayer()) + " took 2 cards";
		logger.log(ply, Logger::Level::Info);
		ply.clear();
		if (player1.getCharacterName() == Characters::Name::BlackJack)
		{
			if (player1.getCard(player1.getNumberOfCards() - 1).getNumber().second == Cards::symbol::Hearts ||
				player1.getCard(player1.getNumberOfCards() - 1).getNumber().second == Cards::symbol::Tiles)
			{
				player1.addCard(unusedCards.pickCards());
				ply = "Player" + std::to_string(player1.getNrPlayer()) + " took an extra card";
				logger.log(ply, Logger::Level::Info);
				ply.clear();
			}
		}
	}
	else
		if (player1.getCharacterName() == Characters::Name::LuckyDuke)
		{
			int ct = 0;
			while (ct < 2)
			{
				int op;
				Cards c1, c2;
				c1 = unusedCards.pickCards(), c2 = unusedCards.pickCards();
				if (c1.getPriority() < c2.getPriority())
				{
					ply = "Player" + std::to_string(player1.getNrPlayer()) + " chose card  " + c1.getName();
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					player1.addCard(std::move(c1));
					discardedCards.addCard(std::move(c2));
				}
				else if (c1.getPriority() > c2.getPriority())
				{
					ply = "Player" + std::to_string(player1.getNrPlayer()) + " chose card  " + c2.getName();
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					player1.addCard(std::move(c2));
					discardedCards.addCard(std::move(c1));
				}
				ct++;
			}
		}
		else
			if (player1.getCharacterName() == Characters::Name::KitCarlson)
			{
				int op;
				Cards c1, c2, c3;
				std::pair<int, int> prio;
				c1 = unusedCards.pickCards(), c2 = unusedCards.pickCards(), c3 = unusedCards.pickCards();
				prio = maxPrio(c1.getPriority(), c2.getPriority(), c3.getPriority());
				if (prio.first == 1 && prio.second == 2)
				{
					ply = "Player" + std::to_string(player1.getNrPlayer()) + " chose not to keep card number " + std::to_string(3);
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					player1.addCard(std::move(c1)); player1.addCard(std::move(c2)); discardedCards.addCard(std::move(c3));
				}
				if (prio.first == 1 && prio.second == 3)
				{
					ply = "Player" + std::to_string(player1.getNrPlayer()) + " chose not to keep card number " + std::to_string(2);
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					player1.addCard(std::move(c1)); player1.addCard(std::move(c3)); discardedCards.addCard(std::move(c2));
				}
				if (prio.first == 2 && prio.second == 3)
				{
					ply = "Player" + std::to_string(player1.getNrPlayer()) + " chose not to keep card number " + std::to_string(1);
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					player1.addCard(std::move(c2)); player1.addCard(std::move(c3)); discardedCards.addCard(std::move(c1));
				}
			}
	if (player1.getCharacterName() == Characters::Name::PedroRamirez && discardedCards.getDiscardedCards().size() != 0)
	{
		//discardedCards.showTheLastCard();
		system("cls");
		std::cout << "Vrei sa tragi ultima carte din teancul de decartate?\n";
		std::cout << "1:Da\n";
		std::cout << "2:Nu\n";
		if (discardedCards.getPriority() <= 3)
		{
			ply = "Player" + std::to_string(player1.getNrPlayer()) + " chose take last card from discarted cards ";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
			player1.addCard(discardedCards.pickLastCard()); player1.addCard(unusedCards.pickCards());
		}
		else {
			ply = "Player" + std::to_string(player1.getNrPlayer()) + " chose not to take last card from discarted cards ";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
			player1.addCard(unusedCards.pickCards()); player1.addCard(unusedCards.pickCards());
		}

	}
	
}
void Game::run()
{
	//-----------Initializari--------------------------
	logger.log("Game Start", Logger::Level::Info);
	unusedCards.citire(createPriority());
	logger.log("UnusedCards was read", Logger::Level::Info);
	std::cout << "\n\n\n\n\n\n\n\n\n\n\n\n";
	std::cout << "                                                 START GAME                               ";
	Functions wait; wait.wait(4500);
	system("cls");
	Characters characters;



	//Numbers for rols
	std::array <int, 4>roles = { 1,2,2,3 };
	chooseRoles(roles);

	Player player1, player2, player3, player4;
	player1 = constructPlayer(roles[0], characters, 1);
	player2 = constructPlayerAI(roles[1], characters, 2);
	player3 = constructPlayerAI(roles[2], characters, 3);
	player4 = constructPlayerAI(roles[3], characters, 4);
	whoIsSherif(player1, player2, player3, player4);
	verificare(player1, player2, player3, player4);
	logger.log("Players were initialaize", Logger::Level::Info);
	
	while (true)
	{
		system("cls");
		
		logger.log("Player"+ std::to_string(player1.getNrPlayer())+" turn", Logger::Level::Info);
		if (player1.isAlive() == true )
		{
			if (player1.getNrPlayer() == 1)
			{
				if (tunPlayer(player1, player2, player3, player4) == 2)
				{
					system("cls");
					std::cout << "\n\n\n\n\n------------------Serif winnn------------------------- \n\n\n\n\n";
					logger.log("Serif winnn", Logger::Level::Info);
					break;
				}
			}
			else
			{
				if (tunPlayerAI(player1, player2, player3, player4) == 2)
				{
					system("cls");
					std::cout << "\n\n\n\n\n------------------Serif winnn------------------------- \n\n\n\n\n";
					logger.log("Serif winnn", Logger::Level::Info);
					break;
				}
			}
		}
		system("cls");
		if (player2.isAlive())
		{
			
			logger.log("Player" + std::to_string(player1.getNrPlayer()) + " turn", Logger::Level::Info);
			if (player2.getNrPlayer() == 1)
			{
				if (tunPlayer(player2, player1, player3, player4) == 1)
				{
					if (isOutlaw(player2, player3, player4) == true)
					{
						system("cls");
						std::cout << "\n\n\n\n\n------------------Outlaw winnn------------------------- \n\n\n\n\n";
						logger.log("Outlaw winnn", Logger::Level::Info);
					}
					else
					{
						system("cls");
						std::cout << "\n\n\n\n\n------------------Renegat winnn------------------------- \n";
						logger.log("Renegat winnn", Logger::Level::Info);
					}
					break;
				}
			}
			else
			{
				if (tunPlayerAI(player2, player1, player3, player4) == 1)
				{
					if (isOutlaw(player2, player3, player4) == true)
					{
						system("cls");
						std::cout << "\n\n\n\n\n------------------Outlaw winnn------------------------- \n\n\n\n\n";
						logger.log("Outlaw winnn", Logger::Level::Info);
					}
					else
					{
						system("cls");
						std::cout << "\n\n\n\n\n------------------Renegat winnn------------------------- \n";
						logger.log("Renegat winnn", Logger::Level::Info);
					}
					break;
				}
			}
		}
		system("cls");
		if (player3.isAlive())
		{
			
			logger.log("Player" + std::to_string(player3.getNrPlayer()) + " turn", Logger::Level::Info);
			if (player3.getNrPlayer() == 1)
			{
				if (tunPlayer(player3, player1, player2, player4) == 1)
				{
					if (isOutlaw(player2, player3, player4) == true)
					{
						system("cls");
						std::cout << "\n\n\n\n\n------------------Outlaw winnn------------------------- \n\n\n\n\n";
						logger.log("Outlaw winnn", Logger::Level::Info);
					}
					else
					{
						system("cls");
						std::cout << "\n\n\n\n\n------------------Renegat winnn------------------------- \n";
						logger.log("Renegat winnn", Logger::Level::Info);
					}
					break;
				}
			}
			else
			{
				if (tunPlayerAI(player3, player1, player2, player4) == 1)
				{
					if (isOutlaw(player2, player3, player4) == true)
					{
						system("cls");
						std::cout << "\n\n\n\n\n------------------Outlaw winnn------------------------- \n\n\n\n\n";
						logger.log("Outlaw winnn", Logger::Level::Info);
					}
					else
					{
						system("cls");
						std::cout << "\n\n\n\n\n------------------Renegat winnn------------------------- \n";
						logger.log("Renegat winnn", Logger::Level::Info);
					}
					break;
				}
			}
		}
		system("cls");
		if (player4.isAlive())
		{
			
			logger.log("Player" + std::to_string(player4.getNrPlayer()) + " turn", Logger::Level::Info);
			if (player4.getNrPlayer() == 1)
			{
				if (tunPlayer(player4, player1, player3, player2) == 1)
				{
					if (isOutlaw(player2, player3, player4) == true)
					{
						system("cls");
						std::cout << "\n\n\n\n\n------------------Outlaw winnn------------------------- \n\n\n\n\n";
						logger.log("Outlaw winnn", Logger::Level::Info);
					}
					else
					{
						system("cls");
						std::cout << "\n\n\n\n\n------------------Renegat winnn------------------------- \n";
						logger.log("Renegat winnn", Logger::Level::Info);
					}
					break;
				}
			}
			else {
				if (tunPlayerAI(player4, player1, player3, player2) == 1)
				{
					if (isOutlaw(player2, player3, player4) == true)
					{
						system("cls");
						std::cout << "\n\n\n\n\n------------------Outlaw winnn------------------------- \n\n\n\n\n";
						logger.log("Outlaw winnn", Logger::Level::Info);
					}
					else
					{
						system("cls");
						std::cout << "\n\n\n\n\n------------------Renegat winnn------------------------- \n";
						logger.log("Renegat winnn", Logger::Level::Info);
					}
					break;
				}
			}
		}
	}
}

void Game::lunMusBut(Player & player1)
{
	if (player1.hasButoi(1) == false)
		if (player1.hasButoi(2) == true)
			std::cout << "Hint: Ai un butoi, ai putea sa-l activezi pentru a incerca sa contracarezi actiunea unei carti Bang!.\n";
	if (player1.hasMustang(1) == false)
		if (player1.hasMustang(2) == true)
			std::cout << "Hint: Ai un mustang, ai putea sa-l activezi pentru a mari distanta la care te vad ceilalti jucatori.\n";
	if (player1.hasLuneta(1) == false)
		if (player1.hasLuneta(2) == true)
			std::cout << "Hint: Ai o luneta, ai putea sa-l activezi pentru a micsora distanta la care ii vezi pe ceilalti jucatori.\n";
}
void Game::arma(Player & player1)
{
	if (player1.distanceGunInHeand() > player1.getGun().getDistance())
		if (player1.getGun().getName() != "Volcanic")
			std::cout << "Hint: In mana ai o arma cu o distanta de tragere mai mare, ai putea sa o activezi.\n";
		else if (player1.getRole() == Roles::Rol::Outlaw&& player1.getDistance(0) == 1)
			std::cout << "Hint: In mana ai o arma cu o distanta de tragere mai mare, dar tu esti bandit si distanta fata de serif este de 1 si te-ar ajuta mai mult Volcanicul pe care il ai activat.\n";

}
void Game::bang(Player & player1)
{
	if (player1.getRole() == Roles::Rol::Outlaw&& player1.getDistance(0) <= player1.getGun().getDistance())
		std::cout << "Hint: Esti bandit si poti sa tragi in serif, daca il omori castigi.\n";
	if (player1.getRole() == Roles::Rol::Sheriff)
		std::cout << "Hint: Esti serif si trebuie sa ii omori pe toti ceilalti ca sa castigi, alege jucatorul cu cele mai putine vieti pentru a mai diminua numarul de inamici in cazul in care reusesti sa-l omori.\n";
	if (player1.getRole() == Roles::Rol::Renegate)
		std::cout << "Hint: Esti renegat si trebuie sa ii omori pe toti ceilalti ca sa castigi, dar nu uita ca trebuie sa ramai ultimul in joc, trage in cineva si incerca sa le scazi la toti viata.\n";

}
void Game::lunMusButAI(Player & player1)
{
	std::string ply;
	if (player1.hasButoi(1) == false)
		if (player1.hasButoi(2) == true)
		{
			player1.activeCard(player1.hasButoiLocation());
			ply = "Player" + std::to_string(player1.getNrPlayer()) + " active Butoi ";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
		}
	if (player1.hasMustang(1) == false)
		if (player1.hasMustang(2) == true)
		{
			player1.activeCard(player1.hasMustangLocation());
			ply = "Player" + std::to_string(player1.getNrPlayer()) + " active Mustang ";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
		}
	if (player1.hasLuneta(1) == false)
		if (player1.hasLuneta(2) == true)
		{
			player1.activeCard(player1.hasLunetaLocation());
			ply = "Player" + std::to_string(player1.getNrPlayer()) + " active Luneta ";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
		}
}
void Game::armaAI(Player & player1)
{
	if (player1.distanceGunInHeand() > player1.getGun().getDistance())
		if (player1.getGun().getName() != "Volcanic")
		{
			Cards gun = player1.discardGun();
			if (gun.getName() != "None")
			{
				discardedCards.addCard(gun);
			}
			player1.setGun(player1.discardCard(player1.distanceGunInHeandLocation()));
		}
		else
			if (player1.getRole() == Roles::Rol::Outlaw&& player1.getDistance(0) == 1)
			{
				Cards gun = player1.discardGun();
				if (gun.getName() != "None")
				{
					discardedCards.addCard(gun);
				}
				player1.setGun(player1.discardCard(player1.distanceGunInHeandLocation()));
			}

}


int Game::tunPlayer(Player & player1, Player & player2, Player & player3, Player & player4)
{
	Functions wait;
	int tragere = 0;
	const int nrP2 = player2.getNrPlayer();
	const int nrP3 = player3.getNrPlayer(), nrP4 = player4.getNrPlayer();
	if (unusedCards.sizePackage() <= 2)
	{
		unusedCards = std::move(discardedCards);
		unusedCards.shuffleVector();
	}
	startTurnPlayer(player1, player2, player3, player4);
	int ok = 1;
	while (ok != 0)
	{
		system("cls");
		std::cout << player1;
		std::cout << "\n--------------------------------------\n";
		if(player2.getRole()==Roles::Rol::Sheriff)
			std::cout << "Player" << player2.getNrPlayer()<<" is Serif" << " lives:: " << player2.getNumberOfLives() << std::endl;
		else
			std::cout << "Player" << player2.getNrPlayer() << " lives:: " << player2.getNumberOfLives() << std::endl;
		if (player3.getRole() == Roles::Rol::Sheriff)
			std::cout << "Player" << player3.getNrPlayer() << " is Serif" << " lives:: " << player3.getNumberOfLives() << std::endl;
		else
			std::cout << "Player" << player3.getNrPlayer() << " lives:: " << player3.getNumberOfLives() << std::endl;
		if (player4.getRole() == Roles::Rol::Sheriff)
			std::cout << "Player" << player4.getNrPlayer() << " is Serif" << " lives:: " << player4.getNumberOfLives() << std::endl;
		else
			std::cout << "Player" << player4.getNrPlayer() << " lives:: " << player4.getNumberOfLives() << std::endl;

		std::cout << "\n---------Discard Card-------------\n";
		discardedCards.showTheLastCard();
		std::cout << "\n--------------------------------------\n";
		std::cout << "1:Activeaza o carte(Arma,Butoi,Mustang,Luneta)\n";
		std::cout << "2:Joaca o carte\n";
		std::cout << "3:Decarteaza o carte\n";
		std::cout << "4:Incheie tura\n";
		if (player1.getCharacterName() == Characters::Name::SidKetchum)
		{
			std::cout << "***********************************************\n";
			std::cout << "* 5:Decarteaza 2 carti pentru a primi o viata *\n";
			std::cout << "***********************************************\n";
			if (player1.getNumberOfLives() <= 2 && player1.getNumberOfCards() > 2)
				std::cout << "Hint: Ai putine vieti, poti alege sa decartezi doua carti pentru a micsora sansele de a fi dat afara din joc.\n";
		}
		int op;
		lunMusBut(player1);
		arma(player1);
		if (player1.hasSalon() == true && player1.getNumberOfLives() <= 2)
			std::cout << "Hint: Ai putine vieti si ai o carte Salon in mana ai putea sa o activezi.\n";
		if (player1.hasBere() == true && player1.getNumberOfLives() <= 4)
			std::cout << "Hint: Ai o bere in mana si nu ai atins numarul maxim de vieti, ai putea sa o activezi.\n";
		if (player1.hasGatling() == true)
			std::cout << "Hint: Ai o carte Gatling in mana si putea sa o joci, deoarece scade cate o viata la toti ceilalti.\n";

		if ((player1.getCharacterName() == Characters::Name::WillyTheKid || player1.getGun().getName() == "Volcanic") && player1.getNrOfBang() > 1)
		{
			if (player1.getRole() == Roles::Rol::Outlaw && player1.getDistance(0) <= player1.getGun().getDistance())
				std::cout << "Hint: Ai " << player1.getNrOfBang() << " de carti Bang si distanta iti permite sa tragi in serif, ai putea sa folosesti catile BANG pe acesta pentru a-ti creste sansele de castig.\n";
			else if (player1.getRole() == Roles::Rol::Sheriff)
				std::cout << "Hint: Ai " << player1.getNrOfBang() << " de carti Bang, le poti folosi pe oricine in functie de distanta fata de acestia.\n";
			else if (player1.getRole() == Roles::Rol::Renegate)
				std::cout << "Hint: Ai " << player1.getNrOfBang() << " de carti Bang, le poti folosi pe oricine in functie de distanta fata de acestia, dar nu uita esti Renegat si seriful trebuie eliminat ultimul.\n";
		}
		while (true)
		{
			std::cin >> op;
			if (player1.getCharacterName() == Characters::Name::SidKetchum)
			{
				if (op < 1 || op>5)
				{
					std::cout << "Nu ai ales o optiune valida" << std::endl;
					Functions f; f.wait(5);
				}
				else break;
			}
			else if (op<1||op>4)
			{
				std::cout << "Nu ai ales o optiune valida" << std::endl;
				Functions f; f.wait(5);
			}
			else break;
		}
		std::string ply = "Player";
		std::string nrPly = std::to_string(player1.getNrPlayer());
		ply += nrPly + " chose option " + std::to_string(op);
		logger.log(ply, Logger::Level::Info);
		ply.clear();

		
		switch (op)
		{
		case 1: {
			std::cout << "Alegeti carte pe care doriti sa o activati\n";
			int nrCarte;
			while (true)
			{
				std::cin >> nrCarte;
				if (nrCarte<1 || nrCarte>player1.getNumberOfCards())
				{
					std::cout << "Nu ai ales o carte existanta" << std::endl;
				}
				else break;
			}
			if (player1.getCard(nrCarte - 1).isGun() == true)
			{
				Cards gun = player1.discardGun();
				if (gun.getName() != "None")
				{
					discardedCards.addCard(gun);
					ply = "Player" + nrPly + " discarded a weapon";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				player1.setGun(player1.discardCard(nrCarte - 1));
				ply = "Player" + nrPly + " activated " + player1.getGunName();
				logger.log(ply, Logger::Level::Info);
				ply.clear();
			}
			else
			{
				if (player1.getCard(nrCarte - 1).getName() == "Butoi" && player1.isButoi().getName() == "None")
				{
					ply = "Player" + nrPly + " activated " + player1.getCard(nrCarte - 1).getName();
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					player1.activeCard(nrCarte - 1);
					
				}
				else
					if (player1.getCard(nrCarte - 1).getName() == "Butoi" && player1.isButoi().getName() != "None")
					{
						std::cout << "Nu poti activa decat o carte Butoi";
						logger.log("A barrel is already activated", Logger::Level::Warning);
						Functions wait; wait.wait(5);
						break;
					}
					else
						if (player1.getCard(nrCarte - 1).getName() == "Mustang" && player1.isMustang().getName() == "None")
						{
							player1.activeCard(nrCarte - 1);
							distanceByMustang(player2, player3, player4, player1.getNrPlayer(), 1);
							ply = "Player" + nrPly + " activated a Mustang";
							logger.log(ply, Logger::Level::Info);
							ply.clear();
						}
						else
							if (player1.getCard(nrCarte - 1).getName() == "Mustang" && player1.isMustang().getName() != "None")
							{
								std::cout << "Nu poti activa decat o carte Mustang";
								logger.log("A Mustang is already activated", Logger::Level::Warning);
								Functions wait; wait.wait(5);
								break;
							}
							else
								if (player1.getCard(nrCarte - 1).getName() == "Luneta"  && player1.isLuneta().getName() == "None")
								{
									player1.activeCard(nrCarte - 1);
									distanceByLuneta(player1, 1);
									ply = "Player" + nrPly + " activated a Luneta";
									logger.log(ply, Logger::Level::Info);
									ply.clear();
								}
								else
									if (player1.getCard(nrCarte - 1).getName() == "Luneta"  && player1.isLuneta().getName() != "None")
									{
										std::cout << "Nu poti activa decat o carte Luneta";
										logger.log("Luneta is already activated", Logger::Level::Warning);
										wait.wait(5);
										break;
									}
									else
									{
										std::cout << "Nu se poate activa\n";
										logger.log("This card can't be activated", Logger::Level::Warning);
										wait.wait(5);
										break;
									}
			}
			if (player1.getCharacterName() == Characters::Name::SuzyLafayette && player1.getNumberOfCards() == 0)
			{
				takeACard(player1);

			}
		} break;
		case 2: {
			std::cout << "Alegeti carte pe care doriti sa o jucati\n";
			int nrCarte;
			while (true)
			{
				std::cin >> nrCarte;
				if (nrCarte<1 || nrCarte>player1.getNumberOfCards())
				{
					std::cout << "Nu ai ales o carte existanta" << std::endl;
					wait.wait(5);
				}
				else break;
			}
			ply = "Player" + nrPly + " chose card " + player1.getCard(nrCarte - 1).getName();
			logger.log(ply, Logger::Level::Info);
			ply.clear();
			if (player1.getCard(nrCarte - 1).getName() == "Bere")
			{
				if (player1.getNumberOfLives() < 5)
				{
					player1.updateLives(1);
					discardedCards.addCard(player1.discardCard(nrCarte - 1));
					ply = "Player" + nrPly + " used a beer to take a life back";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				else
				{
					std::cout << "Nu poti activa aceasta carte, ai deja numarul maxim de vieti\n";
					wait.wait(5);
					logger.log("This card can not be used because you have max numbers of lifes", Logger::Level::Warning);
				}
			}
			else
				if (player1.getCard(nrCarte - 1).getName() == "Salon")
				{
					discardedCards.addCard(player1.discardCard(nrCarte - 1));
					salonAction(player1, player2, player3, player4);
					ply = "Player" + nrPly + " used Salon ";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				else
					if (player1.getCard(nrCarte - 1).getName() == "Gatling")
					{
						discardedCards.addCard(player1.discardCard(nrCarte - 1));
						gatlingAction(player1, player2, player3, player4);
						ply = "Player" + nrPly + " used Gatling";
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						if (isSherifAlive(player2, player3, player4) == false)
						{
							ok = 0;
							return 1;
						}
						if (OnlySherifAlive(player2, player3, player4) == true)
						{
							ok = 0;
							return 2;
						}
					}
					else
						if (player1.getCard(nrCarte - 1).getName() == "Diligenta")
						{
							player1.addCard(unusedCards.pickCards());
							player1.addCard(unusedCards.pickCards());
							discardedCards.addCard(player1.discardCard(nrCarte - 1));
							ply = "Player" + nrPly + " used Diligenta";
							logger.log(ply, Logger::Level::Info);
							ply.clear();
						}
						else
							if (player1.getCard(nrCarte - 1).getName() == "Wells Fargo")
							{
								player1.addCard(unusedCards.pickCards());
								player1.addCard(unusedCards.pickCards());
								player1.addCard(unusedCards.pickCards());
								discardedCards.addCard(player1.discardCard(nrCarte - 1));
								ply = "Player" + nrPly + " used Wells Fargo";
								logger.log(ply, Logger::Level::Info);
								ply.clear();
							}
							else
								if (player1.getCard(nrCarte - 1).getName() == "Cat Balou")
								{
									std::cout << "Alege jucatorul\n";
									int jucator;
									if (player1.getRole() == Roles::Rol::Outlaw)
										std::cout << "Hint: Alege seriful.\n";
									while (true)
									{
										std::cin >> jucator;
										if (jucator<1 || jucator>nrPlayers)
										{
											std::cout << "Nu ai ales un jucator disponibil" << std::endl;
											wait.wait(5);
										}else
										if (jucator == player1.getNrPlayer()) {
											std::cout << "Nu poti folosi aceasta carte pe tine" << std::endl;
											wait.wait(5);
										}
										else break;
									}
									if (jucator == nrP2 && player2.getNumberOfLives() != 0 && player2.getNumberOfCards()>0 )
									{
										discardedCards.addCard(player1.discardCard(nrCarte - 1));
										catBalouActionAI(player1, player2, player3, player4);
										ply = "Player" + nrPly + " used Cat Balou on player" + std::to_string(jucator);
										logger.log(ply, Logger::Level::Info);
										ply.clear();
									}
									else
										if (jucator == nrP3 && player3.getNumberOfLives() != 0 && player3.getNumberOfCards() > 0)
										{
											discardedCards.addCard(player1.discardCard(nrCarte - 1));
											catBalouActionAI(player1, player3, player2, player4);
											ply = "Player" + nrPly + " used Cat Balou on player" + std::to_string(jucator);
											logger.log(ply, Logger::Level::Info);
											ply.clear();
										}
										else
											if (jucator == nrP4 && player4.getNumberOfLives() != 0 && player4.getNumberOfCards() > 0)
											{
												discardedCards.addCard(player1.discardCard(nrCarte - 1));
												catBalouActionAI(player1, player4, player2, player3);
												ply = "Player" + nrPly + " used Cat Balou on player" + std::to_string(jucator);
												logger.log(ply, Logger::Level::Info);
												ply.clear();
											}
											else
											{
												std::cout << "Playerul nu este in viata";
												logger.log("Player is not alive", Logger::Level::Warning);
												wait.wait(5);
											}
								}
								else
									if (player1.getCard(nrCarte - 1).getName() == "Panica!")
									{
											std::cout << "Alege jucatorul\n";
											int jucator;
											while (true)
											{
												std::cin >> jucator;
												if (jucator<1 || jucator>nrPlayers)
												{
													std::cout << "Nu ati ales un jucator valid." << std::endl;
													wait.wait(5);
												}else
												if (jucator == player1.getNrPlayer()) {
													std::cout << "Nu poti folosi aceasta carte pe tine" << std::endl;
													wait.wait(5);
												}
												else break;
											}
											if (jucator == player1.getNrPlayer())
											{
												std::cout << "Nu poti folosi aceasta carte pe tine\n";
												logger.log("You can't use Panica! on you", Logger::Level::Warning);
												Functions wait; wait.wait(5);
											}
											else
											{
												if (player1.getDistance(jucator - 1) <= 1)
												{
													if (jucator == nrP2 && player2.getNumberOfLives() != 0 && player2.getNumberOfCards() > 0)
													{
														panicaAction(player1, player2, player3, player4, nrCarte);
														discardedCards.addCard(player1.discardCard(nrCarte - 1));
														ply = "Player" + nrPly + " used Panica! on player" + std::to_string(jucator);
														logger.log(ply, Logger::Level::Info);
														ply.clear();
													}
													if (jucator == nrP3 && player3.getNumberOfLives() != 0 && player3.getNumberOfCards() > 0)
													{
														panicaAction(player1, player3, player2, player4, nrCarte);
														discardedCards.addCard(player1.discardCard(nrCarte - 1));
														ply = "Player" + nrPly + " used Panica! on player" + std::to_string(jucator);
														logger.log(ply, Logger::Level::Info);
														ply.clear();
													}
													if (jucator == nrP4 && player4.getNumberOfLives() != 0 && player4.getNumberOfCards() > 0)
													{
														panicaAction(player1, player4, player2, player3, nrCarte);
														discardedCards.addCard(player1.discardCard(nrCarte - 1));
														ply = "Player" + nrPly + " used Panica! on player" + std::to_string(jucator);
														logger.log(ply, Logger::Level::Info);
														ply.clear();
													}
													break;
												}
												else
												{
													std::cout << "Nu poti folosi aceasta carte pe jucatorul selectat\n";
													logger.log("You can't use Panica! on this player,is too far", Logger::Level::Warning);
													wait.wait(5);
												}
											}
										
									}
									else
										if ((player1.getCard(nrCarte - 1).getName() == "Bang!" || (player1.getCard(nrCarte - 1).getName() == "Ratat!") && player1.getCharacterName() == Characters::Name::CalamityJanet))
										{
											bang(player1);
											if (tragere >= player1.getNrTrageri())
											{
												std::cout << "Ai folosit numarul maxim de carti bang pe tura\n";
												logger.log("You used the maxim numbers of bang", Logger::Level::Warning);
												Functions wait; wait.wait(5);
											}
											else
											{
												std::cout << "Alege jucatorul\n";
												int jucator;
												while (true)
												{
													std::cin >> jucator;
													if (jucator<1 || jucator>nrPlayers)
													{
														std::cout << "Nu ati ales un jucator valid." << std::endl;
														wait.wait(5);
													}
													else break;
												}
												if (jucator == player1.getNrPlayer())
												{
													std::cout << "Nu te poti impusca singur\n";
													logger.log("You can't use Bang on you", Logger::Level::Warning);
													Functions wait; wait.wait(5);
												}
												else
												{
													if (player1.getDistance(jucator - 1) <= player1.getGun().getDistance())
													{
														if (jucator == nrP2)
														{
															ply = "Player" + nrPly + " used Bang on player" + std::to_string(jucator);
															logger.log(ply, Logger::Level::Info);
															ply.clear();
															bangAction(player1, player2, player3, player4, nrCarte, tragere);
														}
														else
															if (jucator == nrP3)
															{
																ply = "Player" + nrPly + " used Bang on player" + std::to_string(jucator);
																logger.log(ply, Logger::Level::Info);
																ply.clear();
																bangAction(player1, player3, player2, player4, nrCarte, tragere);
															}
															else
																if (jucator == nrP4)
																{
																	ply = "Player" + nrPly + " used Bang on player" + std::to_string(jucator);
																	logger.log(ply, Logger::Level::Info);
																	ply.clear();
																	bangAction(player1, player4, player2, player3, nrCarte, tragere);
																}
														if (isSherifAlive(player2, player3, player4) == false)
														{
															ok = 0;
															return 1;
														}
														if (OnlySherifAlive(player2, player3, player4) == true)
														{
															ok = 0;
															return 2;
														}
													}
													else
													{
														std::cout << "Nu poti folosi aceasta carte pe jucatorul selectat\n";
														logger.log("You can't use Bang! on selected player because is too far", Logger::Level::Warning);
														wait.wait(5);
													}

												}
											}
										}
										else
										{
											std::cout << "Nu poti folosi aceasta carte ";
											wait.wait(5);
										}
			if (player1.getCharacterName() == Characters::Name::SuzyLafayette && player1.getNumberOfCards() == 0)
			{
				takeACard(player1);
			}
		}break;
		case 3: {
			std::cout << "Alege cartea pe care vrei sa o decartezi\n";
			int nrCart;
			while (true)
			{
				std::cin >> nrCart;
				if (nrCart<1 || nrCart>player1.getNumberOfCards())
				{
					std::cout << "Nu ati ales o carte existanta." << std::endl;
				}
				else break;
			}
			discardedCards.addCard(player1.discardCard(nrCart - 1));
			ply = "Player" + nrPly + " discarted a card";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
			if (player1.getCharacterName() == Characters::Name::SuzyLafayette && player1.getNumberOfCards() == 0)
			{
				takeACard(player1);
			}
		}break;
		case 4: {
			if (player1.getNumberOfLives() >= player1.getNumberOfCards())
			{
				ok = 0;
				break;
			}
			else
			{
				int nrCartiDecartat = player1.getNumberOfCards() - player1.getNumberOfLives();
				std::cout << "Trebuie sa decartezi " << nrCartiDecartat << " carti\n";
				ply = "Player" + nrPly + " has to discard " + std::to_string(nrCartiDecartat) + " cards";
				logger.log(ply, Logger::Level::Warning);
				ply.clear();
				for (auto it = 0; it < nrCartiDecartat; ++it)
				{
					std::cout << "Alege cartea\n ";
					int nrCart;
					while (true)
					{
						std::cin >> nrCart;
						if (nrCart<1 || nrCart>player1.getNumberOfCards())
						{
							std::cout << "Nu ati ales o carte existanta." << std::endl;
						}
						else break;
					}
					discardedCards.addCard(player1.discardCard(nrCart - 1));
					ply = "Player" + nrPly + "  discarded card number " + std::to_string(nrCart);
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				ok = 0;
				break;
			}
		}break;
		case 5: {
			if (player1.getCharacterName() != Characters::Name::SidKetchum)
				break;
			else
			{
				if (player1.getRole() == Roles::Rol::Sheriff && player1.getNumberOfLives() < 5 && player1.getNumberOfCards() >= 2)
				{
					std::cout << "Alege doua carti pe care vrei sa le decartezi\n";
					int c1, c2;
					while (true)
					{
						std::cout << "Cartea 1: "; std::cin >> c1;
						std::cout << "Cartea 2: "; std::cin >> c2;
						if ((c1<1 || c1>player1.getNumberOfCards()) && (c2 < 1 || c2 < player1.getNumberOfCards()))
						{
							std::cout << "Nu ati ales o caret existanta." << std::endl;
						}
						else break;
					}
					if (c1 < c2)
						std::swap(c1, c2);
					discardedCards.addCard(player1.discardCard(c1 - 1));
					discardedCards.addCard(player1.discardCard(c2 - 1));
					player1.updateLives(1);
					ply = "Player" + nrPly + "  discarded card number " + std::to_string(c1) + " and " + std::to_string(c2) + " to take a life back";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
				else
					if (player1.getRole() != Roles::Rol::Sheriff && player1.getNumberOfLives() < 4 && player1.getNumberOfCards() >= 2)
					{
						std::cout << "Alege doua carti pe care vrei sa le decartezi\n";
						int c1, c2;
						while (true)
						{
							std::cout << "Cartea 1: "; std::cin >> c1;
							std::cout << "Cartea 2: "; std::cin >> c2;
							if ((c1<1 || c1>player1.getNumberOfCards()) && (c2 < 1 || c2 < player1.getNumberOfCards()))
							{
								std::cout << "Nu ati ales o caret existanta." << std::endl;
							}
							else break;
						}
						if (c1 < c2)
							std::swap(c1, c2);
						discardedCards.addCard(player1.discardCard(c1 - 1));
						discardedCards.addCard(player1.discardCard(c2 - 1));
						player1.updateLives(1);
						ply = "Player" + nrPly + "  discarded card number " + std::to_string(c1) + " and " + std::to_string(c2) + " to take a life back";
						logger.log(ply, Logger::Level::Info);
						ply.clear();
					}
					else {
						std::cout << "Nu poti folosi aceasta optiune, numarul maxim de vieti a fost atins\n";
						wait.wait(5);
					}
			}
		}break;
		}
	}
	return 0;
}

int Game::tunPlayerAI(Player & player1, Player & player2, Player & player3, Player & player4)
{
	Functions wait;
	std::string ply;
	int tragere = 0;
	const int nrP2 = player2.getNrPlayer();
	const int nrP3 = player3.getNrPlayer(), nrP4 = player4.getNrPlayer();
	if (unusedCards.sizePackage() <= 2)
	{
		unusedCards = std::move(discardedCards);
		unusedCards.shuffleVector();
	}
	startTurnPlayerAI(player1, player2, player3, player4);
	int ok = 1;
	while (ok != 0)
	{
		system("cls");
		if (player2.getNrPlayer() == 1)
		{
			std::cout << player2;
			std::cout << "\n--------------------------------------\n";
			if (player3.getRole() == Roles::Rol::Sheriff)
				std::cout << "Player" << player3.getNrPlayer() << " is Serif" << " lives:: " << player3.getNumberOfLives() << std::endl;
			else
				std::cout << "Player" << player3.getNrPlayer() << " lives:: " << player3.getNumberOfLives() << std::endl;
			if (player1.getRole() == Roles::Rol::Sheriff)
				std::cout << "Player" << player1.getNrPlayer() << " is Serif" << " lives:: " << player1.getNumberOfLives() << std::endl;
			else
				std::cout << "Player" << player1.getNrPlayer() << " lives:: " << player1.getNumberOfLives() << std::endl;
			if (player4.getRole() == Roles::Rol::Sheriff)
				std::cout << "Player" << player4.getNrPlayer() << " is Serif" << " lives:: " << player4.getNumberOfLives() << std::endl;
			else
				std::cout << "Player" << player4.getNrPlayer() << " lives:: " << player4.getNumberOfLives() << std::endl;
		}
		if (player3.getNrPlayer() == 1)
		{
			std::cout << player3;
			if (player2.getRole() == Roles::Rol::Sheriff)
				std::cout << "Player" << player2.getNrPlayer() << " is Serif" << " lives:: " << player2.getNumberOfLives() << std::endl;
			else
				std::cout << "Player" << player2.getNrPlayer() << " lives:: " << player2.getNumberOfLives() << std::endl;
			if (player1.getRole() == Roles::Rol::Sheriff)
				std::cout << "Player" << player1.getNrPlayer() << " is Serif" << " lives:: " << player1.getNumberOfLives() << std::endl;
			else
				std::cout << "Player" << player1.getNrPlayer() << " lives:: " << player1.getNumberOfLives() << std::endl;
			if (player4.getRole() == Roles::Rol::Sheriff)
				std::cout << "Player" << player4.getNrPlayer() << " is Serif" << " lives:: " << player4.getNumberOfLives() << std::endl;
			else
				std::cout << "Player" << player4.getNrPlayer() << " lives:: " << player4.getNumberOfLives() << std::endl;
		}
		if (player4.getNrPlayer() == 1)
		{
			std::cout << player4;
			if (player3.getRole() == Roles::Rol::Sheriff)
				std::cout << "Player" << player3.getNrPlayer() << " is Serif" << " lives:: " << player3.getNumberOfLives() << std::endl;
			else
				std::cout << "Player" << player3.getNrPlayer() << " lives:: " << player3.getNumberOfLives() << std::endl;
			if (player1.getRole() == Roles::Rol::Sheriff)
				std::cout << "Player" << player1.getNrPlayer() << " is Serif" << " lives:: " << player1.getNumberOfLives() << std::endl;
			else
				std::cout << "Player" << player1.getNrPlayer() << " lives:: " << player1.getNumberOfLives() << std::endl;
			if (player2.getRole() == Roles::Rol::Sheriff)
				std::cout << "Player" << player2.getNrPlayer() << " is Serif" << " lives:: " << player2.getNumberOfLives() << std::endl;
			else
				std::cout << "Player" << player2.getNrPlayer() << " lives:: " << player2.getNumberOfLives() << std::endl;
		}
		
		std::cout << "\n---------Discard Card-------------\n";
		discardedCards.showTheLastCard();
		
		int op;
		lunMusButAI(player1);
		armaAI(player1);
		if (player1.hasSalon() == true && player1.getNumberOfLives() <= 2)
		{
			discardedCards.addCard(player1.discardCard(player1.hasSalonLocation()));
			salonAction(player1, player2, player3, player4);
			ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Salon";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
		}
		 if (player1.hasBere() == true && player1.getNumberOfLives() <= 4)
		{
			
			discardedCards.addCard(player1.discardCard(player1.hasBereLocation()));
			player1.updateLives(1);
			ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bere";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
		}
		 if (player1.hasGatling() == true)
		{
			discardedCards.addCard(player1.discardCard(player1.hasGatlingLocation()));
			gatlingAction(player1, player2, player3, player4);
			ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Gatling";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
			if (isSherifAlive(player2, player3, player4) == false)
			{
				ok = 0;
				return 1;
			}
			if (OnlySherifAlive(player2, player3, player4) == true)
			{
				ok = 0;
				return 2;
			}
		}

		 if ((player1.getCharacterName() != Characters::Name::WillyTheKid && player1.getGun().getName() != "Volcanic") && player1.getNrOfBang() > 1 && tragere==0)
		{
			if (player1.getRole() == Roles::Rol::Outlaw )
			{
				if (player2.getRole() == Roles::Rol::Sheriff  &&  player1.getDistance(player2.getNrPlayer() - 1) <= player1.getGun().getDistance())
				{
					ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player"+ std::to_string(player2.getNrPlayer());
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					bangAction(player1, player2, player3, player4, player1.getBangLocation()+1, tragere);
				}
				if (player3.getRole() == Roles::Rol::Sheriff &&  player1.getDistance(player3.getNrPlayer() - 1) <= player1.getGun().getDistance())
				{
					ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player3.getNrPlayer());
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					bangAction(player1, player3, player2, player4, player1.getBangLocation()+1, tragere);
				}
				if (player4.getRole() == Roles::Rol::Sheriff&& player1.getDistance(player4.getNrPlayer() - 1) <= player1.getGun().getDistance())
				{
					ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player4.getNrPlayer());
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					bangAction(player1, player4, player2, player4, player1.getBangLocation()+1, tragere);
				}
			}
			else if (player1.getRole() == Roles::Rol::Sheriff)
			{
				unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
				int jucator = rand() % 4 + 1;
				while (jucator == player1.getNrPlayer() || player1.getDistance(jucator - 1) <= player1.getGun().getDistance())
				{
					jucator = rand() % 4 + 1;
				}
				if (jucator == nrP2)
				{
					ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player2.getNrPlayer());
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					bangAction(player1, player2, player3, player4, player1.getBangLocation() + 1, tragere);
				}
				else
					if (jucator == nrP3)
					{
						ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player3.getNrPlayer());
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						bangAction(player1, player3, player2, player4, player1.getBangLocation() + 1, tragere);
					}
					else
						if (jucator == nrP4)
						{
							ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player4.getNrPlayer());
							logger.log(ply, Logger::Level::Info);
							ply.clear();
							bangAction(player1, player4, player2, player3, player1.getBangLocation() + 1, tragere);
						}
				if (isSherifAlive(player2, player3, player4) == false)
				{
					ok = 0;
					return 1;
				}
				if (OnlySherifAlive(player2, player3, player4) == true)
				{
					ok = 0;
					return 2;
				}
			}
			else if (player1.getRole() == Roles::Rol::Renegate)
			{
				unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
				int jucator = rand() % 4 + 1;
				while (jucator == player1.getNrPlayer() || player1.getDistance(jucator - 1) <= player1.getGun().getDistance())
				{
					jucator = rand() % 4 + 1;
				}
				if (jucator == nrP2)
				{
					ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player2.getNrPlayer());
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					bangAction(player1, player2, player3, player4, player1.getBangLocation() + 1, tragere);
				}
				else
					if (jucator == nrP3)
					{
						ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player3.getNrPlayer());
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						bangAction(player1, player3, player2, player4, player1.getBangLocation() + 1, tragere);
					}
					else
						if (jucator == nrP4)
						{
							ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player4.getNrPlayer());
							logger.log(ply, Logger::Level::Info);
							ply.clear();
							bangAction(player1, player4, player2, player3, player1.getBangLocation() + 1, tragere);
						}
				if (isSherifAlive(player2, player3, player4) == false)
				{
					ok = 0;
					return 1;
				}
				if (OnlySherifAlive(player2, player3, player4) == true)
				{
					ok = 0;
					return 2;
				}
			}
		}
		 if ((player1.getCharacterName() == Characters::Name::WillyTheKid || player1.getGun().getName() == "Volcanic") && player1.getNrOfBang() > 1)
		{
			if (player1.getRole() == Roles::Rol::Outlaw )
			{
				if (player2.getRole() == Roles::Rol::Sheriff &&  player1.getDistance(player2.getNrPlayer() - 1) <= player1.getGun().getDistance())
				{
					ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player2.getNrPlayer());
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					bangAction(player1, player2, player3, player4, player1.getBangLocation() + 1, tragere);
				}
				if (player3.getRole() == Roles::Rol::Sheriff&&  player1.getDistance(player3.getNrPlayer() - 1) <= player1.getGun().getDistance())
				{
					ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player3.getNrPlayer());
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					bangAction(player1, player3, player2, player4, player1.getBangLocation() + 1, tragere);
				}
				if (player4.getRole() == Roles::Rol::Sheriff &&  player1.getDistance(player4.getNrPlayer() - 1) <= player1.getGun().getDistance())
				{
					ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player4.getNrPlayer());
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					bangAction(player1, player4, player2, player4, player1.getBangLocation() + 1, tragere);
				}
			}
			else if (player1.getRole() == Roles::Rol::Sheriff)
			{
				unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
				int jucator= jucator = rand() % 4 + 1; ;
				while (jucator == player1.getNrPlayer() || player1.getDistance(jucator - 1) <= player1.getGun().getDistance())
				{
					jucator = rand() % 4 + 1;
				}
				if (jucator == nrP2)
				{
					ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player2.getNrPlayer());
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					bangAction(player1, player2, player3, player4, player1.getBangLocation() + 1, tragere);
				}
				else
					if (jucator == nrP3)
					{
						ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player3.getNrPlayer());
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						bangAction(player1, player3, player2, player4, player1.getBangLocation() + 1, tragere);
					}
					else
						if (jucator == nrP4)
						{
							ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player4.getNrPlayer());
							logger.log(ply, Logger::Level::Info);
							ply.clear();
							bangAction(player1, player4, player2, player3, player1.getBangLocation() + 1, tragere);
						}
				if (isSherifAlive(player2, player3, player4) == false)
				{
					ok = 0;
					return 1;
				}
				if (OnlySherifAlive(player2, player3, player4) == true)
				{
					ok = 0;
					return 2;
				}
			}
			else if (player1.getRole() == Roles::Rol::Renegate)
			{
				unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
				int jucator= jucator = rand() % 4 + 1;;
				while (jucator == player1.getNrPlayer() || player1.getDistance(jucator - 1) <= player1.getGun().getDistance())
				{
					jucator = rand() % 4 + 1;
				}
				if (jucator == nrP2)
				{
					ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player2.getNrPlayer());
					logger.log(ply, Logger::Level::Info);
					ply.clear();
					bangAction(player1, player2, player3, player4, player1.getBangLocation() + 1, tragere);
				}
				else
					if (jucator == nrP3)
					{
						ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player3.getNrPlayer());
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						bangAction(player1, player3, player2, player4, player1.getBangLocation() + 1, tragere);
					}
					else
						if (jucator == nrP4)
						{
							ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Bang! on player" + std::to_string(player4.getNrPlayer());
							logger.log(ply, Logger::Level::Info);
							ply.clear();
							bangAction(player1, player4, player2, player3, player1.getBangLocation() + 1, tragere);
						}
				if (isSherifAlive(player2, player3, player4) == false)
				{
					ok = 0;
					return 1;
				}
				if (OnlySherifAlive(player2, player3, player4) == true)
				{
					ok = 0;
					return 2;
				}
			}
		}
		 if (player1.getCharacterName() == Characters::Name::SidKetchum)
		{
			if (player1.getRole() == Roles::Rol::Sheriff && player1.getNumberOfLives() < 5 && player1.getNumberOfCards() >= 2)
			{
				int c1, c2;
				c1 = player1.cardWithLessPriority();
				discardedCards.addCard(player1.discardCard(c1));
				c2 = player1.cardWithLessPriority();
				discardedCards.addCard(player1.discardCard(c2 ));
				player1.updateLives(1);
				ply = "Player" + std::to_string(player1.getNrPlayer()) + " discard to cards and take on life back";
				logger.log(ply, Logger::Level::Info);
				ply.clear();
			}
			else
				if (player1.getRole() != Roles::Rol::Sheriff && player1.getNumberOfLives() < 4 && player1.getNumberOfCards() >= 2)
				{
					int c1, c2;
					c1 = player1.cardWithLessPriority();
					discardedCards.addCard(player1.discardCard(c1));
					c2 = player1.cardWithLessPriority();
					discardedCards.addCard(player1.discardCard(c2 ));
					player1.updateLives(1);
					ply = "Player" + std::to_string(player1.getNrPlayer()) + " discard to cards and take on life back";
					logger.log(ply, Logger::Level::Info);
					ply.clear();
				}
		}
		 if (player1.hasDiligenta() == true)
		{
			player1.addCard(unusedCards.pickCards());
			player1.addCard(unusedCards.pickCards());
			discardedCards.addCard(player1.discardCard(player1.hasDiligentaLocation()));
			ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Diligenta";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
		}
		 if (player1.hasWeelFargo() == true)
		{
			player1.addCard(unusedCards.pickCards());
			player1.addCard(unusedCards.pickCards());
			player1.addCard(unusedCards.pickCards());
			discardedCards.addCard(player1.discardCard(player1.hasWeelFargoLocation()));
			ply = "Player" + std::to_string(player1.getNrPlayer()) + " usen Wells Fargo";
			logger.log(ply, Logger::Level::Info);
			ply.clear();
		}
		 if (player1.hasCatBalou() == true)
		{
			if (player1.getRole() == Roles::Rol::Outlaw)
			{
				if (player2.getRole() == Roles::Rol::Sheriff  && player2.isAlive() && player2.getNumberOfCards() > 0)
				{
					if (player2.getNrPlayer() == 1)
					{
						ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Cat Balou on player"+ std::to_string(player2.getNrPlayer());
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						discardedCards.addCard(player1.discardCard(player1.hasCatBalouLocation()));
						catBalouAction(player1, player2, player3, player4);

					}
					else
					{
						ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Cat Balou on player" + std::to_string(player2.getNrPlayer());
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						discardedCards.addCard(player1.discardCard(player1.hasCatBalouLocation()));
						catBalouActionAI(player1, player2, player3, player4);
					}
				}
				if (player3.getRole() == Roles::Rol::Sheriff  && player3.isAlive() && player3.getNumberOfCards() > 0)
				{
					if (player3.getNrPlayer() == 1)
					{
						ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Cat Balou on player" + std::to_string(player3.getNrPlayer());
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						discardedCards.addCard(player1.discardCard(player1.hasCatBalouLocation()));
						catBalouAction(player1, player3, player2, player4);
					}
					else
					{
						ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Cat Balou on player" + std::to_string(player3.getNrPlayer());
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						discardedCards.addCard(player1.discardCard(player1.hasCatBalouLocation()));
						catBalouActionAI(player1, player3, player2, player4);
					}
				}
				if (player4.getRole() == Roles::Rol::Sheriff  && player4.isAlive() && player4.getNumberOfCards() > 0)
				{
					if (player4.getNrPlayer() == 1)
					{
						ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Cat Balou on player" + std::to_string(player4.getNrPlayer());
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						discardedCards.addCard(player1.discardCard(player1.hasCatBalouLocation()));
						catBalouAction(player1, player4, player3, player2);
					}
					else
					{
						ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Cat Balou on player" + std::to_string(player4.getNrPlayer());
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						discardedCards.addCard(player1.discardCard(player1.hasCatBalouLocation()));
						catBalouActionAI(player1, player4, player3, player2);
					}
				}
			}
			else
			{
				unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
				int jucator =0;
				while (true)
				{
					jucator = rand() % 4 + 1;
					if (jucator == player2.getNrPlayer() && player2.isAlive() && player2.getNumberOfCards() > 0)
					{
						if (player2.getNrPlayer() == 1)
						{
							ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Cat Balou on player" + std::to_string(player2.getNrPlayer());
							logger.log(ply, Logger::Level::Info);
							ply.clear();
							discardedCards.addCard(player1.discardCard(player1.hasCatBalouLocation()));
							catBalouAction(player1, player2, player3, player4);
						}
						else
						{
							ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Cat Balou on player" + std::to_string(player2.getNrPlayer());
							logger.log(ply, Logger::Level::Info);
							ply.clear();
							discardedCards.addCard(player1.discardCard(player1.hasCatBalouLocation()));
							catBalouActionAI(player1, player2, player3, player4);
						}
						break;
					}
					if (jucator == player3.getNrPlayer() && player3.isAlive() && player3.getNumberOfCards() > 0)
					{
						if (player3.getNrPlayer() == 1)
						{
							ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Cat Balou on player" + std::to_string(player3.getNrPlayer());
							logger.log(ply, Logger::Level::Info);
							ply.clear();
							discardedCards.addCard(player1.discardCard(player1.hasCatBalouLocation()));
							catBalouAction(player1, player3, player2, player4);
						}
						else
						{
							ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Cat Balou on player" + std::to_string(player3.getNrPlayer());
							logger.log(ply, Logger::Level::Info);
							ply.clear();
							discardedCards.addCard(player1.discardCard(player1.hasCatBalouLocation()));
							catBalouActionAI(player1, player3, player2, player4);
						}
						break;
					}
					if (jucator == player4.getNrPlayer() && player4.isAlive() && player4.getNumberOfCards() > 0)
					{
						if (player4.getNrPlayer() == 1)
						{
							ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Cat Balou on player" + std::to_string(player4.getNrPlayer());
							logger.log(ply, Logger::Level::Info);
							ply.clear();
							discardedCards.addCard(player1.discardCard(player1.hasCatBalouLocation()));
							catBalouAction(player1, player4, player3, player2);
						}
						else
						{
							ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Cat Balou on player" + std::to_string(player4.getNrPlayer());
							logger.log(ply, Logger::Level::Info);
							ply.clear();
							discardedCards.addCard(player1.discardCard(player1.hasCatBalouLocation()));
							catBalouActionAI(player1, player4, player3, player2);
						}
						break;
					}
				}
			}
		}
		 if (player1.hasPanica() == true)
		{
			if (player1.getRole() == Roles::Rol::Outlaw)
			{
				if (player2.getRole() == Roles::Rol::Sheriff  && player2.isAlive() && player1.getDistance(player2.getNrPlayer() - 1) <= 1 && player2.getNumberOfCards() > 0)
				{
					if (player2.getNrPlayer() == 1)
					{
						ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Panica! on player" + std::to_string(player2.getNrPlayer());
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						panicaActionAI(player1, player2, player3, player4, player1.hasPanicaLocation() + 1);
						discardedCards.addCard(player1.discardCard(player1.hasPanicaLocation()));
					}
					
				}
				if (player3.getRole() == Roles::Rol::Sheriff  && player3.isAlive() && player1.getDistance(player3.getNrPlayer() - 1) <= 1 && player3.getNumberOfCards() > 0)
				{
					if (player3.getNrPlayer() == 1)
					{
						ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Panica! on player" + std::to_string(player3.getNrPlayer());
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						panicaActionAI(player1, player3, player2, player4, player1.hasPanicaLocation() + 1);
						discardedCards.addCard(player1.discardCard(player1.hasPanicaLocation()));
					}			
				}
				if (player4.getRole() == Roles::Rol::Sheriff  && player4.isAlive() && player1.getDistance(player4.getNrPlayer() - 1) <= 1 && player4.getNumberOfCards() > 0)
				{
					if (player4.getNrPlayer() == 1)
					{
						ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Panica! on player" + std::to_string(player4.getNrPlayer());
						logger.log(ply, Logger::Level::Info);
						ply.clear();
						panicaActionAI(player1, player4, player3, player2, player1.hasPanicaLocation() + 1);
						discardedCards.addCard(player1.discardCard(player1.hasPanicaLocation()));
					}
				}
			}
			else
			{
				unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
				int jucator = 0;
				while (true)
				{
					jucator = rand() % 4 + 1;
					if (jucator == player2.getNrPlayer() && player2.isAlive() && player1.getDistance(player2.getNrPlayer() - 1) <= 1 && player2.getNumberOfCards() > 0)
					{
						if (player2.getNrPlayer() == 1)
						{
							ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Panica! on player" + std::to_string(player2.getNrPlayer());
							logger.log(ply, Logger::Level::Info);
							ply.clear();
							panicaActionAI(player1, player2, player3, player4, player1.hasPanicaLocation() + 1);
							discardedCards.addCard(player1.discardCard(player1.hasPanicaLocation()));
						}
						break;
					}
					if (jucator == player3.getNrPlayer() && player3.isAlive() && player1.getDistance(player3.getNrPlayer() - 1) <= 1 && player3.getNumberOfCards() > 0)
					{
						if (player3.getNrPlayer() == 1)
						{
							ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Panica! on player" + std::to_string(player3.getNrPlayer());
							logger.log(ply, Logger::Level::Info);
							ply.clear();
							panicaActionAI(player1, player3, player2, player4, player1.hasPanicaLocation() + 1);
							discardedCards.addCard(player1.discardCard(player1.hasPanicaLocation()));
						}				
						break;
					}
					if (jucator == player4.getNrPlayer() && player4.isAlive() && player1.getDistance(player4.getNrPlayer() - 1) <= 1 && player4.getNumberOfCards() > 0)
					{
						if (player4.getNrPlayer() == 1)
						{
							ply = "Player" + std::to_string(player1.getNrPlayer()) + " used Panica! on player" + std::to_string(player4.getNrPlayer());
							logger.log(ply, Logger::Level::Info);
							ply.clear();
							panicaActionAI(player1, player4, player2, player3, player1.hasPanicaLocation() + 1);
							discardedCards.addCard(player1.discardCard(player1.hasPanicaLocation()));
						}						
						break;
					}
				}
			}
		}
		 if (player1.getCharacterName() == Characters::Name::SuzyLafayette && player1.getNumberOfCards() == 0)
		 {
			 takeACard(player1);
			 ply = "Player" + std::to_string(player1.getNrPlayer()) + " tooke a card";
			 logger.log(ply, Logger::Level::Info);
			 ply.clear();
		 }
		 if (player1.getNumberOfLives() >= player1.getNumberOfCards())
		 {
			 ok = 0;

		 }
		 else
		 {
			 int nrCartiDecartat = player1.getNumberOfCards() - player1.getNumberOfLives();
			 unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
			 for (auto it = 0; it < nrCartiDecartat; ++it)
			 {
				 int nrCart = rand() % player1.getNumberOfCards() + 1;
				 ply = "Player" + std::to_string(player1.getNrPlayer()) + " discard card number " + std::to_string(nrCart);
				 logger.log(ply, Logger::Level::Info);
				 ply.clear();
				 discardedCards.addCard(player1.discardCard(nrCart - 1));
			 }
			 ok = 0;

		 }
	}
	return 0;
}

Player Game::constructPlayer(const int & rolNumber, Characters& caracter, const int &nrPlayer)
{
	int nrCarti = 0;
	int numarCaracter;
	int nrCaracter;
	Characters carac;
	while (true)
	{
		std::cout << "Alege un numar intre 1 si " << 16 - nrPlayer + 1 << std::endl;
		std::cin >> numarCaracter;
		try {
			nrCaracter = caracter.pickCharacter(numarCaracter,nrPlayer);
			Characters c(nrCaracter);
			carac = c;
			break;
		}
		catch (const std::out_of_range& exception) {
			std::cout << exception.what() << std::endl;
		}
	}
	nrCarti = carac.getLives();
	if (rolNumber == 1)
		nrCarti++;
	std::vector<Cards>carti;
	for (auto index = 0; index < nrCarti; ++index)
		carti.push_back(unusedCards.pickCards());
	Player player(rolNumber, nrCaracter, carti, nrPlayer, nrPlayers);
	return std::move(player);
}

Player Game::constructPlayerAI(const int & rolNumber, Characters & caracter, const int & nrPlayer)
{
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	int nrCarti = 0;
	int numarCaracter= rand() % 10 + 1;
	int nrCaracter;
	Characters carac;
	while (true)
	{
		//std::cout << "Alege un numar intre 1 si " << 16 - nrPlayer + 1 << std::endl;
		//std::cin >> numarCaracter;
		try {
			nrCaracter = caracter.pickCharacter(numarCaracter, nrPlayer);
			Characters c(nrCaracter);
			carac = c;
			break;
		}
		catch (const std::out_of_range& exception) {
			std::cout << exception.what() << std::endl;
		}
	}
	nrCarti = carac.getLives();
	if (rolNumber == 1)
		nrCarti++;
	std::vector<Cards>carti;
	for (auto index = 0; index < nrCarti; ++index)
		carti.push_back(unusedCards.pickCards());
	Player player(rolNumber, nrCaracter, carti, nrPlayer, nrPlayers);
	return std::move(player);
}
