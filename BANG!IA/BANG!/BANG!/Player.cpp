#include "Player.h"
#include <iterator>
#include <algorithm>
#include <utility>
#include <vector>

const int trageri = 999;

Player::Player() :
	gun("Colt 45", 0, "None"),
	rol(0),
	character(0),
	lives(0)
{
}

Player::Player(const int & rol, const int & character, const std::vector<Cards> cards, const int& nrPalyer, const int &nrOfPlayers) :
	gun("Colt 45", 0, "None"),
	rol(rol),
	character(character),
	inHeandCards(cards),
	nrPlayer(nrPalyer)
{

	lives = this->character.getLives();
	if (this->rol.getName() == Roles::Rol::Sheriff)
		lives++;
	distanceCalculation(nrOfPlayers);
	if (this->character.getName() == Characters::Name::WillyTheKid)
		this->nrTrageri = trageri;
	else
		this->nrTrageri = 1;
}

int Player::getNumberOfLives() const
{
	return lives;
}

int Player::getNumberOfCards() const
{
	return inHeandCards.size();
}

Cards Player::discardActiveCard(const std::string & name)
{
	auto node = this->activeCards.extract(name);
	if (node)
		return std::move(node.mapped());

	return Cards();
}

void Player::discardALLCards(DiscardedCards &discardedCards)
{
	if (inHeandCards.size() > 0)
	{
		for (int i = 0; i < inHeandCards.size(); ++i)
			discardedCards.addCard(std::move(this->discardCard(i)));
	}
	if (activeCards.size() > 0)
	{
		for (auto pair : activeCards)
			discardedCards.addCard(std::move(this->discardActiveCard(pair.first)));
	}
}

void Player::setGun(const Cards & card)
{
	gun = card;
	if (gun.getName() == "Volcanic")
		this->nrTrageri = trageri;
	else
		if (this->character.getName() != Characters::Name::WillyTheKid)
			this->nrTrageri = 1;
}

Cards Player::discardGun()
{
	if (gun.getName() != "Colt 45")
	{
		Cards  card = std::move(this->gun);
		gun = Cards("Colt 45", 0, "None");
		if (this->character.getName() != Characters::Name::WillyTheKid)
			nrTrageri = 1;
		return std::move(card);
	}
	return Cards();

}

Cards Player::discardCard(const int & index)
{
	Cards  card = std::move(this->inHeandCards.at(index));
	this->inHeandCards.erase(this->inHeandCards.begin() + index);
	return std::move(card);

}

void Player::addCard(const Cards & card)
{
	this->inHeandCards.push_back(card);
}

void Player::activeCard(const int & card)
{
	Cards c = std::move(this->inHeandCards.at(card));
	this->activeCards.insert(std::make_pair<std::string, Cards&>(c.getName(), c));
	this->inHeandCards.erase(this->inHeandCards.begin() + card);
}

void Player::distanceCalculation(const int & numberOfPlayer)
{
	//distanta se calculeaza in fucntie de numar de jucatori, astfel ca se observa ca modului diferentei dintre numarul a doi jucatori mod numarul de jucatori din joc ne va da distanta dintre acestia
	distance.clear();
	for (int index = 1; index <= numberOfPlayer; index++)
	{
		int dist = abs(index - nrPlayer);
		if (dist <= numberOfPlayer / 2)
		{
			distance.push_back(dist);
		}
		else
		{
			distance.push_back(numberOfPlayer - dist);
		}
	}

}

void Player::distanceCalculationAfterOnePlayerDead(const int & nrPlayer, const int & numberOfPlayer)
{
	if (this->distance[nrPlayer - 1] > 1 && abs(this->nrPlayer - nrPlayer) % numberOfPlayer > 1)
		this->distance[nrPlayer - 1]--;
}

void Player::increaseDistance(const int & index)
{
	distance.at(index)++;
}
void Player::decreaseDistance(const int & index)
{
	distance.at(index)--;
}
int Player::getDistance(const int & index)
{
	return distance.at(index);
}
int Player::getNrActiveCards() const
{
	return this->activeCards.size();
}
Roles::Rol Player::getRole() const
{
	return this->rol.getName();
}
Characters::Name Player::getCharacterName()
{
	return character.getName();
}
Cards Player::getCard(const int & index)
{
	return inHeandCards.at(index);
}
int Player::getNrPlayer()const
{
	return nrPlayer;
}
void Player::updateLives(const int & index)
{
	if (index == 1)
		this->lives++;
	else
		this->lives--;
}
Cards Player::isBeer()
{
	for (auto it = 0; it < inHeandCards.size(); ++it)
		if (inHeandCards.at(it).getName() == "Bere")
		{
			Cards  card = std::move(inHeandCards.at(it));
			inHeandCards.erase(inHeandCards.begin() + it);
			return std::move(card);
		}
	return Cards();
}
void Player::showCards(const int& sem)
{
	if (sem == 2)
	{
		if (!inHeandCards.empty())
		{
			int ct = 1;
			std::cout << "\nCarti in mana\n";
			for (const auto& card : inHeandCards)
			{
				std::cout << "----------------" << ct << "-----------------\n" << card;
				ct++;
			}
			//	std::copy(inHeandCards.begin(), inHeandCards.end(), std::ostream_iterator<Cards>(std::cout, ""));
		}

		if (!activeCards.empty())
		{
			int ct = 1;
			std::cout << "\nCarti Activate\n";
			for (const auto& card : activeCards)
			{
				std::cout << "----------------" << ct << "-----------------\n" << card.second;
				ct++;
			}
		}
	}
	if (sem == 0)
	{
		if (!activeCards.empty())
		{
			int ct = 1;
			std::cout << "\nCarti Activate\n";
			for (const auto& card : activeCards)
			{
				std::cout << "----------------" << ct << "-----------------\n" << card.second;
				ct++;
			}
		}
	}
	if (sem == 1)
	{
		if (!inHeandCards.empty())
		{
			int ct = 1;
			std::cout << "\nCarti in mana\n";
			for (const auto& card : inHeandCards)
			{
				std::cout << "----------------" << ct << "-----------------\n" << card;
				ct++;
			}
			//std::copy(inHeandCards.begin(), inHeandCards.end(), std::ostream_iterator<Cards>(std::cout, ""));
		}
	}
}
Cards Player::getGun() const
{
	return gun;
}
std::string Player::getGunName()
{
	return this->gun.getName();
}
Cards Player::isBang()
{
	for (auto it = 0; it < inHeandCards.size(); ++it)
		if (inHeandCards.at(it).getName() == "Bang!")
		{
			Cards  card = std::move(inHeandCards.at(it));
			inHeandCards.erase(inHeandCards.begin() + it);
			return std::move(card);
		}
	return Cards();
}
Cards Player::isRatat()
{
	for (auto it = 0; it < inHeandCards.size(); ++it)
		if (inHeandCards.at(it).getName() == "Ratat!")
		{
			Cards  card = std::move(inHeandCards.at(it));
			inHeandCards.erase(inHeandCards.begin() + it);
			return std::move(card);
		}
	return Cards();
}
bool Player::isAlive() const
{
	if (lives > 0)
		return true;
	return false;
}
void Player::setNrPlayer(const int & nr)
{
	this->nrPlayer = nr;
}
int Player::getNrTrageri() const
{
	return this->nrTrageri;
}
Cards Player::isButoi()
{
	auto node = this->activeCards.extract("Butoi");
	if (node)
		return std::move(node.mapped());

	return Cards();
}
Cards Player::isMustang()
{
	auto node = this->activeCards.extract("Mustang");
	if (node)
		return std::move(node.mapped());

	return Cards();
}
Cards Player::isLuneta()
{
	auto node = this->activeCards.extract("Luneta");
	if (node)
		return std::move(node.mapped());

	return Cards();
}
void Player::copiereCarti(Player player)
{
	for (int i = 0; i < player.getNumberOfCards(); ++i)
		this->inHeandCards.push_back(player.getCard(i));
	for (auto it : player.activeCards)
		this->inHeandCards.push_back(it.second);

}
std::ostream & operator<<(std::ostream & os, const Player & player)
{
	os << player.rol;
	os << player.character;
	os << "Vieti: " << player.lives << std::endl;
	os << player.gun << std::endl;

	if (!player.inHeandCards.empty())
	{
		os << "\n---------Carti in mana----------\n";
		int ct = 1;
		for (const auto& card : player.inHeandCards)
		{
			os << "---------" << ct << "---------\n" << card;
			ct++;
		}
		//std::copy(player.inHeandCards.begin(), player.inHeandCards.end(), std::ostream_iterator<Cards>(os, "---------------\n"+player.inHeandCards.size()));
	}

	if (!player.activeCards.empty())
	{
		int ct = 1;
		os << "\n--------Carti Activate----------\n";
		for (const auto& card : player.activeCards)
		{
			os << "---------" << ct << "---------\n" << card.second;
			ct++;
		}
	}
	os << "\nDistante: |";
	for (auto index = 0; index < player.distance.size(); ++index)
	{

		if (index == player.getNrPlayer() - 1)
			os << 0 << " | ";
		else if (player.distance.at(index) < 1)
			os << 1 << " | ";
		else
			os << player.distance.at(index) << " | ";

	}
	//std::copy(player.distance.begin(), player.distance.end(), std::ostream_iterator<int>(os, "|"));
	return os;
}
int Player::getNrOfBang()
{
	int nr = 0;
	for (auto it : inHeandCards)
	{
		if (it.getName() == "Bang!")
			nr++;
	}
	return nr;
}
int Player::getBangLocation()
{
	for (auto it=0;it< inHeandCards.size();it++)
	{
		if (inHeandCards.at(it).getName() == "Bang!")
			return it;
	}
}
int Player::cardWithLessPriority()
{
	int min = 0;
	int index = 0;
	for (auto it = 0; it < inHeandCards.size(); it++)
		if (inHeandCards.at(it).getPriority() > min)
		{
			min = inHeandCards.at(it).getPriority();
			index = it;
		}
	return index;
}
bool Player::hasMustang(int op)
{
	if (op == 1)
	{
		for (auto it : activeCards)
			if (it.first == "Mustang")
				return true;
	}
	else
	{
		for (auto it : inHeandCards)
			if (it.getName() == "Mustang")
				return true;
	}
	return false;
}
bool Player::hasButoi(int op)
{
	if (op == 1)
	{
		for (auto it : activeCards)
			if (it.first == "Butoi")
				return true;
	}
	else
	{
		for (auto it : inHeandCards)
			if (it.getName() == "Butoi")
				return true;
	}
	return false;
}
bool Player::hasLuneta(int op)
{
	if (op == 1)
	{
		for (auto it : activeCards)
			if (it.first == "Luneta")
				return true;
	}
	else
	{
		for (auto it : inHeandCards)
			if (it.getName() == "Luneta")
				return true;
	}
	return false;
}
bool Player::hasSalon()
{
	for (auto it : inHeandCards)
		if (it.getName() == "Salon")
			return true;
	return false;
}
bool Player::hasBere()
{
	for (auto it : inHeandCards)
		if (it.getName() == "Bere")
			return true;
	return false;
}
bool Player::hasGatling()
{
	for (auto it : inHeandCards)
		if (it.getName() == "Gatling")
			return true;
	return false;
}
int Player::hasMustangLocation()
{
	
		for (auto it =0;it<inHeandCards.size();it++)
			if (inHeandCards.at(it).getName() == "Mustang")
				return it ;
}
int Player::hasButoiLocation()
{
	for (auto it = 0; it < inHeandCards.size(); it++)
		if (inHeandCards.at(it).getName() == "Butoi")
			return it;
}
int Player::hasLunetaLocation()
{
	for (auto it = 0; it < inHeandCards.size(); it++)
		if (inHeandCards.at(it).getName() == "Luneta")
			return it;
}
int Player::hasSalonLocation()
{
	for (auto it = 0; it < inHeandCards.size(); it++)
		if (inHeandCards.at(it).getName() == "Salon")
			return it;
}
int Player::hasBereLocation()
{
	for (auto it = 0; it < inHeandCards.size(); it++)
		if (inHeandCards.at(it).getName() == "Bere")
			return it;
}
int Player::hasCatBalouLocation()
{
	for (auto it = 0; it < inHeandCards.size(); it++)
		if (inHeandCards.at(it).getName() == "Cat Balou")
			return it;
}
bool Player::hasCatBalou()
{
	for (auto it = 0; it < inHeandCards.size(); it++)
		if (inHeandCards.at(it).getName() == "Cat Balou")
			return true;
	return false;
}
int Player::hasWeelFargoLocation()
{
	for (auto it = 0; it < inHeandCards.size(); it++)
		if (inHeandCards.at(it).getName() == "Wells Fargo")
			return it;
}
bool Player::hasWeelFargo()
{
	for (auto it = 0; it < inHeandCards.size(); it++)
		if (inHeandCards.at(it).getName() == "Wells Fargo")
			return true;
	return false;
}
int Player::hasPanicaLocation()
{
	for (auto it = 0; it < inHeandCards.size(); it++)
		if (inHeandCards.at(it).getName() == "Panica!")
			return it;
}
bool Player::hasPanica()
{
	for (auto it = 0; it < inHeandCards.size(); it++)
		if (inHeandCards.at(it).getName() == "Panica!")
			return true;
	return false;
}
int Player::hasDiligentaLocation()
{
	for (auto it = 0; it < inHeandCards.size(); it++)
		if (inHeandCards.at(it).getName() == "Diligenta")
			return it;
}
bool Player::hasDiligenta()
{
	for (auto it = 0; it < inHeandCards.size(); it++)
		if (inHeandCards.at(it).getName() == "Diligenta")
			return true;
	return false;
}
int Player::hasGatlingLocation()
{
	for (auto it = 0; it < inHeandCards.size(); it++)
		if (inHeandCards.at(it).getName() == "Gatling")
			return it;
}
int Player::distanceGunInHeandLocation()
{
	int dist = 0;
	for (auto it = 0; it < inHeandCards.size(); it++)
	{
		if (inHeandCards.at(it).getDistance() > dist)
			return it;
	}

}
int Player::distanceGunInHeand()
{
	int dist = 0;
	for (auto it : inHeandCards)
	{
		if (it.getDistance() > dist)
			dist = it.getDistance();
	}
	return dist;
}