#pragma once
#include "Roles.h"
#include"Characters.h"
#include"Cards.h"
#include"DiscardedCards.h"
#include <unordered_map>	
#include <array>
class Player
{
public:
	Player();
	Player(const int &rol, const int &character, const std::vector<Cards>cards, const int& nrPlayer, const int &nrOfPlayers);

	int getNumberOfLives()const;
	int getNumberOfCards()const;
	int getDistance(const int& index);
	int getNrActiveCards() const;
	Roles::Rol getRole()const;
	Characters::Name getCharacterName();
	Cards getCard(const int &index);
	int getNrPlayer()const;
	Cards getGun()const;
	std::string getGunName();
	int getNrTrageri()const;

	void setGun(const Cards& card);
	void setNrPlayer(const int &nr);

	Cards discardActiveCard(const std::string& name);
	void discardALLCards(DiscardedCards &discardedCards);
	Cards discardGun();
	Cards discardCard(const int& index);
	void addCard(const Cards& card);
	void activeCard(const int& card);
	//distanta se ia in cerc
	void distanceCalculation(const int& numberOfPlayer);
	//distanta se moifica daca un anumit player este mort 
	void distanceCalculationAfterOnePlayerDead(const int& nrPlayer, const int& numberOfPlayer);
	void increaseDistance(const int& index);
	void decreaseDistance(const int& index);
	void updateLives(const int &index);
	void copiereCarti(Player player);
	void showCards(const int& sem);

	friend std::ostream& operator << (std::ostream& os, const Player& player);

	bool isAlive()const;
	Cards isBang();
	Cards isRatat();
	Cards isButoi();
	Cards isMustang();
	Cards isLuneta();
	Cards isBeer();
	
	int getNrOfBang();
	bool hasMustang(int op);
	bool hasButoi(int op);
	bool hasLuneta(int op);
	bool hasSalon();
	bool hasBere();
	bool hasGatling();

	int hasMustangLocation();
	int hasButoiLocation();
	int hasLunetaLocation();
	int hasSalonLocation();
	int hasBereLocation();
	int hasCatBalouLocation();
	bool hasCatBalou();
	int hasWeelFargoLocation();
	bool hasWeelFargo();
	int hasPanicaLocation();
	bool hasPanica();
	int hasDiligentaLocation();
	bool hasDiligenta();
	int hasGatlingLocation();
	int distanceGunInHeandLocation();
	int distanceGunInHeand();
	int getBangLocation();

	int cardWithLessPriority();
private:
	Roles rol;
	int nrTrageri;
	Characters character;
	int lives;
	std::vector<Cards>inHeandCards;
	std::unordered_map<std::string, Cards>activeCards;
	std::vector<int> distance;
	int nrPlayer;
	Cards gun;
};

