#include "Roles.h"


Roles::Roles(int nrRol)
{
	switch (nrRol)
	{
	case 0: {
		this->name = Rol::None;
		break; }
	case 1:
	{
		this->name = Rol::Sheriff;
		break;
	}
	case 4:
	{
		this->name = Rol::DeputySheriff;
		break;
	}
	case 2:
	{
		this->name = Rol::Outlaw;
		break;
	}
	case 3:
	{
		this->name = Rol::Renegate;
		break;
	}
	default:
		break;
	}
}


Roles::Rol Roles::getName() const
{
	return this->name;
}


std::ostream & operator<<(std::ostream & os, const Roles & rol)
{
	switch (rol.name)
	{
	case Roles::Rol::Sheriff:
	{
		os << "Rol: Sheriff" << std::endl << "Must eliminate all the Outlaws and the Renegade, to protect law and order " << std::endl;
		break;
	}
	case Roles::Rol::DeputySheriff:
	{
		os << "Rol: Deputy Sheriff" << std::endl << "They help and protect the Sheriff, and share his same goal, at all costs!" << std::endl;
		break;
	}
	case Roles::Rol::Outlaw:
	{
		os << "Rol: Outlaw" << std::endl << "They would like to kill the Sheriff, but they have no scruples about eliminating each other to gain rewards!" << std::endl;
		break;
	}
	case Roles::Rol::Renegate:
	{
		os << "Rol: Renegate" << std::endl << "He wants to be the new Sheriff, his goal is to be the last character in play." << std::endl;
		break;
	}
	default:
		break;
	}
	return os;
}