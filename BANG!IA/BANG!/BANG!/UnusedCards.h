#pragma once
#include<vector>
#include"DiscardedCards.h"
#include "Cards.h"
class UnusedCards
{
public:
	UnusedCards() = default;
	UnusedCards(std::vector<Cards> cards);

	void citire(std::vector < std::pair<std::string, int>>priority);
	void shuffleVector();

	UnusedCards& operator= (const DiscardedCards & other);
	UnusedCards& operator= (DiscardedCards && other);
	int sizePackage()const;
	Cards pickCards();

private:
	std::vector<Cards> unusedCards;
};

