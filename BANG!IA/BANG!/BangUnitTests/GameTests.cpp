#include "stdafx.h"
#include "CppUnitTest.h"
#include "Game.h"
#include "Player.h"
#include "Roles.h"
#include "UnusedCards.h"
#include "DiscardedCards.h"
#include "Cards.h"
#include "Characters.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BangUnitTests
{
	TEST_CLASS(GameTests)
	{
	public:

		TEST_METHOD(WhoIsSherif)
		{
			std::vector<Cards>carti;
			Player playerOne(3, 9, carti, 1, 4);
			Player playerTwo(2, 8, carti, 2, 4);
			Player playerThree(1, 4, carti, 3, 4);
			Player playerFour(4, 10, carti, 4, 4);

			Game game;
			game.whoIsSherif(playerOne, playerTwo, playerThree, playerFour);

			if (playerOne.getRole() != Roles::Rol::Sheriff)
				Assert::Fail;
		}

		/*TEST_METHOD(TakeACard)
		{
			
			UnusedCards unusedCards;
			unusedCards.citire();
			std::vector<Cards>carti;
			for (auto index = 0; index < Characters(9).getLives(); ++index)
				carti.push_back(unusedCards.pickCards());

	
			Player player(3, 9, carti, 1, 4);
			Game game;
			game.takeACard(player);

			if (carti.size() != Characters(9).getLives() + 2)
				Assert::IsFalse;
		}*/

		TEST_METHOD(SalonAction)
		{
			std::vector<Cards>carti;
			Player playerOne(3, 9, carti, 1, 4);
			Player playerTwo(2, 8, carti, 2, 4);
			Player playerThree(1, 4, carti, 3, 4);
			Player playerFour(4, 10, carti, 4, 4);

			Game game;
			game.salonAction(playerOne, playerTwo, playerThree, playerFour);

			if (playerOne.getNumberOfLives() != 4 && playerTwo.getNumberOfLives() != 5
				&& playerThree.getNumberOfLives() != 5 && playerFour.getNumberOfLives() != 4)
				Assert::Fail;
		}

		
	};
}