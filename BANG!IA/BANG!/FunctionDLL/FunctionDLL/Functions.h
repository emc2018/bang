#pragma once
#ifdef FUNCTIONS_EXPORTS
#define FUNCTIONS_API _declspec (dllexport)
#else 
#define FUNCTIONS_API _declspec(dllimport)
#endif

#include<vector>
class  FUNCTIONS_API Functions
{
public:
	Functions() = default;
	 std::vector<int> shuffleVector(std::vector<int> vector);
	 void wait(const int& numberOfSecond);
	int getRamdomNumber(const int& limitInf, const int& limitSup);
};

